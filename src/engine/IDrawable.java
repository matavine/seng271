/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.awt.Graphics;

/**
 *
 * @author Geoff
 */
public interface IDrawable 
{
    public void draw(Graphics graphics);
}
