/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.views;

import engine.ISingleton;
import engine.ui.UIManager;
import game.views.GameView;
import game.views.SetupView;
import game.views.StartView;
import java.util.HashMap;

/**
 *
 * @author Michael
 */
public class ViewManager implements ISingleton
{
    private static ViewManager m_instance;
    public static ViewManager getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new ViewManager();
        }

        return m_instance;
    }

    private HashMap<String, View> m_views;
    private String m_current_view_name;
    private View m_current_view;
    
    private String m_next_view_name = null;
    private boolean m_change_view = false;

    public View getCurrentView()
    {
        if (m_change_view)
        {
            UIManager.getInstance().hideAllUI();
        
            m_current_view.onExit();
            m_current_view = m_views.get(m_next_view_name);
            m_current_view_name = m_next_view_name;
            m_current_view.onEnter();
            m_change_view = false;
        }
        
        return m_current_view;
    }

    private ViewManager()
    {
        m_views = new HashMap<String, View>();

        m_current_view = new StartView();
        m_current_view.onEnter();
        m_current_view_name = "start";
        m_views.put("start", m_current_view);
        m_views.put("game", new GameView());
        m_views.put("setup", new SetupView());
    }

    public void changeView(String view_name)
    {
        if (!m_views.containsKey(view_name))
        {
            System.out.println("No such view registered in the ViewManager");
            return;
        }

        if (m_current_view_name.equals(view_name))
        {
            System.out.println("Current view and desired view are the same.");
            return;
        }
        
        m_change_view = true;
        m_next_view_name = view_name;
    }

    @Override
    public void destroy() 
    {
        m_instance = null;
    }
}
