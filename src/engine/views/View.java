/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.views;

import engine.IDrawable;
import engine.IUpdateable;

/**
 *
 * @author Michael
 */
public abstract class View implements IUpdateable, IDrawable
{
    public abstract void initialize();
    public abstract void destroy();

    public abstract void onEnter();
    public abstract void onExit();
}
