/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.structures;

/**
 *
 * @author Geoff
 */
public class Pair<X, Y> {
    public final X x;
    public final Y y;
    public Pair(X x, Y y)
    {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(this.getClass() == o.getClass())
        {
            return ( this.x.equals(( (Pair<X, Y>)o ).x) && this.y.equals(( (Pair<X, Y>)o ).y) );
        }
        else
        {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.x != null ? this.x.hashCode() : 0);
        hash = 97 * hash + (this.y != null ? this.y.hashCode() : 0);
        return hash;
    }
}
