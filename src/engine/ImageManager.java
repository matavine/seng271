/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package engine;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.RGBImageFilter;
import java.util.HashMap;
import javax.swing.ImageIcon;

/**
 *
 * @author Adam Anderson
 */
public class ImageManager implements ISingleton
{
    public static class ColorFilter
        extends RGBImageFilter
    {
        Color m_color;
        public ColorFilter(Color color) {
            m_color = color;
            canFilterIndexColorModel = false;
            
        }
        
        @Override
        public int filterRGB(int x, int y, int rgb) {
            if((rgb & 0x0000ffff) == 0 && (rgb & 0xff000000) != 0)
            {
                float scale_value = ((rgb & 0x00ff0000) >> 16)/256f;
                int red = (int)(m_color.getRed() * scale_value);
                red = (red << 16) & 0x00ff0000;
                int green = (int)(m_color.getGreen() * scale_value);
                green = (green << 8) & 0x0000ff00;
                int blue = (int)(m_color.getBlue() * scale_value);
                blue = blue & 0x000000ff;
                return (0xff000000 | red | green | blue);
            }
            return rgb;
        }
    }
    
    public static class GreyScaleFilter
        extends RGBImageFilter
    {
        public GreyScaleFilter() {
            canFilterIndexColorModel = false;
        }
        
        @Override
        public int filterRGB(int x, int y, int rgb)
        {
            int scale = 0;
            scale += ((rgb & 0x00ff0000) >> 16);
            scale += ((rgb & 0x0000ff00) >> 8);
            scale += (rgb & 0x000000ff);
            scale /= 3;
            
            rgb = (rgb & 0xff000000) + (scale << 16) + (scale << 8) + scale; 
            
            return rgb;
        }
    }
    
    private static final String CONTENT_DIR = "content/";
    private static ImageManager m_instance;
    private HashMap<String, ImageIcon> m_sprites;

    private ImageManager()
    {
        this.m_sprites = new HashMap(32);
    }

    public static ImageManager getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new ImageManager();
        }
        return m_instance;
    }

    private void loadSprite(String path)
    {
        m_sprites.put(path, new ImageIcon(path));
    }
    
    public void preloadSprite(String path)
    {
        path = CONTENT_DIR + path;
        m_sprites.put(path, new ImageIcon(path));
    }

    public Image getSprite(String path)
    {
        path = CONTENT_DIR + path;
        if(!m_sprites.containsKey(path))
        {
            this.loadSprite(path);
        }

        return (m_sprites.get(path)).getImage();
    }
    
    public Image filterSprite(String existing_path, String new_path, Color color)
    {
        if(m_sprites.containsKey(CONTENT_DIR + new_path))
        {
            return getSprite(new_path);
        }
        
        Image existing_image = getSprite(existing_path);

        ImageFilter filter = new ColorFilter(color);
        FilteredImageSource src = new FilteredImageSource(existing_image.getSource(), filter);
        
        Image new_image = Toolkit.getDefaultToolkit().createImage(src);
        
        m_sprites.put(CONTENT_DIR + new_path, new ImageIcon(new_image));
        return getSprite(new_path);
    }
    
    public Image greySprite(String path)
    {
        String new_path = path + "_greyscale";
        
        if(m_sprites.containsKey(CONTENT_DIR + new_path))
        {
            return getSprite(new_path);
        }
        
        Image existing_image = getSprite(path);

        ImageFilter filter = new GreyScaleFilter();
        FilteredImageSource src = new FilteredImageSource(existing_image.getSource(), filter);
        
        Image new_image = Toolkit.getDefaultToolkit().createImage(src);
        
        m_sprites.put(CONTENT_DIR + new_path, new ImageIcon(new_image));
        return getSprite(new_path);
    }
    
    public ImageIcon getSpriteIcon(String path)
    {
        path = CONTENT_DIR + path;
        if(!m_sprites.containsKey(path))
        {
            this.loadSprite(path);
        }

        return m_sprites.get(path);
    }

    @Override
    public void destroy() 
    {
        m_instance = null;
    }
}
