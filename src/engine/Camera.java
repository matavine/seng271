/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import game.Game;
import java.awt.Point;

/**
 *
 * @author G-slice
 */
public class Camera
    implements IUpdateable
{   
    protected float m_pan_speed = 0.25f;
    
    protected enum CameraState
    {
        NONE,
        PANNING,
        DRAGGING;
    }
    
    /**
     * The current state of the camera, used for panning.
     */
    protected CameraState m_state = CameraState.NONE;
    
    /**
     * The position that the camera is currently panning towards.
     */
    protected Point m_pan_target;
    
    /**
     * The current position of the camera, controls the offset between screen space and world space.
     */
    protected Point m_position;
    
    /**
     * The previously recorded position of the camera.
     */
    protected Point m_previous_position;
    
    /**
     * Creates a new camera object, with no special settings.
     */
    public Camera()
    {
        this.m_position = new Point();
        this.m_previous_position = new Point();
    }
    
    /**
     * Used to get the current position of the camera.
     * @return a copy of the camera's position. (Feel free to perform operations on it)
     */
    public Point getPosition()
    {
        return (Point)m_position.clone();
    }
    
    /**
     * Used to set the current position of the camera, the position is copied, so transforms may be performed on it.
     * @param position the position to move the camera to.
     */
    public void setPosition(Point position)
    {
        this.m_previous_position = (Point)m_position.clone();
        this.m_position = (Point)position.clone();
        
        m_position = constrainPosition(m_position);
        if(m_state == CameraState.PANNING)
        {
            m_state = CameraState.NONE;
        }
    }
    
    /**
     * Used to center the camera on a provided position.
     * @param position the position to center the camera on.
     */
    public void centerPosition(Point position)
    {
        this.m_previous_position = (Point)m_position.clone();
        this.m_position = (Point)position.clone();
        m_position.translate(-Game.SCREEN_WIDTH / 2, -Game.SCREEN_HEIGHT / 2);
        
        m_position = constrainPosition(m_position);
        if(m_state == CameraState.PANNING)
        {
            m_state = CameraState.NONE;
        }
    }
    
    /**
     * Translates the position of the camera by the provided vector.
     * @param p The point representing the vector to translate the camera along.
     */
    public void translatePosition(Point p)
    {
        translatePosition(p.x, p.y);
    }
    
    /**
     * Translates the position of the camera by the provided values.
     * @param x the x vector to add to the camera position.
     * @param y the y vector is add to the camera position.
     */
    public void translatePosition(int x, int y)
    {
        m_previous_position = (Point)m_position.clone();
        m_position.translate(x, y);
        
        m_position = constrainPosition(m_position);
        if(m_state == CameraState.PANNING)
        {
            m_state = CameraState.NONE;
        }
    }
    
    /**
     * Used to detect if a position in screen space is within the camera's view.
     * @param position The position to check.
     * @return True if the position is within the camera's view.
     */
    public boolean isInView(Point position)
    {
        if(position.x < 0 || position.x > Game.SCREEN_WIDTH)
        {
            return false;
        }
        if(position.y < 0 || position.y > Game.SCREEN_HEIGHT)
        {
            return false;
        }
        return true;
    }
    
    public void panPosition(Point position)
    {
        m_state = CameraState.PANNING;
        m_pan_target = constrainPosition((Point)position.clone());
    }
    
    public void panCenter(Point position)
    {
        m_state = CameraState.PANNING;
        position = (Point)position.clone();
        position.translate(-Game.SCREEN_WIDTH / 2, -Game.SCREEN_HEIGHT / 2);
        m_pan_target = constrainPosition(position);
    }
    
    /**
     * Converts a position from world space to screen space.
     * @param position The position to be copied and converted.
     * @return a copy of the position translated according to the camera's offset.
     */
    public Point worldToScreenSpace(Point position)
    {
        Point new_position = (Point)position.clone();
        new_position.translate(m_position.x * -1, m_position.y * -1);
        return new_position;
    }
    
    /**
     * Converts a position from screen space to world space.
     * @param position The position to be converted
     * @return a copy of the provided position translated back into world space.
     */
    public Point screenSpaceToWorld(Point position)
    {
        Point new_position = (Point)position.clone();
        new_position.translate(m_position.x, m_position.y);
        return new_position;
    }
    
    /**
     * Constrains the position of the camera according to a provided function.
     */
    protected Point constrainPosition(Point position)
    {
        return position;
        // Do Nothing
    }
    
    /**
     * This function should be called every frame. Used to dynamically effect the camera position.
     * e.g. camera shake, panning or other dynamic positioning.
     * @param game_time a game time object is required, so that the camera is aware of time changes between update calls.
     */
    @Override
    public void update(GameTime game_time)
    {
        if(m_state == CameraState.PANNING)
        {
            this.m_previous_position = (Point)m_position.clone();
            
            float x_diff = m_pan_target.x - m_position.x;
            float y_diff = m_pan_target.y - m_position.y;
            
            if(Math.abs(x_diff) <= 1 && Math.abs(y_diff) <= 1)
            {
                m_position = m_pan_target;
                m_position = constrainPosition(m_position);
                m_state = CameraState.NONE;
            }
            else
            {
                m_position.translate((int)(x_diff * m_pan_speed), (int)(y_diff * m_pan_speed));
                m_position = constrainPosition(m_position);
            }
        }
    }
}
