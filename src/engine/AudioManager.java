/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.io.File;
import javax.sound.sampled.*;
/**
 *
 * @author Michael
 */
public class AudioManager implements ISingleton
{
    private static AudioManager m_instance;
    public static AudioManager getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new AudioManager();
        }
        
        return m_instance;
    }
    
    private boolean is_music_playing = false;
    private Clip current_music_clip;
    private Clip current_effect_clip;
    
    private AudioManager()
    {
        // Do nothing.
    }
    
    public boolean isMusicPlaying()
    {
        return is_music_playing;
    }

    public void stopAudio()
    {
            if (!current_music_clip.isOpen())
            {
                return;
            }
            
            current_music_clip.stop();
            current_music_clip.close();
            is_music_playing = false;
    }

    public void playMusic(String path)
    {
        if(is_music_playing == true) 
        {
            stopAudio();
        }
        try
        {
            AudioInputStream stream = AudioSystem.getAudioInputStream(new File(path));
            DataLine.Info info = new DataLine.Info(Clip.class, stream.getFormat());
            current_music_clip = (Clip) AudioSystem.getLine(info);
            current_music_clip.open(stream);
            current_music_clip.loop(Clip.LOOP_CONTINUOUSLY);
            is_music_playing = true;
        }
        catch (UnsupportedAudioFileException i)
        {
            System.out.println("UnsupportedAudioFileException: "+ i.getMessage());
        }
        catch (Exception e)
        {
            System.out.println("Failed to play audio: " + e.getMessage());
        }
    }

    public void playEffect(String path)
    {
        try
        {
            AudioInputStream stream = AudioSystem.getAudioInputStream(new File(path));
            DataLine.Info info = new DataLine.Info(Clip.class, stream.getFormat());
            current_effect_clip = (Clip) AudioSystem.getLine(info);
            current_effect_clip.open(stream);
            current_effect_clip.start();
        }
        catch (UnsupportedAudioFileException i)
        {
            System.out.println("UnsupportedAudioFileException: "+ i.getMessage());
        }
        catch (Exception e)
        {
            System.out.println("Failed to play audio: " + e.getMessage());
        }
    }

    @Override
    public void destroy() 
    {
        m_instance = null;
    }
}
