/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

/**
 *
 * @author Michael
 */
public class GameTime 
{
    private long m_total_elapsed_time;
    private long m_time_since_last_update;
    private int m_fps;
    
    public long getTotalElapsedTime()
    {
        return m_total_elapsed_time;
    }
    
    public long getTimeSinceLastUpdate()
    {
        return m_time_since_last_update;
    }
    
    public int getFPS()
    {
        return m_fps;
    }
    
    public GameTime(long total_elapsed_time, long time_since_last_update, int fps)
    {
        m_total_elapsed_time = total_elapsed_time;
        m_time_since_last_update = time_since_last_update;
        m_fps = fps;
    }
}
