/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.input;

import engine.GameTime;
import engine.ISingleton;
import engine.IUpdateable;
import java.awt.Point;
import java.util.HashMap;

/**
 *
 * @author michael
 */
public class InputHandler
    implements IUpdateable, ISingleton
{
    private static InputHandler m_instance;
    public static InputHandler getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new InputHandler();
        }
        
        return m_instance;
    }
    
    private KeyHandler m_key_handler;
    public KeyHandler getKeyHandler()
    {
        return m_key_handler;
    }
    
    private MouseHandler m_mouse_handler;
    public MouseHandler getMouseHandler()
    {
        return m_mouse_handler;
    }

    public enum InputType
    {
        MOUSE,
        KEY
    };
    
    public enum InputState
    {
        PRESSED,
        HELD,
        JUSTRELEASED,
        RELEASED
    };
    
    private HashMap<Integer, InputState> m_key_status;
    private HashMap<Integer, InputState> m_mouse_status;
    
    private InputHandler()
    {
        m_key_handler = new KeyHandler();
        m_mouse_handler = new MouseHandler();
        m_key_status = new HashMap<Integer, InputState>();
        m_mouse_status = new HashMap<Integer, InputState>();
    }
    
    public boolean isInputPressed(InputType input_type, int input_code)
    {
        return inputMatchesState(input_type, input_code, InputState.PRESSED);
    }
    
    public boolean isInputJustReleased(InputType input_type, int input_code)
    {
        return inputMatchesState(input_type, input_code, InputState.JUSTRELEASED);
    }
    
    public boolean isInputReleased(InputType input_type, int input_code)
    {
        return inputMatchesState(input_type, input_code, InputState.RELEASED);
    }
    
    public boolean isInputHeld(InputType input_type, int input_code)
    {
        return inputMatchesState(input_type, input_code, InputState.HELD);
    }
    
    public boolean isInputDown(InputType input_type, int input_code)
    {
        return (isInputPressed(input_type, input_code) || isInputHeld(input_type, input_code));
    }
    
    public boolean isInputUp(InputType input_type, int input_code)
    {
        return (isInputJustReleased(input_type, input_code) || isInputReleased(input_type, input_code));
    }
    
    private boolean inputMatchesState(InputType input_type, int input_code, InputState input_state)
    {
        boolean result = false;
        switch (input_type)
        {
            case MOUSE:
                if(!m_mouse_status.containsKey(input_code))
                {
                    registerInput(input_type, input_code);
                }
                result = (m_mouse_status.get(input_code) == input_state);
                break;
            case KEY:
                if(!m_key_status.containsKey(input_code))
                {
                    registerInput(input_type, input_code);
                }
                result = (m_key_status.get(input_code) == input_state);
                break;
        }
        return result;
    }
    
    public void registerInput(InputType input_type, int input_code)
    {
        switch (input_type)
        {
            case MOUSE:
                if(m_mouse_handler.isMouseButtonPressed(input_code))
                {
                    m_mouse_status.put(input_code, InputState.HELD);
                }
                else
                {
                    m_mouse_status.put(input_code, InputState.RELEASED);
                }
                break;
            case KEY:
                if(m_key_handler.isKeyPressed(input_code))
                {
                    m_key_status.put(input_code, InputState.HELD);
                }
                else
                {
                    m_key_status.put(input_code, InputState.RELEASED);
                }
                break;
        }
    }
    
    public Point getMousePosition()
    {
        return m_mouse_handler.getPosition();
    }
    
    @Override
    public void update(GameTime game_time)
    {
        for (Integer key_code : m_key_status.keySet())
        {
            int code = key_code.intValue();
            InputState state = m_key_status.get(key_code);
            if (m_key_handler.isKeyPressed(code))
            {
                if(state == InputState.PRESSED)
                    m_key_status.put(code, InputState.HELD);
                else if(state == InputState.RELEASED || state == InputState.JUSTRELEASED)
                    m_key_status.put(code, InputState.PRESSED);
            }
            else
            {
                if(state == InputState.JUSTRELEASED)
                    m_key_status.put(code, InputState.RELEASED);
                else if(state == InputState.PRESSED || state == InputState.HELD)
                    m_key_status.put(code, InputState.JUSTRELEASED);
            }
        }
        
        for (Integer key_code : m_mouse_status.keySet())
        {
            int code = key_code.intValue();
            InputState state = m_mouse_status.get(key_code);
            if (m_mouse_handler.isMouseButtonPressed(code))
            {
                if(state == InputState.PRESSED)
                    m_mouse_status.put(code, InputState.HELD);
                else if(state == InputState.RELEASED || state == InputState.JUSTRELEASED)
                    m_mouse_status.put(code, InputState.PRESSED);
            }
            else
            {
                if(state == InputState.JUSTRELEASED)
                    m_mouse_status.put(code, InputState.RELEASED);
                else if(state == InputState.PRESSED || state == InputState.HELD)
                    m_mouse_status.put(code, InputState.JUSTRELEASED);
            }
        }
    }
    
    @Override
    public void destroy() 
    {
        m_instance = null;
    }
    
    // TODO: move functionality to a control class, this class is for pure inputs.
    /*
    public void RegisterButton(String name, InputType button_type, int button_code)
    {
        ButtonMap button_map = new ButtonMap(button_type, button_code);
        m_input_map.put(name, button_map);
    }
    
    public void DeregisterButton(String name)
    {
        m_input_map.remove(name);
    }
    
    private class ButtonMap
    {
        private InputHandler.InputType m_button_type;
        private int m_button_code;
        
        public void SetButtonType(InputHandler.InputType button_type)
        {
            m_button_type = button_type;
        }
        
        public void SetButtonCode(int button_code)
        {
            m_button_code = button_code;
        }
        
        public InputHandler.InputType GetButtonType()
        {
            return m_button_type;
        }
        
        public int GetButtonCode()
        {
            return m_button_code;
        }
        
        public ButtonMap(InputHandler.InputType button_type, int button_code)
        {
            m_button_type = button_type;
            m_button_code = button_code;
        }
    }
    */
}
