/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

/**
 *
 * @author michael
 */
public class KeyHandler implements KeyListener
{
    private HashMap m_key_input;

    public enum KeyState
    {
        PRESSED,
        RELEASED
    };

    public KeyHandler()
    {
        m_key_input = new HashMap();
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        setKeyState(e.getKeyCode(), KeyState.PRESSED);
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        setKeyState(e.getKeyCode(), KeyState.RELEASED);
    }
    
    @Override
    public void keyTyped(KeyEvent e)
    {
        // Nothing...
    }

    private void setKeyState(int key_code, KeyState key_state)
    {
        m_key_input.put(key_code, key_state);
    }

    private boolean isKeyState(int key_code, KeyState key_state)
    {
        if (!m_key_input.containsKey(key_code))
        {
            m_key_input.put(key_code, KeyState.RELEASED);
        }

        return (KeyState)m_key_input.get(key_code) == key_state;
    }

    public boolean isKeyPressed(int key_code)
    {
        return isKeyState(key_code, KeyState.PRESSED);
    }

    public boolean isKeyReleased(int key_code)
    {
        return isKeyState(key_code, KeyState.RELEASED);
    }
}
