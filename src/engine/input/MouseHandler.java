/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.input;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;

/**
 *
 * @author michael
 */
public class MouseHandler 
    implements MouseListener, MouseMotionListener
{
    private HashMap m_mouse_input;
    private Point m_position;
    
    private HashMap m_mouse_input_backup;
    private boolean m_is_outside_component = false;
    private boolean m_restore_mouse_input = true;

    public enum MouseState
    {
        PRESSED,
        RELEASED,
    };

    public MouseHandler()
    {
        m_mouse_input = new HashMap();
        m_mouse_input_backup = new HashMap();
        m_position = new Point();
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        //Do nothing, this is bad.
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        setMouseState(e.getButton(), MouseState.PRESSED);
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        setMouseState(e.getButton(), MouseState.RELEASED);
        if (m_is_outside_component)
        {
            m_restore_mouse_input = false;
        }
    }
    
    @Override
    public void mouseEntered(MouseEvent e) 
    {
        m_is_outside_component = false;
        
        if (m_restore_mouse_input)
        {
            m_mouse_input = m_mouse_input_backup;
        }
    }

    @Override
    public void mouseExited(MouseEvent e) 
    {
        m_is_outside_component = true;
        m_restore_mouse_input = true;
        m_mouse_input_backup = (HashMap)m_mouse_input.clone();
        m_mouse_input.clear();
    }
    
    @Override
    public void mouseDragged(MouseEvent me)
    {
        m_position = me.getPoint();
    }

    @Override
    public void mouseMoved(MouseEvent me)
    {
        m_position = me.getPoint();
    }

    private void setMouseState(int mouse_button_code, MouseState mouse_state)
    {
        m_mouse_input.put(mouse_button_code, mouse_state);
    }

    private boolean isMouseState(int mouse_button_code, MouseState mouse_state)
    {
        if (!m_mouse_input.containsKey(mouse_button_code))
        {
            m_mouse_input.put(mouse_button_code, MouseState.RELEASED);
        }

        return (MouseState)m_mouse_input.get(mouse_button_code) == mouse_state;
    }

    public boolean isMouseButtonPressed(int mouse_button_code)
    {
        return isMouseState(mouse_button_code, MouseState.PRESSED);
    }

    public boolean isMouseButtonReleased(int mouse_button_code)
    {
        return isMouseState(mouse_button_code, MouseState.RELEASED);
    }
    
    public Point getPosition()
    {
        return m_position;
    }
    
    public boolean isMouseInComponent()
    {
        return !m_is_outside_component;
    }
}
