/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import engine.input.InputHandler;
import engine.ui.UIManager;
import engine.views.View;
import engine.views.ViewManager;
import game.Game;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 *
 * @author Michael
 */
public class Engine extends JPanel
{    
    private final static int TARGET_FREQUENCY = 1000/60; // Millseconds alloted per frame.
    private static final Semaphore m_update_signal = new Semaphore(1);
    //private static final Semaphore m_draw_signal = new Semaphore(1);
    
    private View m_current_view;
    
    private boolean m_is_game_running;
    private boolean m_is_drawing = false;
    
    private long m_total_elapsed_time;
    private int m_elapsed_time;
    private long m_last_update_time;
    private int m_num_frames;
    private int m_fps;
    
    public Engine()
    {
        m_last_update_time = System.currentTimeMillis();
        m_is_game_running = true;
    }
    
    private void initialize()
    {
        JFrame game_container = new JFrame("SENG 271 Game");
        JPanel content_pane = (JPanel) game_container.getContentPane();
        
        // On windows machines, the panel containing the game is
        // 10 pixels greater than the game screen.
        if (System.getProperty("os.name").startsWith("Windows"))
        {
            content_pane.setPreferredSize(new Dimension(Game.SCREEN_WIDTH - 10, Game.SCREEN_HEIGHT - 10));
        }
        else
        {
            content_pane.setPreferredSize(new Dimension(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT));
        }
        
        content_pane.setLayout(null);
        
        JLayeredPane game_panel = new JLayeredPane();
        game_panel.setBounds(0, 0, Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT);
        content_pane.add(game_panel);
        
        setBounds(0, 0, Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT);
        setFocusable(true);
        setDoubleBuffered(true);
        
        game_panel.add(this, new Integer(0), 0);
        game_container.pack();
        game_container.setResizable(false);
        game_container.setVisible(true);
        
        game_container.addWindowListener(
                new WindowAdapter() 
                {
                        @Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
        
        addKeyListener(InputHandler.getInstance().getKeyHandler());
        addMouseListener(InputHandler.getInstance().getMouseHandler());
        addMouseMotionListener(InputHandler.getInstance().getMouseHandler());
        
        UIManager.getInstance().initialize(game_panel);
    }
    
    private void captureSignal(Semaphore signal)
    {
        try 
        {
            signal.acquire();
        } 
        catch (InterruptedException ex) 
        {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getStackTrace());
            System.exit(0);
        }
    }
    
    public void run()
    {
        m_current_view = ViewManager.getInstance().getCurrentView();
        View previous_view = m_current_view;
        
        while (m_is_game_running)
        {
            captureSignal(m_update_signal);
            
            GameTime game_time = setTargetFPS();
            updateCoreSystems(game_time);
            
            m_current_view.update(game_time);
            UIManager.getInstance().update(game_time);
            m_current_view = ViewManager.getInstance().getCurrentView();
            
            // Make sure that if views have changed, that they update
            // at least once before rendering.
            if (m_current_view.equals(previous_view))
            {
                //captureSignal(m_draw_signal);
                
                m_is_drawing = true;
                repaint();
                
                //m_draw_signal.release();
            }
            else
            {
                m_update_signal.release();
            }
            
            previous_view = m_current_view;
        }
        
        // Clean up view.
        m_current_view.destroy();
    }
    
    private void updateCoreSystems(GameTime game_time)
    {
        InputHandler.getInstance().update(game_time);
    }
    
    @Override
    public void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);
        
       // captureSignal(m_draw_signal);
        
        if (!m_is_drawing || m_current_view == null)
        {
            //m_draw_signal.release();
            return;
        }
        
        m_current_view.draw(graphics);
        UIManager.getInstance().draw(graphics);
        
        graphics.dispose();
        
        m_is_drawing = false;
        
      //  m_draw_signal.release();
        m_update_signal.release();
    }
    
    private void destroy()
    {
        // TODO: Perform clean-up before exiting the engine.
        
        InputHandler.getInstance().destroy();
        AudioManager.getInstance().destroy();
        ImageManager.getInstance().destroy();
        ViewManager.getInstance().destroy();
    }
    
    private GameTime setTargetFPS()
    {
        long current_time = System.currentTimeMillis();
        long delta_time = current_time - m_last_update_time;

        if (delta_time < TARGET_FREQUENCY)
        {
            sleep(TARGET_FREQUENCY - delta_time);
            delta_time = TARGET_FREQUENCY;
            current_time += TARGET_FREQUENCY - delta_time;
        }

        m_num_frames++;
        m_total_elapsed_time += delta_time;
        m_elapsed_time += delta_time;
        if (m_elapsed_time > 1000)
        {
            m_elapsed_time -= 1000;
            m_fps = m_num_frames;
            m_num_frames = 0;
        }
        
        m_last_update_time = current_time;
        
        return new GameTime(m_total_elapsed_time, delta_time, m_fps);
    }
    
    private void sleep(long time)
    {
        try
        {
            TimeUnit.MILLISECONDS.sleep(time);
        }
        catch (Exception e)
        {
            System.out.println("Error! Thread could not sleep. " + e.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Engine engine = new Engine();
        engine.initialize();
        engine.run();
        engine.destroy();
    }
}
