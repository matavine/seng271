/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

/**
 *
 * @author Geoff
 */
public interface IUpdateable 
{
    public void update(GameTime game_time);
}
