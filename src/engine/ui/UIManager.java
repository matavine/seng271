/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.ui;

import engine.GameTime;
import engine.IDrawable;
import engine.ISingleton;
import engine.IUpdateable;
import game.ui.*;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLayeredPane;

/**
 *
 * @author Michael
 */
public class UIManager 
    implements ISingleton, IUpdateable, IDrawable
{
    private static UIManager m_instance;
    public static UIManager getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new UIManager();
        }
        
        return m_instance;
    }
    
    private JLayeredPane m_game_panel;
    private HashMap<String, UI> m_ui;
    private List<UI> m_active_ui;
    
    private boolean m_is_updating = false;
    private List<UI> m_add_queue;
    private List<UI> m_remove_queue;
    
    private UIManager()
    {
        m_ui = new HashMap<String, UI>();
        m_active_ui = new LinkedList<UI>();
        m_add_queue = new LinkedList<UI>();
        m_remove_queue = new LinkedList<UI>();
    }
    
    public void initialize(JLayeredPane game_panel)
    {
        m_game_panel = game_panel;
        registerUI();
    }
    
    public void resetUI()
    {
        m_ui.clear();
        hideAllUI();
        registerUI();
    }
    
    private void registerUI()
    {
        // Register the different UIs.
        m_ui.put("UnitUI", new UnitUI());
        m_ui.put("MiniMapUI", new MiniMapUI());
        m_ui.put("SettlerUI", new SettlerUI());
        m_ui.put("CityUI", new CityUI());
        m_ui.put("PlayerUI", new PlayerUI());
        m_ui.put("StartUI", new StartUI());
        m_ui.put("SetupUI", new SetupUI());
    }

    @Override
    public void destroy() 
    {
        m_instance = null;
    }
    
    public void displayUI(String ui_name, int layer_index, Object model)
    {
        if (!m_ui.containsKey(ui_name))
        {
            System.out.println("UI with the name '" + ui_name + "' does not exist.");
            return;
        }
        
        UI ui = m_ui.get(ui_name);
        if (ui.isAttached())
            return;
        
        ui.setModel(model);
        ui.attachComponents(m_game_panel, layer_index);
        
        if (m_is_updating)
        {
            m_add_queue.add(ui);
            return;
        }
        
        m_active_ui.add(ui);
    }
    
    public void hideUI(String ui_name)
    {
        if (!m_ui.containsKey(ui_name))
        {
            System.out.println("UI with the name '" + ui_name + "' does not exist.");
            return;
        }
        
        UI ui = m_ui.get(ui_name);
        if (!ui.isAttached())
            return;
        
        ui.detachComponents(m_game_panel);
        
        if (m_is_updating)
        {
            m_remove_queue.add(ui);
            return;
        }
        
        m_active_ui.remove(ui);
    }
    
    public void hideAllUI()
    {
        if (m_is_updating)
        {
            m_remove_queue = new LinkedList<UI>(m_active_ui);
            return;
        }
        
        for (UI ui : m_active_ui)
        {
            ui.detachComponents(m_game_panel);
        }
        
        m_active_ui.clear();
    }

    @Override
    public void update(GameTime game_time) 
    {
        m_is_updating = true;
        
        for (UI ui : m_active_ui)
        {
            ui.update(game_time);
        }
        
        for (UI ui : m_add_queue)
        {
            m_active_ui.add(ui);
        }
        
        for (UI ui : m_remove_queue)
        {
            m_active_ui.remove(ui);
        }
        
        m_add_queue.clear();
        m_remove_queue.clear();
        m_is_updating = false;
    }

    @Override
    public void draw(Graphics graphics) 
    {
        for (UI ui : m_active_ui)
        {
            ui.draw(graphics);
        }
    }
}
