/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.ui;

import engine.IDrawable;
import engine.IUpdateable;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLayeredPane;

/**
 *
 * @author Michael
 */
public abstract class UI<T> 
    implements ActionListener, IUpdateable, IDrawable
{
    protected T m_model;
    protected List<Component> m_components = new LinkedList<Component>();;
    private boolean m_is_attached = false;
    
    public void setModel(T model)
    {
        m_model = model;
        onSetModel();
    }
    
    protected void onSetModel()
    {
        // Do nothing.
    }
    
    protected JButton createButton(String name)
    {
        JButton button = new JButton(name);
        button.setFocusable(false);
        button.addActionListener(this);
        m_components.add(button);
        
        return button;
    }
    
    public boolean isAttached()
    {
        return m_is_attached;
    }
    
    public void attachComponents(JLayeredPane game_panel, int layer_index)
    {        
        for (Component component : m_components)
        {
            game_panel.add(component, new Integer(layer_index));
        }
        
        m_is_attached = true;
        
        game_panel.validate();
    }
    
    public void detachComponents(JLayeredPane game_panel)
    {        
        for (Component component : m_components)
        {
            game_panel.remove(component);
        }
        
        m_model = null;
        m_is_attached = false;
        game_panel.validate();
    }
}
