/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.GameTime;
import engine.IDrawable;
import engine.IUpdateable;
import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 * This class is responsible for handling mini map related functionality. This combines with the mini map view for rendering.
 * @author Michael
 */
public class MiniMap
    implements IUpdateable, IDrawable, IPlayerListener, ITurnListener
{   
    private static final int CAMERA_COLOR = 0xFFFFFF00;
    private static final int FADE_FACTOR = 2;
    
    private final static int MINI_MAP_WIDTH = 256;
    private int m_mini_map_height = 256;
    
    private BufferedImage m_mini_map_front_buffer;
    private BufferedImage m_mini_map_back_buffer;
    
    private ImageIcon m_mini_map;
    private Map m_map;
    
    private boolean m_recalculate = true;
    private boolean m_update_ui = false;
    
    /**
     * Used to initialize a mini map.
     * @param map The map to initialize from.
     */
    public MiniMap(Map map)
    {
        m_recalculate = true;
        m_map = map; //TODO: Check if this causes creepy memory leaks... I feel like this will be okay though.
        createBuffer();
        
        Game.getInstance().addTurnListener(this);
    }
    
    /**
     * Used to create the image buffer painted to by the mini map.
     */
    private void createBuffer()
    {
        m_mini_map_height = (int)(MINI_MAP_WIDTH * ((float)m_map.getMapHeight() / m_map.getMapWidth()));
        m_mini_map_front_buffer = new BufferedImage(MINI_MAP_WIDTH, m_mini_map_height, BufferedImage.TYPE_INT_ARGB);
        m_mini_map_back_buffer = new BufferedImage(MINI_MAP_WIDTH, m_mini_map_height, BufferedImage.TYPE_INT_ARGB);
        m_mini_map = new ImageIcon(m_mini_map_front_buffer);
    }
    
    /**
     * Used as a one off render of the map in miniature form for display. Usually for map selection.
     * @param width The width of the mini map in pixels.
     * @param height The height of the mini map in pixels.
     * @param map The map to draw.
     * @param is_random True if the map is generated.
     * @return An image icon containing the mini map.
     */
    public static ImageIcon createMiniMap(int width, int height, Map map, boolean is_random)
    {
        BufferedImage tmp_buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (is_random)
                {
                    int tile_colour = 0xFF42476B;
                    tmp_buffer.setRGB(x, y, tile_colour);
                    continue;
                }
                
                int row = (int)(((float)y / height) * map.getMapHeight());
                int col = (int)(((float)x / width) * map.getMapWidth());
                
                int tile_colour = 0xFF000000;
                
                Tile tile = map.getTile(col, row);
                Tile.TileType tile_type = tile.getType();

                switch (tile_type)
                {
                    case WATER:
                        tile_colour = 0xFF00AEF0;
                        break;
                    case PLAINS:
                        tile_colour = 0xFF7CB24B;
                        break;
                    case FOREST:
                        tile_colour = 0xFF2D7418;
                        break;
                    case DESERT:
                        tile_colour = 0xFFD5CC90;
                        break;
                    case MOUNTAIN:
                        tile_colour = 0xFF787878;
                        break;
                    case GLACIER:
                        tile_colour = 0xFFC9ECF1;
                        break;
                    case HILL:
                        tile_colour = 0xFF8CA05F;
                        break;
                    case TUNDRA:
                        tile_colour = 0xFF85AB75;
                        break;
                    case GRASS:
                        tile_colour = 0xFF36A024;
                        break;
                    case JUNGLE:
                        tile_colour = 0xFF00C000;
                        break;
                    case SWAMP:
                        tile_colour = 0xFF22A07B;
                        break;
                }
                
                tmp_buffer.setRGB(x, y, tile_colour);
            }
        }
        
        if (is_random)
        {
            Graphics graphics = tmp_buffer.getGraphics();
            graphics.setColor(Color.WHITE);
            ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            graphics.setFont(new Font("Trebuchet MS", Font.BOLD, 96));
            graphics.drawString("?", (width / 2) - 15, (height / 2) + 30);
            ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            graphics.dispose();
        }
        
        return new ImageIcon(tmp_buffer);
    }
    
    /**
     * Used to paint the map to the image buffer.
     */
    private void paintMap()
    {
        for (int y = 0; y < m_mini_map_height; y++)
        {
            for (int x = 0; x < MINI_MAP_WIDTH; x++)
            {
                int row = (int)(((float)y / m_mini_map_height) * m_map.getMapHeight());
                int col = (int)(((float)x / MINI_MAP_WIDTH) * m_map.getMapWidth());
                
                int tile_colour = 0xFF000000;
                
                if(Game.getInstance().getPlayer().hasDiscoveredTile(Game.getInstance().getMap().mapToWorld(col, row)))
                {
                    Tile tile = m_map.getTile(col, row);
                    Tile.TileType tile_type = tile.getType();
                
                    switch (tile_type)
                    {
                        case WATER:
                            tile_colour = 0xFF00AEF0;
                            break;
                        case PLAINS:
                            tile_colour = 0xFF7CB24B;
                            break;
                        case FOREST:
                            tile_colour = 0xFF2D7418;
                            break;
                        case DESERT:
                            tile_colour = 0xFFD5CC90;
                            break;
                        case MOUNTAIN:
                            tile_colour = 0xFF787878;
                            break;
                        case GLACIER:
                            tile_colour = 0xFFC9ECF1;
                            break;
                        case HILL:
                            tile_colour = 0xFF8CA05F;
                            break;
                        case TUNDRA:
                            tile_colour = 0xFF85AB75;
                            break;
                        case GRASS:
                            tile_colour = 0xFF36A024;
                            break;
                        case JUNGLE:
                            tile_colour = 0xFF00C000;
                            break;
                        case SWAMP:
                            tile_colour = 0xFF22A07B;
                            break;
                    }
                    
                    if(tile.getVisibility() == Tile.TileVisibility.FADED)
                    {
                        int red = (tile_colour & 0x00FF0000)/FADE_FACTOR & 0x00FF0000;
                        int green = (tile_colour & 0x0000FF00)/FADE_FACTOR & 0x0000FF00;
                        int blue = (tile_colour & 0x000000FF)/FADE_FACTOR & 0x000000FF;
                        tile_colour = 0xFF000000 + red + green + blue;
                    }
                }
                
                m_mini_map_back_buffer.setRGB(x, y, tile_colour);
            }
        }
    }
    
    /**
     * Used to paint the camera bounds to the mini map.
     */
    private void paintCamera()
    {
        Point camera_tl = Game.getInstance().getCamera().getPosition();
        
        Point camera_br = Game.getInstance().getCamera().getPosition();
        camera_br.translate(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT);
        
        camera_tl = new Point(
            (int)(((float)camera_tl.x / m_map.getMapPixelWidth()) * MINI_MAP_WIDTH),
            (int)(((float)camera_tl.y / m_map.getMapPixelHeight()) * m_mini_map_height));
        
        camera_br = new Point(
            (int)(((float)camera_br.x / m_map.getMapPixelWidth()) * MINI_MAP_WIDTH),
            (int)(((float)camera_br.y / m_map.getMapPixelHeight()) * m_mini_map_height));
        
        int x = camera_tl.x;
        int y;
        for (y = camera_tl.y; y < camera_br.y; y++)
        {
            m_mini_map_back_buffer.setRGB(x%MINI_MAP_WIDTH, y%m_mini_map_height, CAMERA_COLOR);
        }
        x = camera_br.x -1;
        for (y = camera_tl.y; y < camera_br.y; y++)
        {
            m_mini_map_back_buffer.setRGB(x%MINI_MAP_WIDTH, y%m_mini_map_height, CAMERA_COLOR);
        }
        
        y = camera_tl.y;
        for (x = camera_tl.x+1; x < camera_br.x-1; x++)
        {
            m_mini_map_back_buffer.setRGB(x%MINI_MAP_WIDTH, y%m_mini_map_height, CAMERA_COLOR);
        }
        
        y = camera_br.y -1;
        for (x = camera_tl.x+1; x < camera_br.x-1; x++)
        {
            m_mini_map_back_buffer.setRGB(x%MINI_MAP_WIDTH, y%m_mini_map_height, CAMERA_COLOR);
        }
    }
    
    /**
     * Used to paint visible units on the mini map.
     */
    private void paintUnits()
    {
        for(Player player : Game.getInstance().getPlayers())
        {
            for(Unit unit : player.getUnits())
            {
                if(Game.getInstance().getPlayer().hasDiscoveredTile(unit.getTile().getPosition()));
                {
                    Tile tile = m_map.getTile(unit.getPosition());
                    int tile_colour = 0xFF000000;

                    if(Game.getInstance().getPlayer().hasDiscoveredTile(unit.getPosition()))
                    {
                        tile_colour = player.getColor().getRGB();

                        if(tile.getVisibility() == Tile.TileVisibility.FADED)
                        {
                            continue;
                        }
                    }

                    Point tile_index = m_map.getTileIndex(tile.getCenterPosition());
                    int x = (int)((tile_index.x/(float)m_map.getMapWidth())*MINI_MAP_WIDTH);
                    int y = (int)((tile_index.y/(float)m_map.getMapHeight())*m_mini_map_height);
                    
                    for(int i = y; i<y+(m_mini_map_height/m_map.getMapHeight()); i++)
                    {
                        if(i >= m_mini_map_height) break;
                        for(int j = x; j<x+(MINI_MAP_WIDTH/m_map.getMapWidth()); j++)
                        {
                            if(j >= MINI_MAP_WIDTH) break;
                            m_mini_map_back_buffer.setRGB(j, i, tile_colour);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Used to paint known cities on the mini map.
     */
    private void paintCities()
    {
        for(Player player : Game.getInstance().getPlayers())
        {
            for(City city : player.getCities())
            {
                if(Game.getInstance().getPlayer().hasDiscoveredTile(city.getTile().getPosition()));
                {
                    Tile tile = m_map.getTile(city.getPosition());
                    int tile_colour = 0xFF000000;

                    if(Game.getInstance().getPlayer().hasDiscoveredTile(city.getPosition()))
                    {
                        tile_colour = player.getColor().getRGB();

                        if(tile.getVisibility() == Tile.TileVisibility.FADED)
                        {
                            int red = (tile_colour & 0x00FF0000)/FADE_FACTOR & 0x00FF0000;
                            int green = (tile_colour & 0x0000FF00)/FADE_FACTOR & 0x0000FF00;
                            int blue = (tile_colour & 0x000000FF)/FADE_FACTOR & 0x000000FF;
                            tile_colour = 0xFF000000 + red + green + blue;
                        }
                    }

                    Point tile_index = m_map.getTileIndex(tile.getCenterPosition());
                    int x = (int)((tile_index.x/(float)m_map.getMapWidth())*MINI_MAP_WIDTH);
                    int y = (int)((tile_index.y/(float)m_map.getMapHeight())*m_mini_map_height);
                    
                    for(int i = y; i<y+(m_mini_map_height/m_map.getMapHeight()); i++)
                    {
                        if(i >= m_mini_map_height) break;
                        for(int j = x; j<x+(MINI_MAP_WIDTH/m_map.getMapWidth()); j++)
                        {
                            if(j >= MINI_MAP_WIDTH) break;
                            m_mini_map_back_buffer.setRGB(j, i, tile_colour);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Used to redraw the mini map.
     */
    public void recalculate()
    {
        m_recalculate = true;
    }
    
    /**
     * Used to get a copy of the current mini map image.
     * @return 
     */
    public ImageIcon getMiniMapImage()
    {
        return m_mini_map;
    }
    
    /**
     * Used to get the dimensions of the current mini map.
     * @return The dimensions of the current mini map.
     */
    public Point getMiniMapDimensions()
    {
        return new Point(MINI_MAP_WIDTH, m_mini_map_height);
    }
    
    /**
     * Used to swap mini map buffers, to prevent rendering of half completed mini maps.
     */
    private void swapBuffers()
    {
        BufferedImage tmp = m_mini_map_front_buffer;
        m_mini_map_front_buffer = m_mini_map_back_buffer;
        m_mini_map_back_buffer = tmp;

        m_mini_map = new ImageIcon(m_mini_map_front_buffer);
        setUpdateUI(true);
    }
    
    /**
     * Used check if the UI should be updated with map changes.
     * @return The boolean dictating if UI should be updated.
     */
    public boolean getUpdateUI()
    {
        return m_update_ui;
    }
    
    /**
     * Used to set the update UI value, dictating if the UI should be updated or not.
     * @param value True if the UI should be updated.
     */
    public void setUpdateUI(boolean value)
    {
        m_update_ui = value;
    }
    
    /**
     * Used to update the mini map each frame.
     * @param game_time The object containing game specific time stats.
     */
    @Override
    public void update(GameTime game_time)
    {   
        if(m_recalculate)
        {
            m_mini_map_back_buffer = new BufferedImage(MINI_MAP_WIDTH, m_mini_map_height, BufferedImage.TYPE_INT_ARGB);
            
            paintMap();
            if(Game.SCREEN_WIDTH < Game.getInstance().getMap().getMapPixelWidth() 
                    && Game.SCREEN_HEIGHT < Game.getInstance().getMap().getMapPixelHeight())
            {
                paintCamera();
            }
            
            paintUnits();
            paintCities();
            swapBuffers();
        }
        
        m_recalculate = false;
    }
    
    /**
     * Used to draw the mini map (No longer used)
     * @param graphics The graphics object to draw to.
     */
    @Override
    public void draw(Graphics graphics)
    {
    }
    
    /**
     * Called only when a unit's position changes. Causes a redraw.
     */
    @Override
    public void onUnitMove() 
    {
        recalculate();
    }
    
    /**
     * Called when a player's turn begins. Causes a redraw.
     */
    @Override
    public void onBeginTurn() 
    {
        recalculate();
    }
    
    /**
     * Called when a player's turn ends.
     */
    @Override
    public void onEndTurn() 
    {
        // Do nothing.
    }
}
