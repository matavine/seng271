/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.Camera;
import engine.GameTime;
import engine.input.InputHandler;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * The player camera extends the camera object to create player controlled cameras within aciv.
 * @author Geoff
 */
public class PlayerCamera
    extends Camera
{
    /**
     * The speed at which the camera pans per frame.
     */
    public static float drag_speed = 0.5f;

    private Point m_initial_camera_position;
    private Point m_initial_mouse_position;
    
    /**
     * Used to convert from a world space position to a screen space position.
     * @param position The world space position to convert from.
     * @return The provided position in screen space.
     */
    @Override
    public Point worldToScreenSpace(Point position)
    {
        Point new_position = (Point)position.clone();

        new_position.translate((m_position.x * -1), (m_position.y * -1));

        if(new_position.x >= Game.SCREEN_WIDTH + Game.getInstance().getMap().getTileWidth()) // Move to LHS
        {
            new_position.translate(Game.getInstance().getMap().getMapPixelWidth() * -1, 0);
        }
        else if(new_position.x <= Game.getInstance().getMap().getTileWidth()*2 * -1) // Move to RHS
        {
            new_position.translate(Game.getInstance().getMap().getMapPixelWidth(), 0);
        }

        return new_position;
    }
    
    /**
     * Used to convert from screen space to world space.
     * @param position A position in screen space to convert.
     * @return The provided position in world space.
     */
    @Override
    public Point screenSpaceToWorld(Point position)
    {
        Point new_position = (Point)position.clone();

        new_position.translate(m_position.x, m_position.y);

        if(new_position.x < 0.0) // LHS
        {
            new_position.translate(Game.getInstance().getMap().getMapPixelWidth(), 0);
        }
        else if(new_position.x >= Game.getInstance().getMap().getMapPixelWidth()) // RHS
        {
            new_position.translate(Game.getInstance().getMap().getMapPixelWidth() * -1, 0);
        }

        return new_position;
    }
    
    /**
     * Used to prevent the camera from being in an impossible location.
     * @param position The position that the camera is being moved to.
     * @return The nearest acceptable position after constraints.
     */
    @Override
    protected Point constrainPosition(Point position)
    {
        position = (Point)position.clone();
        if(Game.SCREEN_HEIGHT < Game.getInstance().getMap().getMapPixelHeight())
        {
            if(position.y < 0)
            {
                position.y = 0;
            }
            if(position.y > Game.getInstance().getMap().getMapPixelHeight() - Game.SCREEN_HEIGHT)
            {
                position.y = Game.getInstance().getMap().getMapPixelHeight() - Game.SCREEN_HEIGHT;
            }
        }
        else
        {
            position.y = 0;
        }
        
        if(Game.SCREEN_WIDTH < Game.getInstance().getMap().getMapPixelWidth())
        {
            if(position.x < 0)
            {
                position.x = Game.getInstance().getMap().getMapPixelWidth() + position.x%Game.getInstance().getMap().getMapPixelWidth();
            }
            if(position.x > Game.getInstance().getMap().getMapPixelWidth())
            {
                position.x = position.x%Game.getInstance().getMap().getMapPixelWidth();
            }
        }
        else
        {
            position.x = 0;
        }
        
        if (!position.equals(m_previous_position))
        {
            Game.getInstance().getMiniMap().recalculate();
        }
        
        return position;
    }
    
    /**
     * Used to update camera logic each frame. This takes care of smooth camera movement and camera controls.
     * @param game_time The set of game time related values.
     */
    @Override
    public void update(GameTime game_time)
    {
        switch(m_state)
        {
            case PANNING:
                m_previous_position = (Point)m_position.clone();

                if(Game.getInstance().getMap().getWorldDistance(m_position, m_pan_target) < 6)
                {
                    m_position = m_pan_target;
                    m_position = constrainPosition(m_position);
                    m_state = CameraState.NONE;
                }
                else
                {
                    Point pos_difference = Game.getInstance().getMap().getWorldDifference(m_position, m_pan_target);
                    
                    m_position.translate((int)(pos_difference.x * m_pan_speed), (int)(pos_difference.y * m_pan_speed));
                    m_position = constrainPosition(m_position);
                }
                // This bleeds over on purpose.
            
            case NONE:
                if(InputHandler.getInstance().isInputPressed(InputHandler.InputType.MOUSE, MouseEvent.BUTTON1))
                {
                    m_initial_camera_position = getPosition();
                    m_initial_mouse_position = InputHandler.getInstance().getMouseHandler().getPosition();
                    m_state = CameraState.DRAGGING;
                }
                else
                {
                    int delta_time = (int)game_time.getTimeSinceLastUpdate();
                    
                    if(InputHandler.getInstance().isInputDown(InputHandler.InputType.KEY, KeyEvent.VK_UP))
                    {
                        translatePosition(0, (int)(drag_speed * -1 * delta_time));
                    }
                    if(InputHandler.getInstance().isInputDown(InputHandler.InputType.KEY, KeyEvent.VK_DOWN))
                    {
                        translatePosition(0, (int)(drag_speed  * delta_time));
                    }
                    if(InputHandler.getInstance().isInputDown(InputHandler.InputType.KEY, KeyEvent.VK_RIGHT))
                    {
                        translatePosition((int)(drag_speed  * delta_time), 0);
                    }
                    if(InputHandler.getInstance().isInputDown(InputHandler.InputType.KEY, KeyEvent.VK_LEFT))
                    {
                        translatePosition((int)(drag_speed * -1  * delta_time), 0);
                    }
                }
                break;
                
            case DRAGGING:
                if(InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.MOUSE, MouseEvent.BUTTON1))
                {
                    m_state = CameraState.NONE;
                }
                else
                {
                    Point mouse_position = InputHandler.getInstance().getMouseHandler().getPosition();
                    Point new_position = new Point(m_initial_camera_position.x, m_initial_camera_position.y);
                    new_position.translate(mouse_position.x * -1, mouse_position.y * -1);
                    new_position.translate(m_initial_mouse_position.x, m_initial_mouse_position.y);
                    setPosition(new_position);
                }
                break;

            default:
                break;
        }
    }
}