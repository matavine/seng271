/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.GameTime;
import engine.IDrawable;
import engine.IUpdateable;
import engine.ImageManager;
import engine.input.InputHandler;
import engine.ui.UIManager;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Ben Cunningham
 */
public class Player
    implements IUpdateable, IDrawable, ITurnListener
{
    private static final Image SELECTED_TILE = ImageManager.getInstance().getSprite("images/SelectedHex.png");
    
    private String m_name;
    private Rectangle2D m_name_bounds;
    
    private List<Unit> m_units;
    private List<City> m_cities;
    private Tile m_selected_tile;
    private Tile m_hovered_tile;
    private Map m_map;
    private Age m_age;
    private int m_gold;
    private Color m_color;
    private String m_color_extension;
    
    private PlayerCamera m_camera;
    private Point m_prev_mouse_pos;
    private boolean m_players_turn;
    private boolean m_lmb_lock;
    
    private HashMap<Point, Boolean> m_discovered_tiles;
    private List<IPlayerListener> m_listeners;
    private boolean m_locked = true;
    
    private boolean m_is_in_game = true;
    private boolean m_turn_ended = false;
    
    /**
     * Used to old the level of technology for the player.
     */
    public enum Age
    {
        STONE, BRONZE, IRON, STEEL, GUNPOWDER;
    };
    
    /**
     * Used to create a new player.
     * @param map The map object that the game exists in.
     * @param age The starting technology age for the player.
     * @param color The specific color used to represent the player.
     * @param name The players name.
     */
    public Player(Map map, Age age, Color color, String name)
    {
        m_name = name;
        m_map = map;
        m_age = age;
        m_units = new ArrayList();
        m_cities = new ArrayList();
        m_selected_tile = null;
        m_discovered_tiles = new HashMap<Point, Boolean>();
        m_color = color;
        m_listeners = new ArrayList<IPlayerListener>();
        m_color_extension = color.toString();
        
        m_lmb_lock = false;
        
        m_camera = new PlayerCamera();
    }
    
    /**
     * Used to get the boundaries of the players name.
     * @return a rectangle object representing the bounds.
     */
    public Rectangle2D getNameBounds()
    {
        return m_name_bounds;
    }
    
    /**
     * Used to set the boundaries of the players name.
     * @param bounds The rectangle representing the bounds.
     */
    public void setNameBounds(Rectangle2D bounds)
    {
        m_name_bounds = bounds;
    }
    
    /**
     * Used to get the player's name.
     * @return string containing the player's name.
     */
    public String getName()
    {
        return m_name;
    }
    
    /**
     * Used to add a player listener, allowing the player to inform that listener of important events.
     * @param listener The object to send event messages to.
     */
    public void addPlayerListener(IPlayerListener listener)
    {
        if (listener != null)
        {
            m_listeners.add(listener);
        }
    }
    
    /**
     * Used to inform that game that a unit has moved, required for updating minimap, fog of war and other graphics related things.
     */
    public void broadcastUnitMove()
    {
        for (IPlayerListener listener : m_listeners)
        {
            listener.onUnitMove();
        }
    }
    
    /**
     * Used to spawn the player's initial units.
     * @param tile The tile to spawn the player on.
     */
    public void spawn(Tile tile)
    {
        m_discovered_tiles.clear();
        m_units.clear();
        
        Unit starting_unit = UnitFactory.getInstance().makeUnit("Settler", this, tile);
        tile.setUnit(starting_unit);
        m_selected_tile = tile;
        addUnit(starting_unit);
    }
    
    /**
     * Used to spawn a unit on the player's selected tile.
     * @param type A string representing the type of unit to spawn.
     */
    public void spawnUnit(String type)
    {
        Point tile_index = Game.getInstance().getMap().getTileIndex(m_selected_tile.getCenterPosition());
        Tile tile = Game.getInstance().getMap().getTile(tile_index.x - 1, tile_index.y - 1);
        
        if(tile.getUnit() == null)
        {
            Unit unit = UnitFactory.getInstance().makeUnit(type, this, tile);
            addUnit(unit);
            tile.setUnit(unit);
        }
    }
    
    /**
     * Used to check if a player has discovered a tile.
     * @param tile_pos The position to check.
     * @return True if the tile has been discovered.
     */
    public boolean hasDiscoveredTile(Point tile_pos)
    {
        if (!m_discovered_tiles.containsKey(tile_pos))
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * Used to inform the player that they have discovered a tile.
     * @param tile_pos The position of the discovered tile.
     */
    public void setDiscoveredTile(Point tile_pos)
    {
        m_discovered_tiles.put(tile_pos, Boolean.TRUE);
        Game.getInstance().getMiniMap().recalculate();
    }
    
    /**
     * Used to get the player's units.
     * @return The player's units.
     */
    public List<Unit> getUnits()
    {
        return m_units;
    }
    
    /**
     * Used to get a list of the player's cities.
     * @return The player's cities.
     */
    public List<City> getCities()
    {
        return m_cities;
    }
    
    /**
     * Used to get the player's playerCamera instance.
     * @return The player's camera.
     */
    public PlayerCamera getCamera()
    {
        return m_camera;
    }
    
    /**
     * Used to get the player's color.
     * @return A color object specific to the player.
     */
    public Color getColor()
    {
        return m_color;
    }
    
    /**
     * Used to get the string extension for images that have been set to this player's color.
     * @return a string holding the image name extension for this player's color.
     */
    public String getColorExtension()
    {
        return m_color_extension;
    }
    
    /**
     * Adds a unit to the player's unit list if the unit is not already in it.
     * @param unit The unit to add to the player's list.
     */
    public void addUnit(Unit unit)
    {
        if(!m_units.contains(unit))
        {
            m_units.add(unit);
        }
    }
    
    /**
     * Removes a unit from the player's unit list, if the unit is in the list.
     * @param unit The unit to be removed from the player's list.
     */
    public void removeUnit(Unit unit)
    {
        m_units.remove(unit);
        
        if(m_selected_tile == null)
        {
            return;
        }
        
        if(unit == m_selected_tile.getUnit())
        {
            if(m_players_turn)
            {
                enableSelectedTile(false);
            }
            m_selected_tile = null;
        }
    }
    
    /**
     * Adds a city to the player's city list if the city is not already in it.
     * @param city The city to add to the player's list.
     */
    public void addCity(City city)
    {
        if(!m_cities.contains(city))
        {
            m_cities.add(city);
        }
    }
    
    /**
     * Removes a city from the player's city list, if the city is in the list.
     * @param city The city to be removed from the player's list.
     */
    public void removeCity(City city)
    {
        m_cities.remove(city);
    }
    
    /**
     * Used to check which age of technology the player is in.
     * @return The player's age of technology.
     */
    public Age getAge()
    {
        return m_age;
    }
    
    /**
     * Used to set the player's selected tile.
     * @param t The tile to force the player to select.
     */
    public void setSelectedTile(Tile t)
    {
        if(t == m_selected_tile)
        {
            return;
        }
        
        enableSelectedTile(false);
        m_selected_tile = t;
        enableSelectedTile(true);
    }
    
    /**
     * Used to ensure that the players selected tile is in the correct state.
     * @param value the state to set the players selected tile to.
     */
    private void enableSelectedTile(boolean value)
    {
        if(m_selected_tile != null)
        {
            if (m_selected_tile.getUnit() != null && m_selected_tile.getUnit().getPlayer() == this)
            {
                m_selected_tile.getUnit().setSelected(value);
            }
            
            if (m_selected_tile.getCity() != null && m_selected_tile.getCity().getPlayer() == this)
            {
                m_selected_tile.getCity().setSelected(value);
            }
        }
    }
    
    /**
     * Used to get the player's selected tile.
     * @return The tile the player currently has selected.
     */
    public Tile getSelectedTile()
    {
        return m_selected_tile;
    }
    
    /**
     * Used to get the tile that the player's mouse is currently over.
     * @return The tile that the mouse is currently over.
     */
    public Tile getHoveredTile()
    {
        return m_hovered_tile;
    }
    
    /**
     * Used to get the player's total gold amount.
     * @return The player's gold amount.
     */
    public int getGold()
    {
        return m_gold;
    }
    
    /**
     * Used to spend or reduce the player's gold.
     * @param amount The amount to reduce the player's gold by.
     */
    public void spendGold(int amount)
    {
        m_gold -= amount;
    }
    
    /**
     * Used to lock the left mouse button, in case a unit is using it.
     * @param lock True to lock the button.
     */
    public void lockLMB(boolean lock)
    {
        m_lmb_lock = lock;
    }
    
    /**
     * Used to check if the player is in game.
     * @return true if the player is in game.
     */
    public boolean isInGame()
    {
        return m_is_in_game;
    }
    
    /**
     * A call that informs the player their turn has just begun.
     */
    @Override
    public void onBeginTurn()
    {
        m_turn_ended = false;
        m_players_turn = true;
        m_locked = true;
        
        enableSelectedTile(false);
        UIManager.getInstance().displayUI("PlayerUI", 3, this);
    }
    
    /**
     * Used to check if the player is locked, or can process commands.
     * @return true is the player is locked and cannot make moves.
     */
    public boolean isLocked()
    {
        return m_locked;
    }
    
    /**
     * Used to unlock the player (triggers the beginning of a player's turn)
     */
    public void unlock()
    {
        m_locked = false;
        
        enableSelectedTile(true);
        for (Unit unit : m_units)
        {
            unit.onBeginTurn();
        }
        
        for (City city : m_cities)
        {
            m_gold += city.getIncome();
        }
        
        UIManager.getInstance().displayUI("MiniMapUI", 1, Game.getInstance().getMiniMap());
    }
    
    /**
     * A call that informs the player their turn has just ended.
     */
    @Override
    public void onEndTurn()
    {
        m_players_turn = false;
        m_lmb_lock = false;
        enableSelectedTile(false);
        
        //UIManager.getInstance().hideUI("PlayerUI");
        
        for (Unit unit : m_units)
        {
            unit.onEndTurn();
        }
        
        UIManager.getInstance().hideAllUI();
    }
    
    /**
     * Informs the player that their turn has ended.
     */
    public void signalEndTurn()
    {
        m_turn_ended = true;
    }
    
    /**
     * Ends the current player's turn.
     */
    private void endTurn()
    {
        Game.getInstance().endTurn();
    }
    
    /**
     * Used to check if this player is still in the game.
     */
    public void updatePlayerStillInGame()
    {
        m_is_in_game = false;
        if (m_cities.isEmpty())
        {
            for (Unit unit : m_units)
            {
                if (unit instanceof Settler)
                {
                    m_is_in_game = true;
                    return;
                }
            }
            
            Game.getInstance().playerEliminated();
        }
        else
        {
            m_is_in_game = true;
        }
    }
    
    /**
     * Handles the players logic per frame.
     * @param game_time The game_time object from the engine (holds delta time)
     */
    @Override
    public void update(GameTime game_time)
    {
        if (m_turn_ended)
        {
            endTurn();
            return;
        }
        
        if (m_cities.isEmpty())
        {
            m_is_in_game = false;
            for (Unit unit : m_units)
            {
                if (unit instanceof Settler)
                {
                    m_is_in_game = true;
                }
            }
        }
        
        if (m_locked)
        {
            return;
        }
        
        m_camera.update(game_time);
        
        Point mouse_pos = InputHandler.getInstance().getMousePosition().getLocation();
        m_hovered_tile = calculateHoveredTile(mouse_pos);
        
        if(!m_lmb_lock)
        {
            handleLeftClick(mouse_pos);
        }
        
        if(m_selected_tile != null && m_selected_tile.getUnit() != null && m_selected_tile.getUnit().getPlayer() == this)
        {
            m_selected_tile.getUnit().update(game_time);
        }
        if(m_selected_tile != null && m_selected_tile.getCity() != null && m_selected_tile.getCity().getPlayer() == this)
        {
            m_selected_tile.getCity().update(game_time);
        }
        
        if (InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_E))
        {
            endTurn();
        }
    }
    
    /**
     * Handles controls related to left click for the current player.
     * @param mouse_pos The current position of the mouse.
     */
    private void handleLeftClick(Point mouse_pos)
    {
        if (InputHandler.getInstance().isInputPressed(engine.input.InputHandler.InputType.MOUSE, MouseEvent.BUTTON1))
        {
            m_prev_mouse_pos = InputHandler.getInstance().getMousePosition().getLocation();
        }
        
        if (m_prev_mouse_pos != null && InputHandler.getInstance().isInputJustReleased(engine.input.InputHandler.InputType.MOUSE, MouseEvent.BUTTON1))
        {
            if(mouse_pos.distance(m_prev_mouse_pos) < 5)
            {
                if (m_hovered_tile != null && !m_hovered_tile.equals(m_selected_tile))
                {
                    if ((m_hovered_tile.getUnit() != null && m_hovered_tile.getUnit().getPlayer() == this) ||
                        (m_hovered_tile.getCity() != null && m_hovered_tile.getCity().getPlayer() == this))
                    {
                        setSelectedTile(m_hovered_tile);
                    }
                    else
                    {
                        Game.getInstance().getCamera().panCenter(m_hovered_tile.getPosition());
                    }
                }
                m_prev_mouse_pos = null;
            }
        }
    }
    
    /**
     * Used to find the tile that the mouse is currently hovering over, or any point in screen space.
     * @param position A position in screen space.
     * @return A tile object located at the provided position. Null if no tile exists.
     */
    private Tile calculateHoveredTile(Point position)
    {
        Point map_position = Game.getInstance().getCamera().screenSpaceToWorld(position);
            
        Tile candidate_tile = m_map.getTile(map_position);
        if (candidate_tile == null)
        {
            return null;
        }

        Rectangle inner_rect = candidate_tile.getInnerRect();

        // Quick bounds check.
        if (inner_rect.contains(map_position))
        {
            return candidate_tile;
        }
        else
        {
            Point candidate_tile_pos = candidate_tile.getPosition();
            Point candidate_tile_center_pos = candidate_tile.getCenterPosition();

            boolean is_pt_near_top = (map_position.y <= candidate_tile_center_pos.y);
            boolean is_pt_near_left = (map_position.x <= candidate_tile_center_pos.x);

            int y_offset = is_pt_near_top ? 0 : Tile.TILE_HEIGHT;
            int x_offset = is_pt_near_left ? 0 : Tile.TILE_WIDTH;

            // Points for one of the corner triangles,
            // where the alpha of the hexagon is zero.
            Point point_a = new Point(candidate_tile_pos.x + x_offset, candidate_tile_pos.y + Tile.TILE_HEIGHT / 2);
            Point point_b = new Point(candidate_tile_pos.x + ((Tile.TILE_WIDTH / 4) + (x_offset / 2)), candidate_tile_pos.y + y_offset);
            Point point_c = new Point(candidate_tile_pos.x + x_offset, candidate_tile_pos.y + y_offset);

            Polygon corner_triangle = new Polygon();
            corner_triangle.addPoint(point_a.x, point_a.y);
            corner_triangle.addPoint(point_b.x, point_b.y);
            corner_triangle.addPoint(point_c.x, point_c.y);

            // If the clicked mouse position is outside of the 
            // corner triangle, it must be inside the candidate
            // hexagon.            
            if (!corner_triangle.contains(map_position))
            {
                return candidate_tile;
            }
            else
            {
                Point candidate_tile_index = m_map.getTileIndex(map_position);

                int adjacent_tile_row;
                if (candidate_tile_index.x % 2 != 0)
                {
                    adjacent_tile_row = candidate_tile_index.y + (is_pt_near_top ? 0 : 1);
                }
                else
                {
                    adjacent_tile_row = candidate_tile_index.y + (is_pt_near_top ? -1 : 0);
                }

                int adjacent_tile_col = candidate_tile_index.x + (is_pt_near_left ? -1 : 1);
                if (adjacent_tile_col < 0)
                {
                    adjacent_tile_col += m_map.getMapWidth();
                }

                adjacent_tile_col %= m_map.getMapWidth();

                Tile adjacent_tile = m_map.getTile(adjacent_tile_col, adjacent_tile_row);
                if (adjacent_tile != null)
                {
                    return adjacent_tile;
                }
            }
        }
        
        return null;
    }
    
    /**
     * Used to draw the graphics for the current player.
     * @param graphics The graphics object to be drawn to.
     */
    @Override
    public void draw(Graphics graphics)
    {
        if (m_players_turn && m_locked)
        {
            return;
        }
        
        if(m_selected_tile != null && m_players_turn)
        {
            Point draw_position = m_selected_tile.getPosition();
            draw_position = Game.getInstance().getCamera().worldToScreenSpace(draw_position);
            graphics.drawImage(SELECTED_TILE, draw_position.x, draw_position.y, null);
        }
        
        for (City city : m_cities)
        {
            city.draw(graphics);
        }
        
        for (Unit unit : m_units)
        {
            unit.draw(graphics);
        }
    }
}
