/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.GameTime;
import engine.IDrawable;
import engine.ISingleton;
import engine.IUpdateable;
import engine.ui.UIManager;
import engine.views.ViewManager;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * The game class contains all game related logic, as is the lowest level object for Aciv specific code.
 * @author Michael
 */
public class Game implements IUpdateable, IDrawable, ISingleton
{
    public static final Color[] PLAYER_COLORS = {Color.RED, new Color(84, 84, 255), Color.CYAN, Color.YELLOW};
    public static boolean DEBUG_MODE = false;
    
    private static Game m_game;
    /**
     * Get a reference to the one and only instance of the game class.
     * @return The only instance of the game class.
     */
    public static Game getInstance()
    {
        if(m_game == null)
        {
            m_game = new Game();
        }
        
        return m_game;
    }
    
    // Game window dimension.
    public static final int SCREEN_WIDTH = (int)(Toolkit.getDefaultToolkit().getScreenSize().width * 0.8f);
    public static final int SCREEN_HEIGHT = (int)(Toolkit.getDefaultToolkit().getScreenSize().height * 0.8f);
    
    private Map m_map;
    private MiniMap m_mini_map;
    private GameTime m_game_time;
    private PlayerCamera m_active_camera;
    
    private Player[] m_players;
    private Player m_active_player;
    private static int m_player_count = 3;
    private static String[] m_player_names;
    private int m_curr_player_id = 0;
    private int m_num_players_in_game;
    
    private List<ITurnListener> m_turn_listeners;
    private static boolean m_generate_map = false;
    
    /**
     * Constructs the game class.
     */
    private Game()
    {
        m_turn_listeners = new ArrayList<ITurnListener>();
    }
    
    /**
     * Called at the start of a new game. Used to set the game up.
     */
    public void initialize()
    {
        //System.out.println("Initializing Game...");
    }
    
    /**
     * Sets a flag ordering the game to randomly generate a map or load a map.
     * @param value True if a map should be randomly generated.
     */
    public static void setGenerateMap(boolean value)
    {
        m_generate_map = value;
    }
    
    /**
     * Used to set the player count in a specific game.
     * @param player_count The number of players to be included in the game.
     */
    public static void setPlayerCount(int player_count)
    {
        m_player_count = player_count;
    }
    
    /**
     * Used to set the names of the players in the game.
     * @param player_names The names to assign to players in numeric order.
     */
    public static void setPlayerNames(String[] player_names)
    {
        m_player_names = player_names;
    }
    
    /**
     * Loads all game content, and sets up the map for play.
     */
    public void loadContent()
    {
        // TODO: Load any content here. Called once per game.
        //System.out.println("Loading Content...");
        
        m_map = new Map();
        addTurnListener(m_map);
        if (m_generate_map)
        {
            m_map.generateTerrain(64, 64);
        }
        else
        {
            m_map.loadTerrain("content/maps/earth.map");
        }
        
        m_mini_map = new MiniMap(m_map);
        
        m_players = new Player[m_player_count];
        for (int i = 0; i < m_player_count; i++)
        {
            m_players[i] = new Player(m_map, Player.Age.STONE, PLAYER_COLORS[i], m_player_names[i]);
            m_players[i].addPlayerListener(m_mini_map);
            m_players[i].addPlayerListener(m_map);
        }
            
        m_map.spawnPlayers(m_players);
        m_num_players_in_game = m_player_count;
        
        m_active_player = m_players[m_curr_player_id];
        m_active_camera = m_active_player.getCamera();
        m_active_player.onBeginTurn();
        
        broadcastBeginTurn();
    }
    
    /**
     * Used to unload game content, preventing memory leaks.
     */
    public void unloadContent()
    {
        // TODO: Unload any content here. Called once per game.
        //System.out.println("Unloading Content...");
        destroy();
    }

    /**
     * Called every frame that the game is running. This call trickles down to all Aciv classes, giving them time to process on each frame.
     * @param game_time The object holding all time related information for the game.
     */
    @Override
    public void update(GameTime game_time) 
    {
        m_game_time = game_time;
        /*
        if (InputHandler.getInstance().isInputPressed(InputHandler.InputType.KEY, KeyEvent.VK_D))
        {
            DEBUG_MODE = !DEBUG_MODE;
        }
        
        if (InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_SPACE))
        {
            reset();
        }
        
        if (InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_N))
        {
            m_active_player.spawnUnit();
        }
        */
        
        m_active_player.update(game_time);
        m_mini_map.update(game_time);
    }
    
    /**
     * The primary function used to draw Aciv, trickles down to all drawable objects within the game.
     * @param graphics 
     */
    @Override
    public void draw(Graphics graphics) 
    {
        graphics.setColor(new Color(0.392f, 0.584f, 0.929f, 1.0f));
        graphics.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        
        // Drawing in game objects
        m_map.draw(graphics);
        
        for (Player player : m_players)
        {
            player.draw(graphics);
        }
        
        m_map.drawFogOfWar(graphics);
        /*
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, 70, 30);
        graphics.setColor(Color.WHITE);
        graphics.drawString("FPS: " + m_game_time.getFPS(), 10, 20);
        */
    }
    
    /**
     * Called whenever a player is eliminated, used to check if only 1 player remains.
     */
    public void playerEliminated()
    {
        m_num_players_in_game--;
        
        if (m_num_players_in_game == 1)
        {
            JOptionPane.showMessageDialog(null, "Congratulations, " + m_active_player.getName() + "! You are victorious!");
            ViewManager.getInstance().changeView("setup");
        }
    }
    
    /**
     * Resets the game starting from turn 1.
     */
    private void reset()
    {
        UIManager.getInstance().resetUI();
        m_turn_listeners.clear();
        m_curr_player_id = 0;
        
        m_map = new Map();
        m_map.generateTerrain(8, 8);
        
        m_mini_map = new MiniMap(m_map);
        
        m_players = new Player[m_player_count];
        for (int i = 0; i < m_player_count; i++)
        {
            m_players[i] = new Player(m_map, Player.Age.STONE, PLAYER_COLORS[i], m_player_names[i]);
            m_players[i].addPlayerListener(m_mini_map);
            m_players[i].addPlayerListener(m_map);
        }
            
        m_map.spawnPlayers(m_players);
        
        m_active_player = m_players[m_curr_player_id];
        m_active_camera = m_active_player.getCamera();
        m_active_player.onBeginTurn();
        
        broadcastBeginTurn();
    }
    
    /**
     * Adds a turn listener object to the game.
     * @param listener The object to be added.
     */
    public void addTurnListener(ITurnListener listener)
    {
        if (listener != null)
        {
            m_turn_listeners.add(listener);
        }
    }
    
    /**
     * Broadcasts the beginning of a new players turn to all turn listeners.
     */
    private void broadcastBeginTurn()
    {
        for (ITurnListener listener : m_turn_listeners)
        {
            listener.onBeginTurn();
        }
    }
    
    /**
     * Broadcasts the end of a players turn ot all turn listeners.
     */
    private void broadcastEndTurn()
    {
        for (ITurnListener listener : m_turn_listeners)
        {
            listener.onEndTurn();
        }
    }
    
    /**
     * Called by a player in order to end their turn.
     */
    public void endTurn()
    {
        m_active_player.onEndTurn();
        broadcastEndTurn();
        do
        {
            m_curr_player_id = (m_curr_player_id + 1) % m_player_count;
            m_active_player = m_players[m_curr_player_id];
        } while (!m_active_player.isInGame());
        
        m_active_camera = m_active_player.getCamera();
        
        m_active_player.onBeginTurn();
        broadcastBeginTurn();
    }
    
    /**
     * Used to get the active game camera.
     * @return The active game camera.
     */
    public PlayerCamera getCamera()
    {
        return m_active_camera;
    }
    
    /**
     * Used to get the active player.
     * @return The active player.
     */
    public Player getPlayer()
    {
        return m_active_player;
    }
    
    /**
     * Used to get a copy of the map.
     * @return The game map.
     */
    public Map getMap()
    {
        return m_map;
    }
    
    /**
     * Used to get a reference to the mini map.
     * @return A reference to the mini map.
     */
    public MiniMap getMiniMap()
    {
        return m_mini_map;
    }
    
    /**
     * Used to get the list of players in the game.
     * @return The list of player objects in the current game.
     */
    public Player[] getPlayers()
    {
        return m_players;
    }
    
    /**
     * Used to destroy the only instance of the game object.
     */
    @Override
    public void destroy() 
    {
        m_game = null;
    }
}
