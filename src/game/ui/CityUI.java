/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ui;

import engine.GameTime;
import engine.ImageManager;
import engine.ui.UI;
import game.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 *
 * @author michael
 */
public class CityUI extends UI<City>
{
    private final UnitData m_settler_data = UnitFactory.getInstance().getUnitData("Settler");
    private final UnitData m_militia_data = UnitFactory.getInstance().getUnitData("Militia");
    private final UnitData m_spearman_data = UnitFactory.getInstance().getUnitData("Spearman");
    private final UnitData m_horseman_data = UnitFactory.getInstance().getUnitData("Horseman");
    private final UnitData m_archer_data = UnitFactory.getInstance().getUnitData("Archer");

    
    private JTabbedPane m_tabs_panel;
    private JPanel m_units_panel;
    private JButton m_build_militia_button;
    private JButton m_build_settler_button;
    private JButton m_build_spearman_button;
    private JButton m_build_horseman_button;
    private JButton m_build_archer_button;

    
    public CityUI()
    {
        initialize();
    }
    
    private void initialize()
    {
        m_tabs_panel = new JTabbedPane();
        m_tabs_panel.setFocusable(false);
        m_tabs_panel.setOpaque(true);
        m_tabs_panel.setAlignmentY(SwingConstants.TOP);
        
        //m_tabs_panel.addTab("Buildings", initBuildingsTab());
        m_tabs_panel.addTab("Units", initUnitsTab());
        
        m_components.add(m_tabs_panel);
    }
    
    @Override
    protected void onSetModel()
    {
        Point mini_map_dim = Game.getInstance().getMiniMap().getMiniMapDimensions();
        m_tabs_panel.setBounds(Game.SCREEN_WIDTH - 270, 10, 260, Game.SCREEN_HEIGHT - mini_map_dim.y - 70);
        Dimension dim = m_tabs_panel.getSize();
        dim.width -= 15;
        dim.height = 0;
        m_units_panel.setPreferredSize(dim);
        
        City city = (City)m_model;
        Player player = city.getPlayer();
        
        m_build_settler_button.setIcon(new ImageIcon(ImageManager.getInstance().filterSprite("images/Settler.png", "images/Settler.png" + player.getColorExtension(), player.getColor())));
        m_build_militia_button.setIcon(new ImageIcon(ImageManager.getInstance().filterSprite("images/Militia.png", "images/Militia.png" + player.getColorExtension(), player.getColor())));
        m_build_spearman_button.setIcon(new ImageIcon(ImageManager.getInstance().filterSprite("images/Spearman.png", "images/Spearman.png" + player.getColorExtension(), player.getColor())));
        m_build_horseman_button.setIcon(new ImageIcon(ImageManager.getInstance().filterSprite("images/Horseman.png", "images/Horseman.png" + player.getColorExtension(), player.getColor())));
        m_build_archer_button.setIcon(new ImageIcon(ImageManager.getInstance().filterSprite("images/Archer.png", "images/Archer.png" + player.getColorExtension(), player.getColor())));

    }
    
    private Component initBuildingsTab()
    {
        JPanel buildings_panel = new JPanel();
        JButton tmp = new JButton("Test");
        tmp.setFocusable(false);
        buildings_panel.add(tmp);
        
        return buildings_panel;
    }
    
    private Component initUnitsTab()
    {
        m_units_panel = new JPanel();
        
        FlowLayout layout = new FlowLayout();
        layout.setAlignment(FlowLayout.CENTER);
        m_units_panel.setLayout(layout);
        
        m_build_settler_button = initUnitButton("Settler", "images/Settler.png", m_settler_data.cost);
        m_units_panel.add(m_build_settler_button);
        
        m_build_militia_button = initUnitButton("Militia", "images/Militia.png", m_militia_data.cost);
        m_units_panel.add(m_build_militia_button);
        
        m_build_spearman_button = initUnitButton("Spearman", "images/Militia.png", m_spearman_data.cost);
        m_units_panel.add(m_build_spearman_button);
        
        m_build_horseman_button = initUnitButton("Horseman", "images/Militia.png", m_horseman_data.cost);
        m_units_panel.add(m_build_horseman_button);
        
        m_build_archer_button = initUnitButton("Archer", "images/Militia.png", m_archer_data.cost);
        m_units_panel.add(m_build_archer_button);
        
        JScrollPane units_scroll_panel = new JScrollPane(m_units_panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        return units_scroll_panel;
    }
    
    private JButton initUnitButton(String name, String icon_path, int cost)
    {
        
        JButton unit_button = new JButton("<html><font color=\"blue\">" + name + "</font><br>" + "Cost: " + cost + "</html>");
        unit_button.setFocusable(false);
        unit_button.setPreferredSize(new Dimension(235, 40));
        unit_button.setIcon(ImageManager.getInstance().getSpriteIcon(icon_path));
        unit_button.setHorizontalAlignment(SwingConstants.LEFT);
        unit_button.addActionListener(this);
        return unit_button;
    }

    @Override
    public void actionPerformed(ActionEvent ae) 
    {
        Player player = Game.getInstance().getPlayer();
        
        if (ae.getSource() == m_build_settler_button)
        {
            if (m_model.createUnit("Settler"))
            {
                player.spendGold(m_settler_data.cost);
            }
        }
        else if (ae.getSource() == m_build_militia_button)
        {
            if (m_model.createUnit("Militia"))
            {
                player.spendGold(m_militia_data.cost);
            }
        }
        else if (ae.getSource() == m_build_spearman_button)
        {
            if (m_model.createUnit("Spearman"))
            {
                player.spendGold(m_spearman_data.cost);
            }
        }
        else if (ae.getSource() == m_build_horseman_button)
        {
            if (m_model.createUnit("Horseman"))
            {
                player.spendGold(m_horseman_data.cost);
            }
        }
        else if (ae.getSource() == m_build_archer_button)
        {
            if (m_model.createUnit("Archer"))
            {
                player.spendGold(m_archer_data.cost);
            }
        }
    }

    @Override
    public void update(GameTime game_time) 
    {
        Player player = Game.getInstance().getPlayer();
        
        if (player.getGold() < m_settler_data.cost)
        {
            m_build_settler_button.setEnabled(false);
        }
        else if (!m_build_settler_button.isEnabled())
        {
            m_build_settler_button.setEnabled(true);
        }
        
        if (player.getGold() < m_militia_data.cost)
        {
            m_build_militia_button.setEnabled(false);
        }
        else if (!m_build_militia_button.isEnabled())
        {
            m_build_militia_button.setEnabled(true);
        }
        
        if (player.getGold() < m_spearman_data.cost)
        {
            m_build_spearman_button.setEnabled(false);
        }
        else if (!m_build_spearman_button.isEnabled())
        {
            m_build_spearman_button.setEnabled(true);
        }
        if (player.getGold() < m_horseman_data.cost)
        {
            m_build_horseman_button.setEnabled(false);
        }
        else if (!m_build_horseman_button.isEnabled())
        {
            m_build_horseman_button.setEnabled(true);
        }
        if (player.getGold() < m_archer_data.cost)
        {
            m_build_archer_button.setEnabled(false);
        }
        else if (!m_build_archer_button.isEnabled())
        {
            m_build_archer_button.setEnabled(true);
        }
    }

    @Override
    public void draw(Graphics graphics) 
    {
        // Draw?
    }
    
}
