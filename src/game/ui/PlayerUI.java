/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ui;

import engine.GameTime;
import engine.ImageManager;
import engine.input.InputHandler;
import engine.ui.UI;
import engine.views.ViewManager;
import game.City;
import game.Game;
import game.Player;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author Michael
 */
public class PlayerUI extends UI<Player>
{
    private JButton m_end_turn_button;
    private JButton m_end_game_button;
    
    public PlayerUI()
    {
        m_end_turn_button = createButton("End Turn");
        m_end_turn_button.setBounds((Game.SCREEN_WIDTH / 2) - 60, Game.SCREEN_HEIGHT - 45, 120, 30);
        
        m_end_game_button = createButton("End Game");
        m_end_game_button.setBounds(15, Game.SCREEN_HEIGHT - 45, 120, 30);
    }
    
    @Override
    protected void onSetModel()
    {
        m_end_turn_button.setVisible(false);
        m_end_game_button.setVisible(false);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        if (e.getSource() == m_end_turn_button)
        {
            m_model.signalEndTurn();
        }
        else if (e.getSource() == m_end_game_button)
        {
            int result = JOptionPane.showConfirmDialog(
                    null,
                    "Are you sure you want to end the game?",
                    "End Game",
                    JOptionPane.YES_NO_OPTION);
            
            if (result == 0)
            {
                ViewManager.getInstance().changeView("start");
            }
        }
    }

    @Override
    public void update(GameTime game_time) 
    {
        if (m_model.isLocked())
        {
            if (InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_ENTER))
            {
                m_end_turn_button.setVisible(true);
                m_end_game_button.setVisible(true);
                m_model.unlock();
            }
        }
    }

    @Override
    public void draw(Graphics graphics) 
    {
        if (m_model.isLocked())
        {
            int num_rows = (Game.SCREEN_HEIGHT / 80) + 1;
            int num_cols = (Game.SCREEN_WIDTH / 80) + 1;
            for (int row = 0; row < num_rows; row++)
            {
                for (int col = 0; col < num_cols; col++)
                {
                    graphics.drawImage(ImageManager.getInstance().getSprite("images/start_menu_bg.png"), col * 80, row * 80, null);
                }
            }
            
            graphics.setColor(new Color(0, 0, 0, 64));
            graphics.fillRect(0, (Game.SCREEN_HEIGHT / 2) - 122, Game.SCREEN_WIDTH, 204);
            graphics.fillRect(0, (Game.SCREEN_HEIGHT / 2) - 120, Game.SCREEN_WIDTH, 200);
            
            String name = m_model.getName();
            Rectangle2D name_bounds = m_model.getNameBounds();
            if (name_bounds == null)
            {
                Rectangle2D bounds = graphics.getFontMetrics().getStringBounds(m_model.getName(), graphics);
                m_model.setNameBounds(bounds);
                name_bounds = bounds;
            }
            
            graphics.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
            ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            
            int x = Game.SCREEN_WIDTH / 2;
            int y = (Game.SCREEN_HEIGHT / 2) - 30;
            
            graphics.setColor(Color.BLACK);
            graphics.drawString(name, x + 3, y + 3);
            graphics.setColor(new Color(250, 255, 220, 255));
            graphics.drawString(name, x, y);

            graphics.setFont(new Font("Arial", Font.PLAIN, 12));
            ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            
            graphics.setColor(Color.WHITE);
            graphics.drawString("Press ENTER to begin your turn.", (Game.SCREEN_WIDTH / 2) - 50, (Game.SCREEN_HEIGHT / 2));
            
            return;
        }
        
        /*
        graphics.setColor(Color.darkGray);
        graphics.fillRect(0, 40, 160, 30);
        graphics.setColor(Color.WHITE);
        graphics.drawString("Player Gold: " + m_model.getGold(), 10, 60);
        */
        
        if (Game.getInstance().getPlayer().getSelectedTile() != null)
        {
            if (Game.getInstance().getPlayer().getSelectedTile().getUnit() != null)
            {
                Game.getInstance().getPlayer().getSelectedTile().getUnit().drawPath(graphics);
                Game.getInstance().getPlayer().getSelectedTile().getUnit().drawAttackRange(graphics);
            }
        }
        
        for(Player player : Game.getInstance().getPlayers())
        {
            for(City city : player.getCities())
            {
                city.drawUI(graphics);
            }
        }
        
        graphics.setColor(new Color(235, 208, 79, 255));
        graphics.fillRoundRect(15, 15, 122, 32, 10, 10);
        graphics.setColor(new Color(77, 73, 46, 255));
        graphics.fillRoundRect(16, 16, 120, 30, 10, 10);
        
        graphics.setFont(new Font("Arial", Font.BOLD, 12));
        graphics.setColor(new Color(235, 208, 79, 255));
        graphics.drawString("Gold: " + m_model.getGold(), 30, 35);
        graphics.setFont(new Font("Arial", Font.PLAIN, 12));
    }
}
