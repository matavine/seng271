/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ui;

import engine.GameTime;
import engine.ui.UI;
import game.Game;
import game.Unit;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

/**
 *
 * @author Michael
 */
public class UnitUI extends UI<Unit>
{
    private JButton m_attack_button;
    private JButton m_move_button;
    
    public UnitUI()
    {
        initialize();
    }
    
    protected void initialize()
    {
        m_attack_button = createButton("Attack");
        m_move_button = createButton("Move");
    }
    
    @Override
    protected void onSetModel()
    {
        Point mini_map_dim = Game.getInstance().getMiniMap().getMiniMapDimensions();
        m_attack_button.setBounds(Game.SCREEN_WIDTH - 265, Game.SCREEN_HEIGHT - mini_map_dim.y - 50, 120, 30);
        m_move_button.setBounds(Game.SCREEN_WIDTH - 135, Game.SCREEN_HEIGHT - mini_map_dim.y - 50, 120, 30);
    }

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        Unit unit = (Unit)m_model;
        
        if (e.getSource() == m_attack_button)
        {
            unit.startAttackCommand();
        }
        else if (e.getSource() == m_move_button)
        {
            unit.startMovementPrediction(true);
        }
    }

    @Override
    public void update(GameTime game_time) 
    {
        // Update?
    }

    @Override
    public void draw(Graphics graphics) 
    {
        // Draw something?
    }
}
