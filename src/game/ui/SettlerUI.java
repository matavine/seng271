/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ui;

import game.Game;
import game.Settler;
import java.awt.Point;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

/**
 *
 * @author michael
 */
public class SettlerUI extends UnitUI
{
    private JButton m_move_button;
    private JButton m_create_city_button;
    
    @Override
    protected void initialize()
    {
        m_move_button = createButton("Move");
        m_create_city_button = createButton("Build City");
    }
    
    @Override
    protected void onSetModel()
    {
        Point mini_map_dim = Game.getInstance().getMiniMap().getMiniMapDimensions();
        m_move_button.setBounds(Game.SCREEN_WIDTH - 265, Game.SCREEN_HEIGHT - mini_map_dim.y - 50, 120, 30);
        m_create_city_button.setBounds(Game.SCREEN_WIDTH - 135, Game.SCREEN_HEIGHT - mini_map_dim.y - 50, 120, 30);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        Settler settler = (Settler)m_model;
        
        if (e.getSource() == m_move_button)
        {
            settler.startMovementPrediction(true);
        }
        else if (e.getSource() == m_create_city_button)
        {
            settler.createCity();
        }
    }
}
