/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ui;

import engine.GameTime;
import engine.ui.UI;
import game.Game;
import game.MiniMap;
import game.Player;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JLabel;

/**
 *
 * @author michael
 */
public class MiniMapUI extends UI<MiniMap> 
    implements MouseListener, MouseMotionListener
{
    private JLabel m_mini_map;
    private Rectangle m_mini_map_border;
    
    public MiniMapUI()
    {   
        initialize();
    }
    
    private void initialize()
    {
        m_mini_map = new JLabel();
        m_mini_map.setVisible(false);
        m_mini_map.setOpaque(true);
        m_mini_map.setFocusable(true);
        m_mini_map.addMouseListener(this);
        m_mini_map.addMouseMotionListener(this);
        m_components.add(m_mini_map);
    }
    
    @Override
    protected void onSetModel() 
    {
        m_mini_map.setIcon(m_model.getMiniMapImage());
        Point mini_map_dim = m_model.getMiniMapDimensions();
        
        int x_pos = Game.SCREEN_WIDTH - mini_map_dim.x - 10;
        int y_pos = Game.SCREEN_HEIGHT - mini_map_dim.y - 10;
        
        m_mini_map.setBounds(x_pos, y_pos, mini_map_dim.x, mini_map_dim.y);
        m_mini_map_border = new Rectangle(x_pos - 2, y_pos - 2, mini_map_dim.x + 4, mini_map_dim.y + 4);
        m_mini_map.setVisible(true);
    }
    
    private void moveCameraTo(Point component_relative_pos)
    {
        Rectangle mini_map_bounds = m_mini_map.getBounds();
        if (component_relative_pos.x < 0)
        {
            component_relative_pos.x = (component_relative_pos.x % mini_map_bounds.width) + mini_map_bounds.width;
        }
        
        float x_ratio = component_relative_pos.x / (float)mini_map_bounds.width;
        float y_ratio = component_relative_pos.y / (float)mini_map_bounds.height;
        
        Point map_dim = new Point(Game.getInstance().getMap().getMapPixelWidth(), 
                                  Game.getInstance().getMap().getMapPixelHeight());
        
        int world_x_pos = (int)(x_ratio * map_dim.x);
        int world_y_pos = (int)(y_ratio * map_dim.y);
        
        Game.getInstance().getCamera().centerPosition(new Point(world_x_pos, world_y_pos));
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) 
    {
        // Not used.
    }

    @Override
    public void update(GameTime game_time) 
    {
        if (m_model.getUpdateUI())
        {
            m_mini_map.setIcon(m_model.getMiniMapImage());
            m_model.setUpdateUI(false);
        }
    }

    @Override
    public void draw(Graphics graphics) 
    {
        graphics.setColor(new Color(53, 89, 134, 255));
        graphics.fillRect(m_mini_map_border.x, m_mini_map_border.y, m_mini_map_border.width, m_mini_map_border.height);
    }

    @Override
    public void mouseClicked(MouseEvent me) 
    {
        // Not used.
    }

    @Override
    public void mousePressed(MouseEvent me) 
    {
        moveCameraTo(me.getPoint().getLocation());
    }

    @Override
    public void mouseReleased(MouseEvent me) 
    {
        // Not used.
    }

    @Override
    public void mouseEntered(MouseEvent me) 
    {
        // Not used.
    }

    @Override
    public void mouseExited(MouseEvent me) 
    {
        // Not used.
    }

    @Override
    public void mouseDragged(MouseEvent me) 
    {
        moveCameraTo(me.getPoint().getLocation());
    }

    @Override
    public void mouseMoved(MouseEvent me) 
    {
        // Not used.
    }
}
