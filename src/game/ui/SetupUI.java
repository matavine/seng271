/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ui;

import engine.GameTime;
import engine.ui.UI;
import engine.views.ViewManager;
import game.Game;
import game.Map;
import game.MiniMap;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 *
 * @author michael
 */
public class SetupUI<SetupView> extends UI
{
    private final static int MAX_PLAYERS = 4;
    private final static Point UI_BOUNDS = new Point((int)(Game.SCREEN_WIDTH * 0.8f), (int)(Game.SCREEN_HEIGHT * 0.75f));
    
    private JLabel m_map_preview;
    private JTextField[] m_player_names;
    private JLabel[] m_player_num_labels;
    private JComboBox m_player_num_list;
    private JComboBox m_map_list;
    private JButton m_back_button;
    private JButton m_start_game_button;
    
    private Map m_selected_map;
    private int m_map_index = 0;
    
    public SetupUI()
    {
        Point top_left_pos = new Point((int)(Game.SCREEN_WIDTH * 0.07f), (int)(Game.SCREEN_HEIGHT * 0.15f));
        
        m_map_preview = new JLabel();
        m_map_preview.setOpaque(true);
        m_map_preview.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        Point map_pos = new Point (top_left_pos.x + (int)(UI_BOUNDS.x / 2 * 0.05f), top_left_pos.y + (int)(UI_BOUNDS.y / 2 * 0.1f));
        Rectangle map_bounds = new Rectangle(map_pos.x, map_pos.y, (int)(UI_BOUNDS.x / 2 * 0.8f), (int)(UI_BOUNDS.y / 2 * 0.6f));
        m_map_preview.setBounds(map_bounds);
        m_components.add(m_map_preview);
        
        m_selected_map = new Map();
        m_selected_map.loadTerrain("content/maps/earth.map");
        m_map_preview.setIcon(MiniMap.createMiniMap(map_bounds.width, map_bounds.height, m_selected_map, false));
        
        Rectangle num_players_label_bounds = new Rectangle(map_bounds.x + (UI_BOUNDS.x / 2), map_bounds.y, UI_BOUNDS.x / 4, 30);
        createLabel("Number Of Players:", num_players_label_bounds);
        
        m_player_num_list = new JComboBox(new String[]{ "Two", "Three", "Four" });
        m_player_num_list.setBackground(Color.GRAY);
        m_player_num_list.setForeground(Color.WHITE);
        Rectangle player_num_list_bounds = new Rectangle(num_players_label_bounds.x + num_players_label_bounds.width, num_players_label_bounds.y, UI_BOUNDS.x / 4, 30);
        m_player_num_list.setBounds(player_num_list_bounds);
        m_player_num_list.addActionListener(this);
        m_components.add(m_player_num_list);
        
        Point player_names_pos = new Point(num_players_label_bounds.x, num_players_label_bounds.y + 50);
        m_player_names = new JTextField[MAX_PLAYERS];
        m_player_num_labels = new JLabel[MAX_PLAYERS];
        for (int i = 0; i < MAX_PLAYERS; i++)
        {   
            Rectangle player_num_label_bounds = new Rectangle(player_names_pos.x, player_names_pos.y, UI_BOUNDS.x / 4, 30);
            JLabel player_num_label = createLabel("Player #" + (i + 1), player_num_label_bounds);
            m_player_num_labels[i] = player_num_label;
            
            JTextField player_txt_field = new JTextField();
            player_txt_field.setForeground(Color.WHITE);
            player_txt_field.setBackground(Color.BLACK);
            player_txt_field.setBorder(BorderFactory.createLineBorder(Color.GRAY));
            player_txt_field.setCaretColor(Color.GRAY);
            Rectangle player_txt_field_bounds = new Rectangle(player_names_pos.x + player_num_label_bounds.width, player_names_pos.y, UI_BOUNDS.x / 4, 30);
            player_txt_field.setBounds(player_txt_field_bounds);
            m_player_names[i] = player_txt_field;
            m_components.add(player_txt_field);
            
            if (i > 1)
            {
                player_num_label.setVisible(false);
                player_txt_field.setVisible(false);
            }
            
            player_names_pos.translate(0, 40);
        }
        
        m_start_game_button = new JButton("Start Game");
        Rectangle start_game_bounds = new Rectangle(player_names_pos.x + UI_BOUNDS.x / 2 - 120, player_names_pos.y + 10, 120, 30);
        m_start_game_button.setBounds(start_game_bounds);
        m_start_game_button.addActionListener(this);
        m_start_game_button.setFocusable(false);
        m_components.add(m_start_game_button);
        
        Rectangle map_label_bounds = new Rectangle(map_bounds.x, map_bounds.y + map_bounds.height + 20, map_bounds.width / 2, 30);
        createLabel("Select Map:", map_label_bounds);
        
        m_map_list = new JComboBox(new String[] { "earth.map", "Randomly Generated" });
        m_map_list.setBackground(Color.GRAY);
        m_map_list.setForeground(Color.WHITE);
        Rectangle map_list_bounds = new Rectangle(map_label_bounds.x + map_label_bounds.width, map_label_bounds.y, map_bounds.width / 2, 30);
        m_map_list.setBounds(map_list_bounds);
        m_map_list.addActionListener(this);
        m_components.add(m_map_list);
        
        m_back_button = new JButton("Back To Main Menu");
        m_back_button.setFocusable(false);
        Rectangle back_button_bounds = new Rectangle(map_label_bounds.x, top_left_pos.y + (int)(UI_BOUNDS.y * 0.8f), 180, 30);
        m_back_button.setBounds(back_button_bounds);
        m_back_button.addActionListener(this);
        m_components.add(m_back_button);
    }
    
    private JLabel createLabel(String text, Rectangle bounds)
    {
        JLabel label = new JLabel(text);
        label.setForeground(new Color(250, 255, 220, 255));
        label.setFont(new Font("Arial", Font.BOLD, 14));
        label.setBounds(bounds);
        m_components.add(label);
        
        return label;
    }
    
    @Override
    protected void onSetModel()
    {
        for (int i = 0; i < MAX_PLAYERS; i++)
        {
            m_player_names[i].setFocusable(true);
        }
        
        m_player_num_list.setFocusable(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) 
    {
        if (ae.getSource() == m_player_num_list)
        {
            int selected_index = m_player_num_list.getSelectedIndex() + 1;
            for (int i = 1; i < MAX_PLAYERS; i++)
            {
                if (i > selected_index)
                {
                    m_player_names[i].setVisible(false);
                    m_player_num_labels[i].setVisible(false);
                    continue;
                }
                
                m_player_names[i].setVisible(true);
                m_player_num_labels[i].setVisible(true);
            }
        }
        else if (ae.getSource() == m_map_list)
        {
            if (m_map_list.getSelectedIndex() != m_map_index)
            {
                Rectangle map_preview_bounds = m_map_preview.getBounds();
                
                if (m_map_list.getSelectedIndex() == 0)
                {
                    m_map_preview.setIcon(MiniMap.createMiniMap(map_preview_bounds.width, map_preview_bounds.height, m_selected_map, false));
                }
                else
                {
                    m_map_preview.setIcon(MiniMap.createMiniMap(map_preview_bounds.width, map_preview_bounds.height, m_selected_map, true));
                }
                
                m_map_index = m_map_list.getSelectedIndex();
            }
        }
        else if (ae.getSource() == m_start_game_button)
        {
            int num_players = m_player_num_list.getSelectedIndex() + 2;
            String[] player_names = new String[num_players];
            for (int i = 0; i < num_players; i++)
            {
                if (m_player_names[i].getText().length() == 0)
                {
                    JOptionPane.showMessageDialog(null,
                        "Player #" + (i + 1) + " needs a name.",
                        "Empty Name",
                        JOptionPane.ERROR_MESSAGE);
                    
                    return;
                }
                
                player_names[i] = m_player_names[i].getText();
            }
            
            for (int i = 0; i < MAX_PLAYERS; i++)
            {
                m_player_names[i].setFocusable(false);
            }
            
            m_player_num_list.setFocusable(false);
            
            if (m_map_index == 1)
            {
                //m_selected_map.generateTerrain(64, 64);
                Game.setGenerateMap(true);
            }
            else
            {
                Game.setGenerateMap(false);
            }
            
            Game.setPlayerCount(num_players);
            Game.setPlayerNames(player_names);
            ViewManager.getInstance().changeView("game");
        }
        else if (ae.getSource() == m_back_button)
        {
            ViewManager.getInstance().changeView("start");
        }
    }

    @Override
    public void update(GameTime game_time) 
    {
    }

    @Override
    public void draw(Graphics graphics) 
    {
    }
    
}