/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.ui;

import engine.GameTime;
import engine.ui.UI;
import engine.views.ViewManager;
import game.Game;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

/**
 *
 * @author michael
 */
public class StartUI<StartView> extends UI
{
    JButton m_new_game_button;
    JButton m_load_game_button;
    JButton m_exit_button;
    
    public StartUI()
    {
        initialize();
    }
    
    private void initialize()
    {
        int x_center = (Game.SCREEN_WIDTH / 2) - 120;
        
        m_new_game_button = createButton("New Game");
        m_new_game_button.setBounds(x_center, Game.SCREEN_HEIGHT - 300, 240, 30);
        
        m_exit_button = createButton("Quit");
        m_exit_button.setBounds(x_center, Game.SCREEN_HEIGHT - 260, 240, 30);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) 
    {
        if (ae.getSource() == m_exit_button)
        {
            System.exit(0);
        }
        else if (ae.getSource() == m_new_game_button)
        {
            ViewManager.getInstance().changeView("setup");
        }
    }

    @Override
    public void update(GameTime game_time) 
    {
    }

    @Override
    public void draw(Graphics graphics) 
    {
    }
    
}
