/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.structures.Pair;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Static class used to calculate paths on Aciv's Hex based maps.
 * @author Geoff
 */
public class PathFinder
{
    // MinHeap Test Code
    /**
     * Main is used as a basic test function to test the min heap used in the pathfinding code.
     * @param args required for the main function, they affect nothing.
     */
    public static void main(String[] args)
    {   
        System.out.println("Testing min heap.");
        NodeMinHeap heap = new NodeMinHeap();
        heap.add(new PathNode(0,0, 0, 10));
        System.out.println(heap.peak().f_score);
        
        heap.add(new PathNode(0,1, 0, 16));
        heap.add(new PathNode(0,2, 0, 9));
        heap.add(new PathNode(0,3, 0, 8));
        heap.add(new PathNode(0,4, 0, 8));
        heap.add(new PathNode(0,5, 0, 12));
        
        heap.replace(new PathNode(0,3, 0, 18));
        
        while(!heap.isEmpty())
        {
            System.out.println(heap.pop().f_score);
        }
    }
    
    /**
     * Assisting object for path finding, simplifies complicated operations and
     * holds data used during path calculations.
     */
    private static class PathNode
        implements Comparable
    {
        private int row;
        private int col;
        private int g_score;
        private int f_score;
        
        /**
         * Path node constructor. Primarily a datatype class.
         * @param row The row of the current path node within the map file.
         * @param col The column of the current path node within the map file.
         * @param g_score The movement cost to get from the starting node to the curren node.
         * @param f_score The movement cost to get from this node to the final node.
         */
        public PathNode(int row, int col, int g_score, int f_score)
        {
            this.row = row;
            this.col = col;
            this.g_score = g_score;
            this.f_score = f_score;
        }
        
        /**
         * Used to create the neighboring nodes of the current path node.
         * @return A list containing all neighboring nodes.
         */
        public List<PathNode> getNeighbors()
        {
            List<PathNode> neighbors = new ArrayList<PathNode>();
            
            int temp_row_top = row - 1;
            int temp_row_bottom = row + 1;
            int temp_col_left = col - 1;
            int temp_col_right = col + 1;
            
            if(temp_col_left < 0)
                temp_col_left = Game.getInstance().getMap().getMapWidth()-1;
            if(temp_col_right > Game.getInstance().getMap().getMapWidth()-1)
                temp_col_right = 0;
            
            if(row > 0)
                neighbors.add(new PathNode(temp_row_top, col, 0, 0));
            if(row < Game.getInstance().getMap().getMapHeight() - 1)
                neighbors.add(new PathNode(temp_row_bottom, col, 0, 0));
            
            if(col%2 == 0)
            {
                if(row > 0)
                {
                    neighbors.add(new PathNode(temp_row_top, temp_col_left, 0, 0));
                    neighbors.add(new PathNode(temp_row_top, temp_col_right, 0, 0));
                }
            }
            else
            {
                if(row < Game.getInstance().getMap().getMapHeight() - 1)
                {
                    neighbors.add(new PathNode(temp_row_bottom, temp_col_left, 0, 0));
                    neighbors.add(new PathNode(temp_row_bottom, temp_col_right, 0, 0));
                }
            }
            
            neighbors.add(new PathNode(row, temp_col_left, 0, 0));
            neighbors.add(new PathNode(row, temp_col_right, 0, 0));
            
            return neighbors;
        }
        
        /**
         * Used to compare two path nodes to each other. Comparison is by f_score. 
         * @param node The node to compare to.
         * @return negative if compared path node has a higher f_score.
         * @throws ClassCastException 
         */
        @Override
        public int compareTo(Object node) throws ClassCastException
        {
            if(!(node instanceof PathNode))
            {
                throw new ClassCastException("PathNode objects may only be compared to other PathNode objects.");
            }
            else
            {
                if(node == null)
                {
                    return -1;
                }
                
                return f_score - ((PathNode)node).f_score;
            }
        }
        
        /**
         * Used to check if two path nodes are identical.
         * @param node The node to compare to.
         * @return True if both nodes represent the same location on the map.
         * @throws ClassCastException 
         */
        @Override
        public boolean equals(Object node) throws ClassCastException
        {
            if(!(node instanceof PathNode))
            {
                throw new ClassCastException("PathNode objects may only be compared to other PathNode objects.");
            }
            else
            {
                return (row == ((PathNode)node).row && col == ((PathNode)node).col);
            }
        }
        
        /**
         * Used to calculate the hash code for the current node.
         * @return The hash code for the current node.
         */
        @Override
        public int hashCode() {
            int hash = this.row;
            hash = Game.getInstance().getMap().getMapWidth() * hash + this.col;
            return hash;
        }
    }
    
    /**
     * A minimum heap class used to calculate search priority of nodes in the A* algorithm.
     * Nodes are sorted by their f_score value, such that the top of the heap is the lowest scoring node.
     * This heap uses an array based implementation.
     */
    private static class NodeMinHeap
    {
        List<PathNode> list = new ArrayList<PathNode>();
        
        /**
         * Min heap constructor.
         */
        public NodeMinHeap()
        {
        }
        
        /**
         * Used to add a path node to the min heap.
         * @param node The node to be added.
         */
        public void add(PathNode node)
        {
            list.add(null);
            int k = list.size() - 1;
            percolateUp(k, node);
            
        }
        
        /**
         * Used to pop the top node off of the heap without ruining the structure.
         * @return The top node of the heap.
         */
        public PathNode pop()
        {
            PathNode poppedNode = list.get(0);
            PathNode lastNode = list.remove(list.size() - 1);
            percolateDown(0, lastNode);
            return poppedNode;
        }
        
        /**
         * Used to look at the top of the heap without removing it.
         * @return A copy of the top node of the heap.
         */
        public PathNode peak()
        {
            return list.get(0);
        }
        
        /**
         * Used to check if the heap is empty.
         * @return True if the heap is empty.
         */
        public boolean isEmpty()
        {
            return list.isEmpty();
        }
        
        /**
         * Used to check if the heap contains a copy of the passed in node.
         * @param node The node to check for a copy of.
         * @return True if the heap contains a copy of the node.
         */
        public boolean contains(PathNode node)
        {
            return list.contains(node);
        }
        
        /**
         * Used to get a copy of the passed node from the heap.
         * @param node The node to get a copy of.
         * @return The copy of the node.
         */
        public PathNode get(PathNode node)
        {
            return list.get(list.indexOf(node));
        }
        
        /**
         * Used to replace the current identical node in the heap.
         * @param node The node to replace the existing heap node.
         */
        public void replace(PathNode node)
        {
            if(list.contains(node))
            {
                int index = list.indexOf(node);
                if(index == list.size() - 1)
                {
                    list.remove(list.size() - 1);
                }
                else
                {
                    PathNode last_node = list.remove(list.size() - 1);
                    int parent = (index - 1) / 2;
                    if(list.get(parent).f_score > last_node.f_score)
                    {
                        percolateUp(index, last_node);
                    }
                    else
                    {
                        percolateDown(index, last_node);
                    }
                }
            }
            add(node);
        }
        
        /**
         * Used to percolate a node up from the base of the heap based on f_score.
         * @param k The location to place the new node.
         * @param node The node to be placed.
         */
        void percolateUp(int k, PathNode node)
        {
            if(list.isEmpty())
            {
                return;
            }
            while (k > 0)
            {
                int parent = (k - 1) / 2;
                PathNode p = list.get(parent);
                if (node.compareTo(p) >= 0)
                {
                    break;
                }
                list.set(k, p);
                k = parent;
            }
            list.set(k, node);
        }
        
        /**
         * Used to percolate a node down the heap structure.
         * @param k The location to place the node at.
         * @param node The node to be placed.
         */
        void percolateDown(int k, PathNode node)
        {
            if (list.isEmpty())
            {
                return;
            }
            while (k < list.size() / 2)
            {
                int child = 2 * k + 1;
                if (child < list.size() - 1 && list.get(child).compareTo(list.get(child + 1)) > 0)
                {
                    child++;
                }
                if (node.compareTo(list.get(child)) <= 0)
                {
                    break;
                }
                list.set(k, list.get(child));
                k = child;
            }
            list.set(k, node);
        }
    }
    
    /**
     * Calculate path from the provided start tile to the provided finish tile, based on the movement limitations of the provided unit.
     * @param unit a unit object providing movement limitations to the A* algorithm.
     * @param start the tile to begin that path on.
     * @param finish the tile the algorithm is searching for.
     * @return a list of positions representing the path to follow.
     */
    public static List<Pair<Point, Integer>> calculatePath(Unit unit, Tile start, Tile finish)
    {
        List<PathNode> closed_set = new ArrayList<PathNode>();
        NodeMinHeap open_set = new NodeMinHeap();
        HashMap<PathNode, PathNode> came_from = new HashMap<PathNode, PathNode>();
        
        PathNode current_node;
        
        Point start_point = Game.getInstance().getMap().worldToMap(start.getPosition());
        Point end_point = Game.getInstance().getMap().worldToMap(finish.getPosition());
        
        PathNode end_node = new PathNode((int)end_point.y, (int)end_point.x, Integer.MAX_VALUE, Integer.MAX_VALUE);
        open_set.add(new PathNode( (int)start_point.y, (int)start_point.x, 0, (int)start_point.distance(end_point) ));
        
        
        while (!open_set.isEmpty())
        {
            current_node = open_set.peak();
            if(current_node.equals(end_node))
            {
                return reconstructPath(came_from, current_node);
            }
            
            open_set.pop();
            closed_set.add(current_node);
            
            for (PathNode neighbor_node : current_node.getNeighbors())
            {
                if(closed_set.contains(neighbor_node))
                {
                    continue;
                }
                
                int move_cost = unit.getMovementCostForTile(Game.getInstance().getMap().getTile(neighbor_node.col, neighbor_node.row));
                if(move_cost < 0 || move_cost > unit.m_max_energy)
                {
                    continue;
                }
                neighbor_node.g_score = current_node.g_score + move_cost;
                
                if (!open_set.contains(neighbor_node) || open_set.get(neighbor_node).g_score > neighbor_node.g_score)
                {
                    came_from.put(neighbor_node, current_node);
                    Point neighbor_point = new Point(neighbor_node.col, neighbor_node.row);
                    neighbor_node.f_score = neighbor_node.g_score + (int)Game.getInstance().getMap().getMapDistance(neighbor_point, end_point);
                    open_set.replace(neighbor_node);
                }
            }
        }
        
        return null;
    }
    
    /**
     * Used to calculate a final path from the resulting data in the A* algorithm by walking backwards.
     * @param came_from A hashmap containing the shortest route to each node.
     * @param current_node The current node to walk backwards from.
     * @return AThe final path calculated by A*.
     */
    private static List<Pair<Point, Integer>> reconstructPath(HashMap<PathNode, PathNode> came_from, PathNode current_node)
    {
        if(came_from.containsKey(current_node))
        {
            Point current_point = new Point(Game.getInstance().getMap().mapToWorld(current_node.col, current_node.row));
            List<Pair<Point, Integer>> path = reconstructPath(came_from, came_from.get(current_node));
            path.add(new Pair<Point, Integer>(current_point, current_node.g_score));
            return path;
        }
        else
        {
            Point current_point = new Point(Game.getInstance().getMap().mapToWorld(current_node.col, current_node.row));
            List<Pair<Point, Integer>> path = new ArrayList<Pair<Point, Integer>>();
            path.add(new Pair<Point, Integer>(current_point, current_node.g_score));
            return path;
        }
    }
}
