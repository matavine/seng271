/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.GameTime;
import engine.IDrawable;
import engine.IUpdateable;
import engine.ImageManager;
import engine.input.InputHandler;
import engine.structures.Pair;
import engine.ui.UIManager;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * The basic unit class, holds all functions that apply to all units.
 * @author Geoffrey Gollmer
 */
public class Unit 
    implements IDrawable, IUpdateable, ISelectable, ITurnListener
{
    protected final static int PATH_NODE_SIZE = 10;
    protected final static int PATH_STEP_SIZE = 24;
    protected final static Color[] PATH_COLORS = {new Color(132, 132, 255, 180), new Color(190, 190, 210, 180), new Color(140, 140, 150, 180), new Color(100, 100, 100, 180)};
    protected final static Color PATH_BORDER = new Color(0, 0, 0, 180);
    protected final static Image ATTACK_HIGHLIGHT = ImageManager.getInstance().getSprite("images/AttackHex.png");
    
    /**
     * The possible states of the unit.
     */
    protected enum UnitState
    {
        MOVING,
        IDLE,
        FORTIFIED;
    }
    /**
     * The possible states of control for units.
     */
    protected enum ControlState
    {
        QUICKPREDICTION,
        FULLPREDICTION,
        ATTACKORDER,
        NONE;
    }
    
    protected Player m_player;
    
    protected Point m_position;
    protected Tile m_tile;
    protected UnitState m_state;
    
    protected ControlState m_control_state;
    protected Tile m_target_tile;
    protected Tile m_destination;
    protected List<Pair<Point, Integer>> m_path;
    
    protected int m_max_energy;
    protected int m_attack_range;
    protected int m_max_health;
    protected float m_health;
    protected int m_energy;
    protected Image m_image;
    protected Image m_exhausted_image;
    protected boolean m_is_selected;
    protected int m_visibility_range;
    protected int m_attack;
    protected int m_defense;
    protected int m_cost;
    
    protected String m_unit_type_weakness;
    protected String m_unit_type;
    
    protected HashMap<Tile.TileType, Integer> m_tile_move_cost;
    
    /**
     * The unit constructor, used to create a new unit object. Should only be called from the unit factory.
     * @param data The unit data object storing the statistics to be applied to this unit object.
     * @param player The player controlling the selected unit.
     * @param tile The tile to create the unit on.
     */
    public Unit(UnitData data, Player player, Tile tile)
    {
        m_player = player;
        m_tile = tile;
        m_state = UnitState.IDLE;
        
        String debug = m_player.getColorExtension();
        //m_image = ImageManager.getInstance().getSprite(data.image_path);
        m_image = ImageManager.getInstance().filterSprite(data.image_path, data.image_path + m_player.getColorExtension(), player.getColor());
        m_exhausted_image = ImageManager.getInstance().greySprite(data.image_path);
        //m_image = ImageManager.getInstance().getSprite(data.image_path + m_player.getColorExtension());
        
        m_position = tile.getPosition();
        
        m_control_state = ControlState.NONE;
        m_target_tile = null;
        m_destination = null;
        
        m_max_energy = data.move_range;
        m_energy = data.move_range;
        m_attack_range = data.attack_range;
        m_max_health = data.max_health;
        m_health = data.max_health;
        m_visibility_range = data.visibility_range;
        
        m_is_selected = false;
        
        m_attack = data.attack;
        m_defense = data.defense;
        m_cost = data.cost;
        
        m_unit_type_weakness = data.unit_type_weakness;
        m_unit_type = data.unit_type;
        
        m_tile_move_cost = data.tile_move_cost;
    }
    
    /**
     * Used to get the maximum visibility range of the unit.
     * @return the integer visibility range in tiles for the unit.
     */
    public int getVisibilityRange()
    {
        return m_visibility_range;
    }
    
    /**
     * Used to find the world position of the unit.
     * @return the world position of the unit.
     */
    public Point getPosition()
    {
        return m_position;
    }
    
    /**
     * Used to get the player object controlling the unit.
     * @return the unit's controlling player
     */
    public Player getPlayer()
    {
        return m_player;
    }
    
    /**
     * Used to get the tile that the unit resides on.
     * @return the unit's current tile.
     */
    public Tile getTile()
    {
        return m_tile;
    }
    
    /**
     * Used to order the unit to attack the targeted tile.
     * @param target the tile to attack.
     */
    public void attack(Tile target)
    {
        if (m_energy >= m_tile_move_cost.get(target.getType()))
        {
            if (target.getUnit() != null)
            {
                handleAttackPrediction(target.getUnit());
            }
            else
            {
                handleAttackPrediction(target.getCity());
            }
            m_energy = 0;
        }
    }
    
    /**
     * Handles attacks on a city.
     * @param city city being attacked.
     */
    private void handleAttackPrediction(City city)
    {
        float damage;
        damage = calculateAttackDamage(city.getTile());
        
        city.takeDamage(damage);
        
        if (city.getHealth() <= 0)
        {
            if (m_unit_type.equals("Ranged"))
            {
                city.addHealth(1);
                m_destination = null;
            }
            else
            {
                city.changePlayer(m_player);
                m_destination = city.getTile();
                doMovementStep();
            }
        }
    }
    
    /**
     * Handles attacks on another unit.
     * @param target unit being attacked.
     */
    private void handleAttackPrediction(Unit target)
    {
        if (m_player.getSelectedTile() != null)
        {            
            float attacker_damage;
            float defender_damage = 0;
            
            attacker_damage = calculateAttackDamage(target.getTile());
            
            if (!(m_unit_type.equals("Ranged")))
            {
                defender_damage = target.calculateDefenseDamage(m_tile);
            }

            dealDamage(defender_damage);
            target.dealDamage(attacker_damage);

            if (target.getHealth() <= 0)
            {
                m_destination = target.m_tile;
                target.destroy();
                if (!(m_unit_type.equals("Ranged")))
                {
                    if (target.getTile().getCenterPosition() == null)
                    {
                        doMovementStep();
                    }
                }
                m_destination = null;
            }
            if (m_health <= 0)
            {
                this.destroy();
            }
        }
    }
    
    /**
     * Used to calculate the damage dealt by the unit based on HP and Tile based reductions.
     * @param t the tile being targeted for attack.
     * @return a float holding the amount of data to be dealt.
     */
    private float calculateAttackDamage(Tile t)
    {
        float damage = m_attack;
        float percent_modifier = 0;
        
        Point p = Game.getInstance().getMap().getTileIndex(m_tile.getCenterPosition());
        Tile adjacent_tiles[] = Game.getInstance().getMap().getNeighbors(p.y, p.x);   
        
        damage = damage * (m_health / m_max_health);
        
        if (!(m_unit_type.equals("Ranged")))
        {
            for (int i = 0; i < adjacent_tiles.length; i++)
            {
                if (adjacent_tiles[i].getUnit() != null && adjacent_tiles[i].getUnit().getPlayer() == m_player && adjacent_tiles[i].getUnit() != this)
                {
                    percent_modifier += 0.15f;
                }
            }
        }
        
        percent_modifier += m_tile.getType().getDefenseBonus();
        percent_modifier -= t.getType().getDefenseBonus();
        
        
        if (t.getUnit() != null)
        {
            if (t.getUnit().getUnitTypeWeakness().equals(m_unit_type) || t.getUnit().getUnitTypeWeakness().equals("All"))
            {
                percent_modifier += .5f;
            }
        }
        
        damage += (damage * percent_modifier);
        
        //Make sure damage dealt is at least 40% of regular damage
        if (damage < (m_attack * 0.4f))
        {
            damage = (m_attack * 0.4f);
        }
        
        return damage;
    }
    
    /**
     * Used to calculate damage dealt to units attacking this unit, based on tile advantage, and unit health.
     * @param t the tile that the unit is being attacked from.
     * @return A float representing the damage dealt back to the attacking unit.
     */
    private float calculateDefenseDamage(Tile t)
    {
        float damage = m_defense;
        float percent_modifier = 0;
        
        damage = damage * (m_health / m_max_health);

        percent_modifier += m_tile.getType().getDefenseBonus();
        percent_modifier -= t.getType().getDefenseBonus();
                
        
        damage += (damage * percent_modifier);
        
        //Make sure damage dealt is at least 40% of regular damage
        if (damage < (m_defense * 0.4f))
        {
            damage = (m_defense * 0.4f);
        }
        
        return damage;
    }
    
    /**
     * Used to deal damage to the current unit.
     * @param damage the damage to be dealt to the unit.
     */
    private void dealDamage(float damage)
    {
        m_health -= damage;
        if(m_health > 0.0f)
        {
            m_health = 0.0f;
        }
    }
    
    /**
     * Used to get the type of unit the current unit is weak against.
     * @return The unit type the current unit is weak against.
     */
    public String getUnitTypeWeakness()
    {
        return m_unit_type_weakness;
    }
    
    /**
     * Used to do a step towards the units target tile.
     */
    private void doMovementStep()
    {
        if(m_destination == null)
        {
            return;
        }
        
        if(m_tile == m_destination)
        {
            m_destination = null;
            return;
        }
        
        m_path = PathFinder.calculatePath(this, m_tile, m_destination);
        int energy_cost = 0;
        
        if(m_path == null)
        {
            return;
        }

        Point new_position = null;
        for(Pair<Point, Integer> point : m_path)
        {
            if(point.y > m_energy)
            {
                break;
            }
            
            new_position = point.x;
            energy_cost = point.y;
        }
        
        
        Tile new_tile = Game.getInstance().getMap().getTile(new_position);
        if(new_tile != null)
        {
            m_position = new_position;
            m_tile.setUnit(null);
            m_tile = new_tile;
            m_energy -= energy_cost;
            m_tile.setUnit(this);
            m_player.setSelectedTile(m_tile);
            m_player.broadcastUnitMove();
            
            if(new_tile == m_destination)
            {
                m_destination = null;
                m_path = null;
            }
            else
            {
                m_path = PathFinder.calculatePath(this, m_tile, m_destination);
            }
        }
    }
    
    /**
     * Used to get the unit's current health.
     * @return The unit's current health.
     */
    private float getHealth()
    {
        return m_health;
    }
    
    /**
     * Used to deal damage to the unit object.
     * @param damage The damage to be dealt.
     */
    public void dealAttackDamage(int damage)
    {
        dealDamage(damage);
    }
    
    /**
     * Used to set the current unit object to be selected.
     * @param value the selected value to be used (True for selected)
     */
    @Override
    public void setSelected(boolean value)
    {
        m_is_selected = value;
        
        if (m_is_selected)
        {
            displayUI();
            return;
        }
        
        hideUI();
    }
    
    /**
     * Used to check if the unit is currently selected.
     * @return True if the unit is selected.
     */
    @Override
    public boolean isSelected()
    {
        return m_is_selected;
    }
    
    /**
     * Used to display the specific unit's User Interface.
     */
    protected void displayUI()
    {
        UIManager.getInstance().displayUI("UnitUI", 2, this);
    }
    
    /**
     * Used to hide the specific unit's User Interface.
     */
    protected void hideUI()
    {
        UIManager.getInstance().hideUI("UnitUI");
    }
    
    /**
     * A call that informs the player their turn has just begun.
     */
    @Override
    public void onBeginTurn()
    {
        m_energy = m_max_energy;
        if(m_tile != m_destination)
        {
            doMovementStep();
        }
    }
    
    /**
     * A call that informs the player their turn has just ended.
     */
    @Override
    public void onEndTurn()
    {
        m_control_state = ControlState.NONE;
    }
    
    /**
     * Used to assess the movement cost of different types of tiles for specific unit types.
     * This should be redone for certain types of units. 
     * @param tile the tile to be assessed.
     * @return The movement cost of the specific tile.
     */
    public int getMovementCostForTile(Tile tile)
    {
        if(tile.getUnit() != null ||
            (tile.getCity() != null && tile.getCity().getPlayer() !=  m_player))
        {
            return -1;
        }
        if(!m_player.hasDiscoveredTile(tile.getPosition()))
        {
            return -1;
        }
        return m_tile_move_cost.get(tile.getType());
    }
    
    /**
     * Used to begin unit movement prediction, either through UI or hot keys.
     * @param full_prediction true if the lmb is to be used to confirm movement commands.
     */
    public void startMovementPrediction(boolean full_prediction)
    {
        if(full_prediction)
        {
            m_control_state = ControlState.FULLPREDICTION;
            m_player.lockLMB(true);
        }
        else
        {
            m_control_state = ControlState.QUICKPREDICTION;
        }
    }
    
    /**
     * Used to swap the unit's control state to attack.
     */
    public void startAttackCommand()
    {
        if(m_attack <= 0)
        {
            return;
        }
        
        m_control_state = ControlState.ATTACKORDER;
        m_player.lockLMB(true);
    }
    
    /**
     * Used to order the unit to move to the hovered tile.
     */
    private void submitMoveOrder()
    {
        Tile tile = m_player.getHoveredTile();
        if (!m_tile.equals(tile) && tile != null)
        {
            m_destination = tile;
            doMovementStep();
        }
        else
        {
            m_destination = null;
        }
    }
    
    /**
     * Used to update the unit each tick.
     * @param game_time a GameTime object to be used for time related calculations.
     */
    @Override
    public void update(GameTime game_time)
    {
        handleRightClick();
        handleMovementPrediction();
        handleAttackOrder();
    }
    
    /**
     * Used to handle right clicking in the units context.
     */
    private void handleRightClick()
    {
        if (InputHandler.getInstance().isInputDown(InputHandler.InputType.MOUSE, MouseEvent.BUTTON3)
                && InputHandler.getInstance().getMouseHandler().isMouseInComponent())
        {
            if(m_control_state == ControlState.NONE || m_control_state == ControlState.ATTACKORDER)
            {
                if(m_control_state == ControlState.ATTACKORDER)
                {
                    m_player.lockLMB(false);
                }
                startMovementPrediction(false);
            }
        }
    }
    
    /**
     * Used to handle movement prediction (Not commands) for a unit.
     */
    private void handleMovementPrediction()
    {
        
        if(m_control_state == ControlState.FULLPREDICTION || m_control_state == ControlState.QUICKPREDICTION)
        {
            Tile hovered_tile = m_player.getHoveredTile();
            if(hovered_tile != null && (hovered_tile != m_target_tile || m_target_tile == null))
            {
                if(m_player.hasDiscoveredTile(hovered_tile.getPosition()))
                {
                    m_path = PathFinder.calculatePath(this, m_tile, hovered_tile);
                }
                else
                {
                    m_target_tile = null;
                    m_path = null;
                }
            }
        }
        
        if(m_control_state == ControlState.FULLPREDICTION)
        {
            if(InputHandler.getInstance().isInputPressed(InputHandler.InputType.MOUSE, MouseEvent.BUTTON1)
                    && InputHandler.getInstance().getMouseHandler().isMouseInComponent())
            {
                submitMoveOrder();
                m_control_state = ControlState.NONE;
                m_target_tile = null;
                m_player.lockLMB(false);
            }
            else if(InputHandler.getInstance().isInputDown(InputHandler.InputType.MOUSE, MouseEvent.BUTTON3))
            {
                m_control_state = ControlState.QUICKPREDICTION;
                m_player.lockLMB(false);
            }
        }
        
        if(m_control_state == ControlState.QUICKPREDICTION)
        {
            if (InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.MOUSE, MouseEvent.BUTTON3)
                    && InputHandler.getInstance().getMouseHandler().isMouseInComponent())
            {
                submitMoveOrder();
                m_target_tile = null;
                m_control_state = ControlState.NONE;
            }
        }
    }
    
    /**
     * Used to handle attack orders for a unit.
     */
    public void handleAttackOrder()
    {
        if(InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_A))
        {
            startAttackCommand();
        }
        
        if(m_control_state == ControlState.ATTACKORDER)
        {
            if(InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.MOUSE, MouseEvent.BUTTON1))
            {
                List<Tile> potential_tiles = new ArrayList<Tile>(Arrays.asList(Game.getInstance().getMap().getTilesInRange(m_tile, m_attack_range)));

                if(!potential_tiles.contains(m_player.getHoveredTile()))
                {
                    m_control_state = ControlState.NONE;
                    m_player.lockLMB(false);
                    return;
                }
                
                if(m_player.getHoveredTile() == m_tile)
                {
                    m_control_state = ControlState.NONE;
                    m_player.lockLMB(false);
                    return;
                }
                
                if(m_player.getHoveredTile().getUnit() == null)
                {
                    if (m_player.getHoveredTile().getCity() == null)
                    {
                        return;
                    }
                    
                    if (m_player.getHoveredTile().getCity().getPlayer() != m_player)
                    {
                        attack(m_player.getHoveredTile());
                        m_control_state = ControlState.NONE;
                        m_player.lockLMB(false);
                    }
                        
                }
                else
                {
                    if(m_player.getHoveredTile().getUnit().getPlayer() == m_player)
                    {
                        return;
                    }

                    attack(m_player.getHoveredTile());
                    m_control_state = ControlState.NONE;
                    m_player.lockLMB(false);
                }
            }
        }
    }
    
    /**
     * Used to draw the unit's graphics in game.
     * @param graphics The graphics object to draw to.
     */
    @Override
    public void draw(Graphics graphics)
    {
        if (m_tile.getVisibility() != Tile.TileVisibility.VISIBLE)
        {
            return;
        }
        
        Point draw_position = Game.getInstance().getCamera().worldToScreenSpace(m_position);
        
        if(m_energy <= 0)
        {
            graphics.drawImage(m_exhausted_image, draw_position.x, draw_position.y, null);
        }
        else
        {
            graphics.drawImage(m_image, draw_position.x, draw_position.y, null);
        }
        
        graphics.setColor(Color.BLACK);
        graphics.drawRect(draw_position.x + 10, draw_position.y + 5, 20, 6);
        graphics.setColor(Color.RED);
        graphics.fillRect(draw_position.x + 11, draw_position.y + 6, 19, 5);
        graphics.setColor(Color.GREEN);
        graphics.fillRect(draw_position.x + 11, draw_position.y + 6, (int)(19 * ((float)m_health/(float)m_max_health)), 5);
    }
    
    /**
     * Used to draw the range polygon for the unit's attack radius.
     * @param graphics The graphics object to draw to.
     */
    public void drawAttackRange(Graphics graphics)
    {
        if(m_control_state == ControlState.ATTACKORDER)
        {
            Point draw_pos = (Point)m_tile.getPosition().clone();
            draw_pos = Game.getInstance().getCamera().worldToScreenSpace(draw_pos);
            Polygon range = calculatePolygon(draw_pos, m_attack_range);

            Graphics2D g2d = (Graphics2D)graphics;

            g2d.setStroke(new BasicStroke(2.0f));
            g2d.setColor(new Color(255, 0, 0, 48));
            g2d.fillPolygon(range);
            g2d.setColor(new Color(0, 0, 0, 128));
            g2d.drawPolygon(range);
            g2d.setStroke(new BasicStroke(1.0f));
            
            if(getPlayer().getHoveredTile() != null)
            {
                List<Tile> potential_tiles = new ArrayList<Tile>(Arrays.asList(Game.getInstance().getMap().getTilesInRange(m_tile, m_attack_range)));
                if(potential_tiles.contains(getPlayer().getHoveredTile()))
                {
                    draw_pos = (Point)getPlayer().getHoveredTile().getPosition().clone();
                    draw_pos = Game.getInstance().getCamera().worldToScreenSpace(draw_pos);
                    graphics.drawImage(ATTACK_HIGHLIGHT, draw_pos.x, draw_pos.y, null);
                }
            }
        }
    }
    
    /**
     * Used to calculate a hex based polygon of a certain range
     * TODO: Move to some sort of general graphics tools class?
     * @param center The center location of the polygon (In world space)
     * @param range The range of the polygon (In hex space)
     * @return A polygon object representing the hex based polygon.
     */
    public Polygon calculatePolygon(Point center, int range)
    {
        int n = 2*( (6*range) + 3);
        int ar_x[] = new int[n];
        int ar_y[] = new int[n];
        int i_offset = 0;
        Point c_pos = (Point)center.clone();
        c_pos.translate(Tile.TILE_WIDTH/4, (Tile.TILE_HEIGHT*range*-1));
        
        for(int i=0; i<(2*range); i++)
        {
            if(i%2 == 0)
            {
                c_pos.translate(Tile.TILE_WIDTH/4*-1, Tile.TILE_HEIGHT/2);
            }
            else
            {
                c_pos.translate(Tile.TILE_WIDTH/2*-1, 0);
            }
            ar_x[i_offset + i] = c_pos.x;
            ar_y[i_offset + i] = c_pos.y;
        }
        i_offset += (2*range);
        
        for(int i=0; i<((2*range)+1); i++)
        {
            if(i%2 == 0)
            {
                c_pos.translate(Tile.TILE_WIDTH/4*-1, Tile.TILE_HEIGHT/2);
            }
            else
            {
                c_pos.translate(Tile.TILE_WIDTH/4, Tile.TILE_HEIGHT/2);
            }
            ar_x[i_offset + i] = c_pos.x;
            ar_y[i_offset + i] = c_pos.y;
        }
        i_offset += (2*range)+1;
        
        for(int i=0; i<((2*range)+2); i++)
        {
            if(i%2 == 0)
            {
                c_pos.translate(Tile.TILE_WIDTH/4, Tile.TILE_HEIGHT/2);
            }
            else
            {
                c_pos.translate(Tile.TILE_WIDTH/2, 0);
            }
            ar_x[i_offset + i] = c_pos.x;
            ar_y[i_offset + i] = c_pos.y;
        }
        i_offset += (2*range)+2;
        
        for(int i=0; i<(2*range); i++)
        {
            if(i%2 == 0)
            {
                c_pos.translate(Tile.TILE_WIDTH/4, Tile.TILE_HEIGHT/2*-1);
            }
            else
            {
                c_pos.translate(Tile.TILE_WIDTH/2, 0);
            }
            ar_x[i_offset + i] = c_pos.x;
            ar_y[i_offset + i] = c_pos.y;
        }
        i_offset += (2*range);
        
        for(int i=0; i<((2*range)+1); i++)
        {
            if(i%2 == 0)
            {
                c_pos.translate(Tile.TILE_WIDTH/4, Tile.TILE_HEIGHT/2*-1);
            }
            else
            {
                c_pos.translate(Tile.TILE_WIDTH/4*-1, Tile.TILE_HEIGHT/2*-1);
            }
            ar_x[i_offset + i] = c_pos.x;
            ar_y[i_offset + i] = c_pos.y;
        }
        i_offset += (2*range)+1;
        
        for(int i=0; i<((2*range)+2); i++)
        {
            if(i%2 == 0)
            {
                c_pos.translate(Tile.TILE_WIDTH/4*-1, Tile.TILE_HEIGHT/2*-1);
            }
            else
            {
                c_pos.translate(Tile.TILE_WIDTH/2*-1, 0);
            }
            ar_x[i_offset + i] = c_pos.x;
            ar_y[i_offset + i] = c_pos.y;
        }
        i_offset += (2*range)+2;
        
        return new Polygon(ar_x, ar_y, n);
    }
    
    /**
     * Used to draw the current calculated path for the unit.
     * @param graphics The graphics object to draw to.
     */
    public void drawPath(Graphics graphics)
    {
        if(m_path != null)
        {
            int current_step = m_energy - m_max_energy;
            int turn_step = 1;
            int color_step = 0;
            
            Point previous_point = null;
            for (Pair<Point, Integer> p : m_path)
            {
                Point pos = p.x;
                Point mid_pos;
                pos = Game.getInstance().getCamera().worldToScreenSpace(pos);
                pos.translate(Tile.TILE_WIDTH/2, Tile.TILE_HEIGHT/2);
                
                if(previous_point == null)
                {
                    previous_point = pos;
                    continue;
                }
                
                mid_pos = new Point((previous_point.x + pos.x)/2, (previous_point.y + pos.y)/2);
                
                if(p.y > current_step + m_max_energy)
                {
                    if(color_step < PATH_COLORS.length - 1)
                    {
                        color_step++;
                    }
                    turn_step++;
                    current_step = p.y - getMovementCostForTile(Game.getInstance().getMap().getTile(p.x));
                }
                
                if(m_path.indexOf(p) == m_path.size()-1)
                {
                    drawPathNode(graphics, mid_pos, PATH_COLORS[color_step]);
                    drawStepMarker(graphics, pos, PATH_COLORS[color_step], turn_step);
                }
                else if(m_path.get(m_path.indexOf(p)+1).y > current_step + m_max_energy)
                {
                    drawPathNode(graphics, mid_pos, PATH_COLORS[color_step]);
                    drawStepMarker(graphics, pos, PATH_COLORS[color_step], turn_step); 
                    
                    if(color_step < PATH_COLORS.length - 1)
                    {
                        color_step++;
                    }
                    turn_step++;
                    current_step = p.y;
                }
                else
                {
                    drawPathNode(graphics, mid_pos, PATH_COLORS[color_step]);
                    drawPathNode(graphics, pos, PATH_COLORS[color_step]);
                }
                previous_point = pos;
            }
        }
    }
    
    /**
     * Used to draw a step marker in the units path, indicating how many turns are required to get to the point in the path.
     * @param graphics The graphics object to draw to.
     * @param pos The position of the marker.
     * @param color The color of the marker.
     * @param step The number to indicate in the marker.
     */
    private void drawStepMarker(Graphics graphics, Point pos, Color color, int step)
    {
        if (Game.getInstance().getCamera().isInView(pos))
        {
            graphics.setColor(color);
            graphics.fillOval( pos.x - PATH_STEP_SIZE/2, pos.y - PATH_STEP_SIZE/2, PATH_STEP_SIZE, PATH_STEP_SIZE);

            graphics.setColor(PATH_BORDER);
            graphics.drawOval( pos.x - PATH_STEP_SIZE/2, pos.y - PATH_STEP_SIZE/2, PATH_STEP_SIZE, PATH_STEP_SIZE);
            
            String str_step = Integer.toString(step);
            int x_offset = -3*(str_step.length()-1);
            graphics.setColor(Color.BLACK);
            graphics.drawString(str_step, pos.x-3+x_offset, pos.y+5);
        }
    }
    
    /**
     * Used to draw a small node in the units path.
     * @param graphics The graphics object to draw to.
     * @param pos The position to draw the node at.
     * @param color The color of the node.
     */
    private void drawPathNode(Graphics graphics, Point pos, Color color)
    {
        if (Game.getInstance().getCamera().isInView(pos))
        {
            graphics.setColor(color);
            graphics.fillOval( pos.x - PATH_NODE_SIZE/2, pos.y - PATH_NODE_SIZE/2, PATH_NODE_SIZE, PATH_NODE_SIZE);

            graphics.setColor(PATH_BORDER);
            graphics.drawOval( pos.x - PATH_NODE_SIZE/2, pos.y - PATH_NODE_SIZE/2, PATH_NODE_SIZE, PATH_NODE_SIZE);
        }
    }
    
    /**
     * Removes the unit from the game.
     */
    public void destroy()
    {        
        m_player.removeUnit(this);
        m_tile.setUnit(null);
    }
}
