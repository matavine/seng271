/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 * Listens to the player's activities during their turn.
 * @author michael
 */
public interface IPlayerListener 
{
    /**
     * Called whenever a player moves one of their units.
     */
    public void onUnitMove();
}
