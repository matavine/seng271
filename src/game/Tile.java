/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.IDrawable;
import engine.ImageManager;
import java.awt.*;
import java.util.Random;

/**
 *
 * @author Michael
 */
public class Tile 
    implements IDrawable
{
    public final static int TILE_WIDTH = 80;
    public final static int TILE_HEIGHT = 60;
    
    private Point m_position;
    private TileType m_type;
    private float m_value;
    private Unit m_unit;
    private City m_city;
    private TileVisibility m_visibility;
    
    /**
     * Once the active player has discovered the current tile, this determines if it is in vision range of one of their units or not.
     */
    public enum TileVisibility
    {
        VISIBLE,
        FADED;
    };
    
    /**
     * TileType dictates the specific values and settings associated with a tile.
     */
    public enum TileType
    {
        LAND (ImageManager.getInstance().getSprite("images/GroundHex.png"), 0, 0, 0.20f),
        WATER (ImageManager.getInstance().getSprite("images/WaterHex.png"), 1, 1, 0.0f),
        GLACIER (ImageManager.getInstance().getSprite("images/GlacierHex.png"), 0, 0, -0.33f),
        DESERT (ImageManager.getInstance().getSprite("images/DesertHex.png"), 0, 0, -0.33f),
        PLAINS (ImageManager.getInstance().getSprite("images/PlainsHex.png"), 4 , 4, -0.33f),
        FOREST (ImageManager.getInstance().getSprite("images/ForestHex.png"), 4, 6, 0.25f),
        MOUNTAIN (ImageManager.getInstance().getSprite("images/MountainHex.png"), 1, 10, 0.4f),
        GRASS (ImageManager.getInstance().getSprite("images/GrassHex.png"), 4, 4, -0.33f),
        JUNGLE (ImageManager.getInstance().getSprite("images/JungleHex.png"), 4, 6, 0.25f),
        SWAMP (ImageManager.getInstance().getSprite("images/SwampHex.png"), 0, 0, -0.33f),
        HILL (ImageManager.getInstance().getSprite("images/HillHex.png"), 2, 2, 0.25f),
        TUNDRA (ImageManager.getInstance().getSprite("images/TundraHex.png"), 1, 1, -0.33f);
      
        private int min_income;
        private int max_income;
        private float defense_bonus;

        /**
        * Used to get the maximum income of the tile type.
        * @return The maximum income of the tile type.
        */
        public int getMaxIncome()
        {
            return max_income;
        }
        
        /**
         * Used to get the defensive bonus of the tile.
         * @return The defensive bonus provided by the tile.
         */
        public float getDefenseBonus()
        {
            return defense_bonus;
        }

        /**
        * Used to get a random value between the maximum and minimum income of the tiel type.
        * @return A random income from the tile type.
        */
        public int getIncome()
        {
            if (min_income == max_income)
            {
                return min_income;
            }

            Random rand = new Random();
            return min_income + rand.nextInt(max_income - min_income);
        }

        private Image image;
        /**
        * Used to get the image used when rendering a tile of this type tile.
        * @return The image used when rendering.
        */
        public Image getTileImage()
        {
            return image;
        }
        
        /**
         * Used to construct a tile type object.
         * @param image The image used to render the tile.
         * @param min_income The minimum income provided by the tile.
         * @param max_income The maximum income provided by the tile.
         * @param defense_bonus The defensive bonus provided by the tile.
         */
        private TileType(Image image, int min_income, int max_income, float defense_bonus)
        {
            this.image = image;
            this.min_income = min_income;
            this.max_income = max_income;
            this.defense_bonus = defense_bonus;
        }
    };
    
    /**
     * Creates a new tile with the provided position and type.
     * @param position The position to place the top left corner of the tile in.
     * @param type The type of the tile being created.
     */
    public Tile(Point position, TileType type)
    {
        m_position = position;
        m_type = type;
        m_value = 0f;
        m_visibility = TileVisibility.FADED;
    }
    
    /**
     * Creates a new tile with the provided position, type and value.
     * @param position The position to place the top left corner of the tile in.
     * @param type The type of tile being created.
     * @param value The value to save to the tile (For debug text)
     */
    public Tile(Point position, TileType type, float value)
    {
        m_position = position;
        m_type = type;
        m_value = ((int)(value * 1000)) / 1000f;
        m_visibility = TileVisibility.FADED;
    }
    
    /**
     * Sets the visibility of the tile.
     * @param visibility The visibility value to set the tile to.
     */
    public void setVisibility(TileVisibility visibility)
    {
        m_visibility = visibility;
        if(visibility == TileVisibility.VISIBLE && m_city != null)
        {
            m_city.informSeen();
        }
    }
    
    /**
     * Used to get the visibility value of the tile.
     * @return The tile's visibility value.
     */
    public TileVisibility getVisibility()
    {
        return m_visibility;
    }
    
    /**
     * Used to place a unit on a tile. (Only one unit may be placed on any given tile)
     * @param unit A reference to the unit placed on the tile.
     */
    public void setUnit(Unit unit)
    {
        m_unit = unit;
    }
    
    /**
     * Used to check the unit standing on the tile.
     * @return The unit current occupying the tile. null if no unit is on the tile.
     */
    public Unit getUnit()
    {
        return m_unit;
    }
    
    /**
     * Used to apply a city to a tile.
     * @param city the newly created city to attach to this tile.
     */
    public void setCity(City city)
    {
        if(m_city == null)
        {
            m_city = city;
        }
    }
    
    /**
     * Used to get the city of this tile.
     * @return The city on this tile (Null if no city exists)
     */
    public City getCity()
    {
        return m_city;
    }
    
    /**
     * Used to get the position of the top left corner of the tile.
     * @return The position of the tile.
     */
    public Point getPosition()
    {
        return m_position;
    }
    
    /**
     * Used to find the center position of the tile (usually for drawing purposes)
     * @return The position at the center of the tile.
     */
    public Point getCenterPosition()
    {
        return new Point(m_position.x + (TILE_WIDTH / 2), m_position.y + (TILE_HEIGHT / 2));
    }
    
    /**
     * Used to get the inner rectangle of the tile (ignoring the tapered edges)
     * @return The inner rectangle of the tile.
     */
    public Rectangle getInnerRect()
    {
        int inner_rect_width = TILE_WIDTH / 2;
        int inner_rect_height = TILE_HEIGHT;
        
        return new Rectangle(m_position.x + (TILE_WIDTH / 4), m_position.y, inner_rect_width, inner_rect_height);
    }
    
    /**
     * Used to get the bounding box of the tile, this box is the bounds of the tile image.
     * @return The bounding box of the tile.
     */
    public Rectangle getTileBound()
    {
        return new Rectangle(m_position.x, m_position.y, TILE_WIDTH, TILE_HEIGHT);
    }
    
    /**
     * Used to get the type of the tile.
     * @return The tile's type.
     */
    public TileType getType()
    {
        return m_type;
    }
    
    /**
     * Used to render the tile, and it's contents.
     * @param graphics A graphics object is required to draw the tile.
     */
    @Override
    public void draw(Graphics graphics)
    {
        if (!Game.getInstance().getPlayer().hasDiscoveredTile(m_position))
        {
            return;
        }
        
        Point draw_position = Game.getInstance().getCamera().worldToScreenSpace(m_position);
        
        graphics.drawImage(this.getType().getTileImage(), draw_position.x, draw_position.y, null);
        
        if (Game.DEBUG_MODE)
        {
            graphics.setColor(new Color(1f, 1f, 0f, 1f));
            graphics.drawString("" + m_value, draw_position.x + (TILE_WIDTH / 3), draw_position.y + (TILE_HEIGHT / 2) + 5);
        }
    }
}
