/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.IDrawable;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 * The map class contains all code related to the game map, and all assisting functions.
 * @author Michael
 */
public class Map 
    implements IDrawable, IPlayerListener, ITurnListener
{   
    private int m_tile_width = 0;
    private int m_tile_height = 0;
    private int m_map_width = 64;
    private int m_map_height = 64;
    private int m_diamond_square_size = 0;
    
    private static Random m_random;
    private Tile[][] m_map;
    
    private static ImageIcon m_undiscovered_hex;
    private static ImageIcon m_faded_hex;
    private List<Tile> m_visible_tiles;
    
    /**
     * The map object is used to store and operate on the in game map, as well as generate new maps to play on.
     */
    public Map()
    {
        m_random = new Random();
        //m_mini_map_buffer = new BufferedImage(MINI_MAP_WIDTH, MINI_MAP_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        
        m_tile_width = Tile.TILE_WIDTH * 3/4;
        m_tile_height = Tile.TILE_HEIGHT;
        
        m_visible_tiles = new ArrayList<Tile>();
        
        initOverlayHex();
        
        //Game.getInstance().addTurnListener(this);
    }
    
    /**
     * Used to create the images displayed for fog of war hexes.
     */
    private void initOverlayHex()
    {
        if (m_undiscovered_hex != null || m_faded_hex != null)
        {
            return;
        }
        
        Polygon overlay_hexagon = new Polygon();
        overlay_hexagon.addPoint(0, 30);
        overlay_hexagon.addPoint(20, 0);
        overlay_hexagon.addPoint(60, 0);
        overlay_hexagon.addPoint(80, 30);
        overlay_hexagon.addPoint(60, 60);
        overlay_hexagon.addPoint(20, 60);
        
        BufferedImage hex_buffer = new BufferedImage(Tile.TILE_WIDTH, Tile.TILE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics graphics = hex_buffer.createGraphics();
        graphics.setColor(Color.BLACK);
        graphics.fillPolygon(overlay_hexagon);
        m_undiscovered_hex = new ImageIcon(hex_buffer);
        
        hex_buffer = new BufferedImage(Tile.TILE_WIDTH, Tile.TILE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        graphics = hex_buffer.createGraphics();
        graphics.setColor(new Color(0f, 0f, 0f, 0.5f));
        graphics.fillPolygon(overlay_hexagon);
        m_faded_hex = new ImageIcon(hex_buffer);
    }
    
    /**
     * Gets the actual map data, be careful with this data, as it can be used to edit the map itself.
     * @return 2D tile array containing the map's data.
     */
    public Tile[][] getMap()
    {
        return m_map;
    }
    
    /**
     * Used to copy the map contents to a single list object.
     * @param tiles The list to copy the map to.
     */
    private void copyMapToList(List<Tile> tiles)
    {
        for(int row = 0; row < m_map_height; row++)
        {
            for(int col = 0; col < m_map_width; col++)
            {
                tiles.add(m_map[row][col]);
            }
        }
    }
    
    /**
     * Spawns a set of players on the current map, finding ideal locations for each.
     * @param players A list of player objects to spawn units for on the map.
     */
    public void spawnPlayers(Player[] players)
    {
        int lowest_income_threshold = 19;
        int min_distance_threshold = (int)((Math.min(getMapPixelWidth(), getMapPixelHeight()) / players.length));
        List<Tile> tiles = new ArrayList<Tile>();        
        copyMapToList(tiles);
        
        Point[] player_positions = new Point[players.length];
        long start_time = System.currentTimeMillis();
        
        for (int p_index = 0; p_index < players.length; p_index++)
        {
            while (true)
            {
                long current_time = System.currentTimeMillis();
                long delta_time = current_time - start_time;
                if (delta_time >= 1000)
                {
                    // If the process takes too long to find a
                    // point on the map, lower the distance 
                    // threshold to make it easier to find one.
                    start_time = current_time;
                    min_distance_threshold = (int)(min_distance_threshold * 0.8f);
                }
                
                if(tiles.size() < players.length - p_index)
                {
                    p_index = 0;
                    generateTerrain(m_map_width, m_map_height);
                    tiles.clear();
                    copyMapToList(tiles);
                }
                
                int tile_pos = tiles.size() > 1 ? m_random.nextInt(tiles.size() - 1) : 0;
                
                Tile tile = tiles.get(tile_pos);
                if (tile.getType() == Tile.TileType.WATER || tile.getType() == Tile.TileType.MOUNTAIN || tile.getUnit() != null)
                {
                    tiles.remove(tile_pos);
                    continue;
                }
                
                int total_income = 0;
                Point tile_index = getTileIndex(tile.getCenterPosition());
                Tile[] neighbors = getNeighbors(tile_index.y, tile_index.x);
                for (Tile neighbor : neighbors)
                {
                    total_income += neighbor.getType().getMaxIncome();
                }
                
                if (total_income >= lowest_income_threshold)
                {
                    boolean passed_dist_threshold = true;
                    for (int i = 0; i < p_index; i++)
                    {
                        float delta_distance = getWorldDistance(tile.getPosition(), player_positions[i]);
                        if (delta_distance < min_distance_threshold)
                        {
                            passed_dist_threshold = false;
                            break;
                        }
                    }
                    
                    if (!passed_dist_threshold)
                    {
                        tiles.remove(tile_pos);
                        continue;
                    }
                    
                    player_positions[p_index] = tile.getCenterPosition();
                    players[p_index].getCamera().centerPosition(tile.getCenterPosition());
                    players[p_index].spawn(tile);
                    break;
                }
                else
                {
                    tiles.remove(tile_pos);
                }
            }
        }
    }
    
    /**
     * Finds all adjacent tiles to the tile at the provided location.
     * @param row the row or y value of the tile in map space.
     * @param col the column or x value of the tile in map space.
     * @return A list of tile objects representing the adjacent tiles to the provided location.
     */
    public Tile[] getNeighbors(int row, int col)
    {
        List<Tile> neighbors = new LinkedList<Tile>();

        int temp_row_top = row - 1;
        int temp_row_bottom = row + 1;
        int temp_col_left = col - 1;
        int temp_col_right = col + 1;

        if(temp_col_left < 0)
            temp_col_left = m_map_width - 1;
        
        if(temp_col_right > m_map_width - 1)
            temp_col_right = 0;

        if(row > 0)
            neighbors.add(m_map[temp_row_top][col]);
        
        if(row < Game.getInstance().getMap().getMapHeight() - 1)
            neighbors.add(m_map[temp_row_bottom][col]);

        if(col%2 == 0)
        {
            if(row > 0)
            {
                neighbors.add(m_map[temp_row_top][temp_col_left]);
                neighbors.add(m_map[temp_row_top][temp_col_right]);
            }
        }
        else
        {
            if(row < Game.getInstance().getMap().getMapHeight() - 1)
            {
                neighbors.add(m_map[temp_row_bottom][temp_col_left]);
                neighbors.add(m_map[temp_row_bottom][temp_col_right]);
            }
        }

        neighbors.add(m_map[row][temp_col_left]);
        neighbors.add(m_map[row][temp_col_right]);

        Tile[] result = new Tile[neighbors.size()];
        neighbors.toArray(result);
        return result;
    }
    
    /**
     * Converts from world space to map space.
     * @param position
     * @return a copy of the provided position in map space.
     */
    public Point worldToMap(Point position)
    {
        int row = (position.y / m_tile_height);        
        int col = (position.x / m_tile_width);
        
        if (row >= m_map_height && col % 2 == 0)
        {
            int tmp_row = row - 1;
            int tmp_col = col - 1;
            
            if (tmp_col < 0)
            {
                tmp_col += m_map_width;
            }

            row = tmp_row;
            col = tmp_col;
        }
        
        if (col % 2 != 0)
        {
            int y_position = (position.y - (m_tile_height / 2));
            row = (y_position / m_tile_height);
            
            if (y_position < 0)
            {
                int tmp_col = col - 1;
                if (tmp_col < 0)
                {
                    col += m_map_width;
                }
                
                col = tmp_col;
            }
        }
        
        return new Point(col, row);
    }
    
    /**
     * Converts from map position to world position.
     * @param col the column or x value in map space.
     * @param row the row or y value in map space.
     * @return a Point object representing the location in world space.
     */
    public Point mapToWorld(int col, int row)
    {
        Point world_position;
        if (col % 2 == 0)
        {
            world_position = new Point(m_tile_width * col, m_tile_height * row);
        }
        else
        {
            world_position = new Point(m_tile_width * col, ( (m_tile_height * row) + (m_tile_height/2) ));
        }
        
        return world_position;
    }
    
    /**
     * Calculates the distance between two points in world space using pythagorus' theorem.
     * @param a the first point.
     * @param b the second point.
     * @return The calculated distance as a float.
     */
    public float getWorldDistance(Point a, Point b)
    {
        float distance;
        float shifted_distance;
        
        distance = (float)a.distance(b);
        
        a = (Point)a.clone();
        a.translate(m_map_width * m_tile_width, 0);
        shifted_distance = (float)a.distance(b);
        
        if(shifted_distance < distance)
        {
            distance = shifted_distance;
        }
        
        a.translate(m_map_width * m_tile_width * -2, 0);
        shifted_distance = (float)a.distance(b);
        
        if(shifted_distance < distance)
        {
            distance = shifted_distance;
        }
        return distance;
    }
    
    /**
     * Calculates the shortest translation required in order to move from position a to position b;
     * @param a the starting point of the difference or translation.
     * @param b the destination of the difference or translation.
     * @return a point representing the translation required to get from a to b.
     */
    public Point getWorldDifference(Point a, Point b)
    {
        int x_diff = b.x - a.x;
        int y_diff = b.y - a.y;
        
        int temp_dist = (b.x - Game.getInstance().getMap().getMapPixelWidth()) - a.x;
        if(Math.abs(temp_dist) < Math.abs(x_diff))
        {
            x_diff = temp_dist;
        }
        
        temp_dist = (b.x + Game.getInstance().getMap().getMapPixelWidth()) - a.x;
        if(Math.abs(temp_dist) < Math.abs(x_diff))
        {
            x_diff = temp_dist;
        }
        
        return new Point(x_diff, y_diff);
    }
    
    /**
     * Calculates the distance between two points in map space using pythagorus' theorem.
     * @param a the first point.
     * @param b the second point.
     * @return The calculated distance as a float.
     */
    public float getMapDistance(Point a, Point b)
    {
        float distance;
        float shifted_distance;
        
        distance = (float)a.distance(b);
        
        a = (Point)a.clone();
        a.translate(m_map_width, 0);
        shifted_distance = (float)a.distance(b);
        
        if(shifted_distance < distance)
        {
            distance = shifted_distance;
        }
        
        a.translate(m_map_width * -2, 0);
        shifted_distance = (float)a.distance(b);
        
        if(shifted_distance < distance)
        {
            distance = shifted_distance;
        }
        return distance;
    }

    /**
     * Used to get a tile object from the map using a position in map space.
     * @param x the x position or column of the map tile.
     * @param y the y position or row of the map tile.
     * @return The tile object stored at the provided location. null if an incorrect location is provided.
     */
    public Tile getTile(int x, int y)
    {
        if (m_map == null || 
            x < 0 || x >= m_map_width || 
            y < 0 || y >= m_map_height)
        {
            return null;
        }
        
        return m_map[y][x];
    }
    
    /**
     * Used to get a tile object from the map using a position in world space.
     * @param position the position in world space that is within the tile.
     * @return The tile object at the provided location.
     */
    public Tile getTile(Point position)
    {
        if (m_map == null)
        {
            return null;
        }
        
        Point map_pos = worldToMap(position);
        
        if (map_pos.y < 0 || map_pos.y >= m_map_height ||
            map_pos.x < 0 || map_pos.x >= m_map_width)
        {
            return null;
        }
        
        Rectangle tile_bound = m_map[(int)map_pos.y][(int)map_pos.x].getTileBound();
        if (!tile_bound.contains(position))
        {
            return null;
        }
        
        return m_map[(int)map_pos.y][(int)map_pos.x];
    }
    
    /**
     * Used to get the tile index of a point (Is this worldToMap?)
     * @param position The position to convert to map space
     * @return index of the tile in map space. null if position is out of map bounds.
     */
    public Point getTileIndex(Point position)
    {
        if (m_map == null)
        {
            return null;
        }
        
        Point map_pos = worldToMap(position);
        
        if (map_pos.y < 0 || map_pos.y >= m_map_height ||
            map_pos.x < 0 || map_pos.x >= m_map_width)
        {
            return null;
        }
        
        Rectangle tile_bound = m_map[(int)map_pos.y][(int)map_pos.x].getTileBound();
        if (!tile_bound.contains(position))
        {
            return null;
        }
        
        return new Point((int)map_pos.x, (int)map_pos.y);
    }
    
    /**
     * Loads a map from the specified file.
     * @param mapFile the path tot he file you wish to load.
     */
    public void loadTerrain(String mapFile)
    {
        int rowNum = 0;
        int colNum = 0;
        char identifier;
        Tile.TileType type;
        String line = null;
        FileReader input;
        BufferedReader buffer;

        try
        {
            input = new FileReader(mapFile);
            buffer = new BufferedReader(input);
            line = buffer.readLine();
        }
        catch(FileNotFoundException e)
        {
            System.err.println("There ain't no such file, fool! " + e.getMessage());
            return;
        }
        catch(IOException e)
        {
            System.err.println("Hurr durr IOException, what do? " + e.getMessage());
            return;
        }

        while(line != null)
        {
            if(line.indexOf("width") != -1)
            {
                m_map_width = (Integer.parseInt(line.replaceAll("[\\D]", "")));
                m_map = new Tile[m_map_height][m_map_width];
            }
            else if(line.indexOf("height") != -1)
            {
                m_map_height = (Integer.parseInt(line.replaceAll("[\\D]", "")));
                m_map = new Tile[m_map_height][m_map_width];
            }
            else if(line.startsWith("t"))
            {
                colNum = 0;
                for(int i = 6; i<(line.length()-1); i++)
                {
                    identifier = line.charAt(i);
                    switch(identifier)
                    {
                        case ' ':
                            type = Tile.TileType.WATER;
                            break;
                        case 'm':
                            type = Tile.TileType.MOUNTAIN;
                            break;
                        case 'h':
                            type = Tile.TileType.HILL;
                            break;
                        case 'd':
                            type = Tile.TileType.DESERT;
                            break;
                        case 'p':
                            type = Tile.TileType.PLAINS;
                            break;
                        case 'g':
                            type = Tile.TileType.GRASS;
                            break;
                        case 's':
                            type = Tile.TileType.SWAMP;
                            break;
                        case 'a':
                            type = Tile.TileType.GLACIER;
                            break;
                        case 't':
                            type = Tile.TileType.TUNDRA;
                            break;
                        case 'f':
                            type = Tile.TileType.FOREST;
                            break;
                        case 'j':
                            type = Tile.TileType.JUNGLE;
                            break;
                        default:
                            type = Tile.TileType.LAND;
                            break;
                    }

                    Point block_position;
                    if (colNum % 2 == 0)
                    {
                        block_position = new Point(m_tile_width * colNum, m_tile_height * rowNum);
                    }
                    else
                    {
                        block_position = new Point(m_tile_width * colNum, ( (m_tile_height * rowNum) + (m_tile_height/2) ));
                    }
                    m_map[rowNum][colNum++] = new Tile(block_position, type);
                }
                rowNum++;
            }
            try
            {
                line = buffer.readLine();
            }
            catch(IOException e)
            {
                System.err.println("Hurr durr IOException, what do?");
                return;
            }
        }
    }
    
    /**
     * Generates a new map of the provided width and height.
     * @param width the width in tiles of the map to generate.
     * @param height the height in tiles of the map to generate.
     */
    public void generateTerrain(int width, int height)
    {   
        m_map = new Tile[width][height];
        m_map_width = width;
        m_map_height = height;
        m_diamond_square_size = m_map_width + 1;
        
        float[][] map_values = new float[m_diamond_square_size][m_diamond_square_size];
        
        initializeMapValues(map_values);
        diamondSquare(map_values, m_diamond_square_size, 5);
        averageMapValues(map_values);
        averageMapValues(map_values);
        setupTerrain(map_values);
    }
    
    /**
     * Used to initialize a randomly generated map's values. More work is done before the map is used.
     * @param map_values The set of data to initialize.
     */
    private void initializeMapValues(float[][] map_values)
    {
        map_values[0][0] = getRandomDisplacement(10);
        map_values[(m_diamond_square_size / 2)][0] = getRandomDisplacement(10);
        map_values[0][(m_diamond_square_size / 2)] = getRandomDisplacement(10);
        map_values[(m_diamond_square_size / 2)][(m_diamond_square_size / 2)] = getRandomDisplacement(10);
        
        for (int i = 0; i < m_diamond_square_size; i++)
        {
            map_values[m_diamond_square_size - 1][i] = getRandomDisplacement(10);
            map_values[i][m_diamond_square_size - 1] = getRandomDisplacement(10);
        }
    }
    
    /**
     * Used to create the actual tiles from generated map data.
     * @param map_values The generated map values used to generate tiles.
     */
    private void setupTerrain(float[][] map_values)
    {
        for (int row = 0; row < m_map_height; row++)
        {
            for (int col = 0; col < m_map_width; col++)
            {   
                float tile_value = map_values[row][col];
                Tile.TileType tile_type;
                
                if(tile_value <= -0.3f)
                {
                    tile_type = Tile.TileType.WATER;
                }
                else
                {
                    if(tile_value <= -0.25f)
                    {
                        tile_type = Tile.TileType.SWAMP;
                    }
                    else if(tile_value <= -0.15f)
                    {
                        tile_type = Tile.TileType.PLAINS;
                    }
                    else if(tile_value <= 0.00f)
                    {
                        tile_type = Tile.TileType.GRASS;
                    }
                    else if(tile_value <= 0.20f)
                    {
                        tile_type = Tile.TileType.FOREST;
                    }
                    else if(tile_value <= 0.35f)
                    {
                        tile_type = Tile.TileType.HILL;
                    }
                    else
                    {
                        tile_type = Tile.TileType.MOUNTAIN;
                    }
                    
                    // Glaciers!                    // TODO: There must be a cleaner way to do this ><
                    if(row + 1 < (m_map_height)/16 || row > (m_map_height)/16*15)
                    {
                        if((row <= 0 || row >= m_map_height-1))
                        {
                            tile_type = Tile.TileType.GLACIER;
                        }
                        else if(row <= 1 || row >= m_map_height-2)
                        {
                            tile_type = m_random.nextBoolean() ? Tile.TileType.GLACIER : Tile.TileType.TUNDRA;
                        }
                        else if(m_random.nextBoolean())
                        {
                            tile_type = Tile.TileType.TUNDRA;
                        }
                    }
                    
                }
                
                Point block_position;
                if (col % 2 == 0)
                {
                    block_position = new Point(m_tile_width * col, m_tile_height * row);
                }
                else
                {
                    block_position = new Point(m_tile_width * col, ( (m_tile_height * row) + (m_tile_height/2) ));
                }
                
                m_map[row][col] = new Tile(block_position, tile_type, tile_value);
            }
        }
    }
    
    /**
     * Used to simplify the map values, preventing massive changes in data.
     * @param map_values The map values to adjust.
     */
    private void averageMapValues(float[][] map_values)
    {        
        for (int row = 0; row < m_map_width; row++)
        {
            for (int col = 0; col < m_map_width; col++)
            {
                float left = map_values[row][(col - 1) & (m_map_width - 1)];
                float right = map_values[row][(col + 1) & (m_map_width - 1)];
                float up = map_values[(row - 1) & (m_map_width - 1)][col];
                float down = map_values[(row + 1) & (m_map_width - 1)][col];
                
                float top_left = map_values[(row - 1) & (m_map_width - 1)][(col - 1) & (m_map_width - 1)];
                float top_right = map_values[(row - 1) & (m_map_width - 1)][(col + 1) & (m_map_width - 1)];
                float bottom_left = map_values[(row + 1) & (m_map_width - 1)][(col - 1) & (m_map_width - 1)];
                float bottom_right = map_values[(row + 1) & (m_map_width - 1)][(col + 1) & (m_map_width - 1)];
                
                map_values[row][col] = (left + right + up + down + top_left + top_right + bottom_left + bottom_right) / 8f;
            }
        }
    }
    
    /**
     * Primary map data generation function.
     * @param map_values The map values to generate information in.
     * @param size The size of the map.
     * @param noise The amount of noise to add to newly generated values.
     */
    private void diamondSquare(float[][] map_values, int size, float noise)
    {
        if (size <= 2)
        {
            return;
        }
        
        int square_step = size / 2;
        for (int x_offset = 0; x_offset < m_map_width; x_offset += square_step)
        {
            for (int y_offset = 0; y_offset < m_map_width; y_offset += square_step)
            {
                sampleSquare(map_values, new Point(x_offset, y_offset), square_step, noise);
            }
        }
        
        int diamond_step = size / 4;
        for (int x_offset = 0; x_offset < m_map_width; x_offset += square_step)
        {
            for (int y_offset = 0; y_offset < m_map_width; y_offset += square_step)
            {                
                sampleDiamond(map_values, new Point(x_offset + diamond_step, y_offset), square_step, noise);
                sampleDiamond(map_values, new Point(x_offset, y_offset + diamond_step), square_step, noise);
            }
        }
        
        float reduced_noise = (noise * 0.5f);
        diamondSquare(map_values, size / 2, reduced_noise);
    }
    
    /**
     * Sub function in the diamond square algorithm, used in part for map value generation.
     * @param map_values The map values to work with.
     * @param position The position of the sample square.
     * @param size The size of the square to work with.
     * @param noise The amount of noise to add to the generated values.
     */
    private void sampleSquare(float[][] map_values, Point position, int size, float noise)
    {
        int mid_offset = size / 2;
        
        float top_left = map_values[position.y][position.x];
        float top_right = map_values[position.y][position.x + size];
        float bottom_left = map_values[position.y + size][position.x];
        float bottom_right = map_values[position.y + size][position.x + size];
        
        float mid_avg = ((top_left + top_right + bottom_left + bottom_right) / 4f);
        mid_avg += getRandomDisplacement(noise);
        
        map_values[position.y + mid_offset][position.x + mid_offset] = mid_avg;
    }
    
    /**
     * Sub function in the diamond square algorithm, used in part for map value generation.
     * @param map_values The map values to work with.
     * @param position The position of the sample diamond.
     * @param size The size of the diamond to work with.
     * @param noise The amount of noise to add to the generated values.
     */
    private void sampleDiamond(float[][] map_values, Point position, int size, float noise)
    {
        int mid_offset = size / 2;
        
        float left = map_values[position.y][(position.x - mid_offset - 1) & (m_map_width - 1)];
        float right = map_values[position.y][(position.x + mid_offset - 1) & (m_map_width - 1)];
        float up = map_values[(position.y - mid_offset - 1) & (m_map_width - 1)][position.x];
        float down = map_values[(position.y + mid_offset - 1) & (m_map_width - 1)][position.x];
        
        float mid_avg = ((left + right + up + down) / 4f);
        mid_avg += getRandomDisplacement(noise);
        
        map_values[position.x][position.y] = mid_avg;
    }
    
    /**
     * Used to get random displacement from a noise value.
     * @param noise The noise value to use when generating displacement.
     * @return The displacement based on noise provided.
     */
    private float getRandomDisplacement(float noise)
    {
        return m_random.nextInt((int)((noise * 2) + 1)) - noise;
    }
    
    /**
     * Used to get the width of the map.
     * @return an integer width of the map in map space.
     */
    public int getMapWidth()
    {
        return m_map_width;
    }
    
    /**
     * Used to get the height of the map.
     * @return an integer height of the map in map space.
     */
    public int getMapHeight()
    {
        return m_map_height;
    }
    
    /**
     * Used to get the width of the map in world space.
     * @return an integer width of the map in world space.
     */
    public int getMapPixelWidth()
    {
        return m_map_width * m_tile_width;
    }
    
    /**
     * Used to get the height of the map in world space.
     * @return an integer height of the map in world space.
     */
    public int getMapPixelHeight()
    {
        return (m_map_height * m_tile_height) + m_tile_height/2;
    }
    
    /**
     * Used to find the width of a tile in the map. *Depricated: use Tile.TILE_WIDTH
     * @return the width of a tile in pixels.
     */
    public int getTileWidth()
    {
        return m_tile_width;
    }
    
    /**
     * Used to find the height of a tile in the map. *Depricated: use Tile.TILE_HEIGHT
     * @return the height of a tile in pixels.
     */
    public int getTileHeight()
    {
        return m_tile_height;
    }
    
    /**
     * Used to calculate the hexagon visible from a specific point at a specific range.
     * @param src The source point of visibility.
     * @param range The range of vision from the source point.
     * @return A polygon representing the visibility provided by the src and range values.
     */
    private Polygon getVisibilityHex(Point src, int range)
    {
        Polygon visibility_hex = new Polygon();
        
        int expand_coefficient = 7;
        
        visibility_hex.addPoint(src.x, src.y - (Tile.TILE_HEIGHT * range) - expand_coefficient);
        visibility_hex.addPoint(src.x + (Tile.TILE_WIDTH * 3 / 4 * range) + expand_coefficient, src.y - (int)((Tile.TILE_HEIGHT / 2f) * range) - expand_coefficient);
        visibility_hex.addPoint(src.x + (Tile.TILE_WIDTH * 3 / 4 * range) + expand_coefficient, src.y +(int)((Tile.TILE_HEIGHT / 2f) * range) + expand_coefficient);
        visibility_hex.addPoint(src.x, src.y + (Tile.TILE_HEIGHT * range) + expand_coefficient);
        visibility_hex.addPoint(src.x - (Tile.TILE_WIDTH * 3 / 4 * range) - expand_coefficient, src.y + (int)((Tile.TILE_HEIGHT / 2f) * range) + expand_coefficient);
        visibility_hex.addPoint(src.x - (Tile.TILE_WIDTH * 3 / 4 * range) - expand_coefficient, src.y - (int)((Tile.TILE_HEIGHT / 2f) * range) - expand_coefficient);
        
        return visibility_hex;
    }
    
    /**
     * Used to set the visibility of tiles based on a single units vision.
     * @param start_index I'm not sure.
     * @param visibility_range The range of vision being used to set tile visibility.
     * @param total_hex_bounds The bounds of the vision hex for the unit.
     * @param tiles_traversed The set of tiles that have already been set this frame.
     */
    private void setTileVisibility(Point start_index, int visibility_range, Polygon total_hex_bounds, boolean[][] tiles_traversed)
    {
        Player player = Game.getInstance().getPlayer();
        
        for (int y = 0; y <= visibility_range; y++)
        {
            int y_index = start_index.y + y;
            if (y_index < 0)
            {
                continue;
            }
            else if (y_index > m_map_height - 1)
            {
                break;
            }

            for (int x = 0; x <= visibility_range; x++)
            {
                boolean flip_left = false;
                boolean flip_right = false;
                
                int x_index = start_index.x + x;
                if (x_index < 0)
                {
                    x_index += m_map_width;
                    flip_left = true;
                }
                else if (x_index >= m_map_width)
                {
                    flip_right = true;
                    x_index %= m_map_width;
                }
                
                if (tiles_traversed[y_index][x_index])
                {
                    continue;
                }
                
                Tile tile = getTile(x_index, y_index);
                Point tile_pos = (Point)tile.getCenterPosition().clone();
                if (flip_left)
                {
                    tile_pos.translate(-getMapPixelWidth(), y);
                }
                else if (flip_right)
                {
                    tile_pos.translate(getMapPixelWidth(), y);
                }
                
                if (total_hex_bounds.contains(tile_pos))
                {
                    tiles_traversed[y_index][x_index] = true;
                    tile.setVisibility(Tile.TileVisibility.VISIBLE);
                    m_visible_tiles.add(tile);
                    
                    if (!player.hasDiscoveredTile(tile.getPosition()))
                    {
                        player.setDiscoveredTile(tile.getPosition());
                    }
                }
            }
        }
    }
    
    /**
     * Called when a unit changes position.
     */
    @Override
    public void onUnitMove()
    {
        calculateFogOfWar();
    }
    
    /**
     * Used to find all tiles within a range, starting from a specific tile.
     * @param start_tile The tile to start at.
     * @param range The range of tiles to move away from the starting tile.
     * @return A list containing all tiles within the provided range of the starting tile.
     */
    public Tile[] getTilesInRange(Tile start_tile, int range)
    {
        Point start_tile_index = getTileIndex(start_tile.getPosition());
        Polygon total_hex_bounds = getVisibilityHex(start_tile.getCenterPosition(), range);
        
        Point start_index = new Point(start_tile_index.x - range, start_tile_index.y - range);
        List<Tile> tiles = new LinkedList<Tile>();
        
        for (int y = 0; y <= range * 2; y++)
        {
            int y_index = start_index.y + y;
            if (y_index < 0)
            {
                continue;
            }
            else if (y_index > m_map_height - 1)
            {
                break;
            }
            
            for (int x = 0; x <= range * 2; x++)
            {
                boolean flip_left = false;
                boolean flip_right = false;
                
                int x_index = start_index.x + x;
                if (x_index < 0)
                {
                    x_index += m_map_width;
                    flip_left = true;
                }
                else if (x_index >= m_map_width)
                {
                    flip_right = true;
                    x_index %= m_map_width;
                }
                
                Tile tile = getTile(x_index, y_index);
                Point tile_pos = (Point)tile.getCenterPosition().clone();
                if (flip_left)
                {
                    tile_pos.translate(-getMapPixelWidth(), y);
                }
                else if (flip_right)
                {
                    tile_pos.translate(getMapPixelWidth(), y);
                }
                
                if (total_hex_bounds.contains(tile_pos))
                {
                    tiles.add(tile);
                }
            }
        }
        
        Tile[] result = new Tile[tiles.size()];
        tiles.toArray(result);
        return result;
    }
    
    /**
     * Used to calculate the fog of war for the current player.
     */
    public void calculateFogOfWar()
    {
        Player player = Game.getInstance().getPlayer();
        List<Unit> player_units = player.getUnits();
        List<City> player_cities = player.getCities();
        boolean[][] tiles_traversed = new boolean[m_map_height][m_map_width];
        
        for (Tile tile : m_visible_tiles)
        {
            tile.setVisibility(Tile.TileVisibility.FADED);
        }
        
        m_visible_tiles.clear();
        
        for (Unit unit : player_units)
        {
            Tile unit_tile = getTile(unit.getPosition());
            if(unit_tile == null) continue;
            Point unit_tile_index = getTileIndex(unit.getPosition());
            
            int visibility_radius = unit.getVisibilityRange();
            int visibility_range = visibility_radius * 2;
            
            Polygon total_hex_bounds = getVisibilityHex(unit_tile.getCenterPosition(), visibility_radius);
            
            Point start_index = new Point(unit_tile_index.x - visibility_radius, unit_tile_index.y - visibility_radius);
            setTileVisibility(start_index, visibility_range, total_hex_bounds, tiles_traversed);
        }
        
        for (City city : player_cities)
        {
            Tile unit_tile = city.getTile();
            Point unit_tile_index = getTileIndex(unit_tile.getPosition());
            
            int visibility_radius = city.getVisibilityRange();
            int visibility_range = visibility_radius * 2;
            
            Polygon total_hex_bounds = getVisibilityHex(unit_tile.getCenterPosition(), visibility_radius);
            
            Point start_index = new Point(unit_tile_index.x - visibility_radius, unit_tile_index.y - visibility_radius);
            setTileVisibility(start_index, visibility_range, total_hex_bounds, tiles_traversed);
        }
    }
    
    /**
     * Used to draw the fog of war (Over the map).
     * @param graphics The graphics object to draw to.
     */
    public void drawFogOfWar(Graphics graphics)
    {
        Player player = Game.getInstance().getPlayer();
        
        Point x_bounds = new Point();
        Point y_bounds = new Point();
        getMapDrawBounds(x_bounds, y_bounds);
        
        for (int row = y_bounds.x; row <= y_bounds.y; row++)
        {
            for (int col = x_bounds.x; col <= x_bounds.y; col++)
            {
                int tmp_col = col;
                if (tmp_col < 0)
                {
                    tmp_col += m_map_width;
                }
                
                tmp_col %= m_map_width;
                
                Tile tile = m_map[row][tmp_col];
                Point draw_position = Game.getInstance().getCamera().worldToScreenSpace(tile.getPosition());
                
                if (!player.hasDiscoveredTile(tile.getPosition()))
                {
                    graphics.drawImage(m_undiscovered_hex.getImage(), draw_position.x, draw_position.y, null);
                    //graphics.drawImage(ImageManager.getInstance().getSprite("images/CloudsHex.png"), draw_position.x, draw_position.y, null);
                }
                else if (tile.getVisibility() == Tile.TileVisibility.FADED)
                {
                    graphics.drawImage(m_faded_hex.getImage(), draw_position.x, draw_position.y, null);
                }
                
                //tile.setVisibility(Tile.TileVisibility.FADED);
            }
        }
    }
    
    /**
     * Used to get the drawing boundaries for the map.
     * @param x_bounds The point object to save x location boundaries in.
     * @param y_bounds The point object to save y location voundaries in.
     */
    private void getMapDrawBounds(Point x_bounds, Point y_bounds)
    {
        Point camera_pos = Game.getInstance().getCamera().getPosition();
        
        int camera_x_vision = camera_pos.x + Game.SCREEN_WIDTH;
        int camera_y_vision = camera_pos.y + Game.SCREEN_HEIGHT;
        
        if (getMapPixelWidth() <= camera_x_vision)
        {
            x_bounds.x = 0;
            x_bounds.y = m_map_width - 1;
        }
        else
        {
            int start_col = (camera_pos.x / Tile.TILE_WIDTH) - 1;
            int end_col = ((camera_x_vision % getMapPixelWidth()) / (Tile.TILE_WIDTH * 3 / 4));
            
            if (start_col >= end_col)
            {
                start_col -= m_map_width;
            }
            
            x_bounds.x = start_col;
            x_bounds.y = end_col;
        }
        
        if (getMapPixelHeight() <= camera_y_vision)
        {
            y_bounds.x = 0;
            y_bounds.y = m_map_height - 1;
        }
        else
        {
            int start_row = (camera_pos.y / Tile.TILE_HEIGHT) - 1;
            if (start_row < 0)
            {
                start_row = 0;
            }
            
            int end_row = ((camera_y_vision + Game.SCREEN_HEIGHT) / Tile.TILE_HEIGHT);
            if (end_row >= m_map_height)
            {
                end_row = m_map_height - 1;
            }
            
            y_bounds.x = start_row;
            y_bounds.y = end_row;
        }
    }
    
    /**
     * The draw function should be called whenever the map needs to be rendered, and only inside the games draw loop.
     * @param graphics A graphics object is required to draw the map on.
     */
    @Override
    public void draw(Graphics graphics)
    {
        Point x_bounds = new Point();
        Point y_bounds = new Point();
        getMapDrawBounds(x_bounds, y_bounds);
        
        for (int row = y_bounds.x; row <= y_bounds.y; row++)
        {
            for (int col = x_bounds.x; col <= x_bounds.y; col++)
            {
                int tmp_col = col;
                if (tmp_col < 0)
                {
                    tmp_col += m_map_width;
                }
                
                tmp_col %= m_map_width;
                m_map[row][tmp_col].draw(graphics);
            }
        }
    }
    
    /**
     * Called whenever a player begins their turn.
     */
    @Override
    public void onBeginTurn() 
    {
        calculateFogOfWar();
    }

    /**
     * Called whenever a player ends their turn.
     */
    @Override
    public void onEndTurn() 
    {
        // Do nothing.
    }
}
