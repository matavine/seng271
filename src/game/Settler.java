/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.GameTime;
import engine.input.InputHandler;
import engine.ui.UIManager;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * Settler is a specific unit class, that has special abilities most units don't.
 * @author Geoff
 */
public class Settler
    extends Unit
{
    /**
     * Construct a new settler using the unit super class constructor.
     * @param data The unit data to use.
     * @param player The player to control the settler.
     * @param tile The tile to place the settler on.
     */
    public Settler(UnitData data, Player player, Tile tile)
    {
        super(data, player, tile);
    }
    
    /**
     * Used to run Settler related logic every frame.
     * @param game_time The class containg all time information for the game.
     */
    @Override
    public void update(GameTime game_time)
    {
        if(InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_C))
        {
            createCity();
        }
        
        super.update(game_time);
    }
    
    /**
     * Used to display the settler specific UI.
     */
    @Override
    protected void displayUI()
    {
        UIManager.getInstance().displayUI("SettlerUI", 2, this);
    }
    
    /**
     * Used to hide the settler specific UI.
     */
    @Override
    protected void hideUI()
    {
        UIManager.getInstance().hideUI("SettlerUI");
    }
    
    /**
     * Used to create a new city with the settler.
     */
    public void createCity()
    {
        if(m_tile.getCity() != null)
        {
            return;
        }
        
        String city_name;
        JComponent[] inputs = new JComponent[] {
                                    new JLabel(""),
                                    new JLabel("Enter the name of the new city:"),
                                };
        
        while (true)
        {
            city_name = (String)JOptionPane.showInputDialog(null, inputs, "New City", JOptionPane.QUESTION_MESSAGE, null, null, "");
            if(city_name == null)
            {
                return;
            }
            else if (city_name.equals(""))
            {
                ((JLabel)inputs[0]).setText("<html><font color=\"red\">The city name cannot be empty.</font></html>");
                continue;
            }

            break;
        }
        
        City new_city = new City(m_tile, m_player, city_name);
        new_city.setSelected(true);
        m_tile.setCity(new_city);
        m_player.addCity(new_city);
        Game.getInstance().getMap().calculateFogOfWar();
        destroy();
        m_player.setSelectedTile(m_tile);
    }
    
    /**
     * Used to destroy the settler object.
     */
    @Override
    public void destroy()
    {
        m_player.removeUnit(this);
        m_tile.setUnit(null);
        
        if (m_health <= 0)
        {
            m_player.updatePlayerStillInGame();
        }
    }
}
