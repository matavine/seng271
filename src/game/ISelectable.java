/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 * Used to mark an object as selectable on the map.
 * @author michael
 */
public interface ISelectable
{
    /**
     * Used to set the selected state of the object.
     * @param value The value to set the selected state to.
     */
    public void setSelected(boolean value);
    /**
     * Checks if the current object is selected.
     * @return True if the current object is selected.
     */
    public boolean isSelected();
}
