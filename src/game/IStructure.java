/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 * Used to simplify income based structures.
 * @author Ben Cunningham
 */
public interface IStructure
{
    /**
     * Called once per turn in order to gather resources from the structure.
     * @return The income provided by the structure.
     */
    public int getIncome();
}
