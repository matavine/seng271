/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.ISingleton;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;

/**
 * Unit factory is used to create new units from a dataset.
 * @author Ben Cunningham
 */
public class UnitFactory implements ISingleton
{
    
    private static UnitFactory m_instance;
    /**
     * Used to get the singleton instance of the unit factory.
     * @return The single instance of the UnitFactory class.
     */
    public static UnitFactory getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new UnitFactory();
        }
        
        return m_instance;
    }
    
    /**
     * The dictionary holding the set of unit data.
     */
    private HashMap<String, UnitData> m_unit_dictionary;
    
    /**
     * Unit factory constructor.
     */
    private UnitFactory()
    {        
        m_unit_dictionary = new HashMap<String, UnitData>();
        
        //Create a list of UnitData so that it may be placed into the HashMap
        List<UnitData> unit_data_list = UnitData.getUnitData();
        
        //Place the list of UnitData into the HashMaps.
        //Keys are the values of unit_name int the UnitData.xml file.
        for (UnitData unit_data : unit_data_list)
        {
            m_unit_dictionary.put(unit_data.unit_name, unit_data);
        }
    }
    
    /**
     * Creates and returns a unit. Also Adds the unit to the specified player.
     * @param unit_type The string title of the unit to be created e.g. Militia or TransportShip.
     * @param player Player to whom the unit belongs.
     * @param tile Tile the unit will be created on.
     * @return Returns a Unit.
     */
    public Unit makeUnit(String unit_type, Player player, Tile tile)
    {
        UnitData unit_data = m_unit_dictionary.get(unit_type);
        
        Unit return_unit;
                
        try 
        {
            Class c = Class.forName(unit_data.unit_class);

            Class par_types[] = new Class[]{unit_data.getClass(), player.getClass(), tile.getClass()};
            Constructor constructor = c.getConstructor(par_types);

            Object arguements[] = new Object[]{unit_data, player, tile};

            return_unit = (Unit)constructor.newInstance(arguements);
        }
        catch (Exception e)
        {
            System.out.println("Error in UnitFactory while creating a unit." + e.getMessage());
            return null;
        }
        
        return return_unit;
    }
    
    /**
     * Used to get a specific unit data object by unit type.
     * @param unit_type The type of unit to get data for.
     * @return A unit data object holding the information for the provided unit type.
     */
    public UnitData getUnitData(String unit_type)
    {
        if (!m_unit_dictionary.containsKey(unit_type))
        {
            return null;
        }
        
        return m_unit_dictionary.get(unit_type);
    }

    /**
     * Used to destroy all instances of the unit factory.
     */
    @Override
    public void destroy() {
        m_instance = null;
    }

}