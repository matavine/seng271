/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.views;

import engine.AudioManager;
import engine.GameTime;
import engine.ImageManager;
import engine.ui.UIManager;
import engine.views.View;
import game.Game;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Michael
 */
public class StartView extends View
{
    private static final String m_title = "Civilization";
    private static final Font m_title_font = new Font("Trebuchet MS", Font.BOLD, 96);
    private Rectangle2D m_title_bounds;

    @Override
    public void initialize()
    {
      
    }

    @Override
    public void destroy()
    {

    }

    @Override
    public void onEnter()
    {
        if (!AudioManager.getInstance().isMusicPlaying())
        {
            AudioManager.getInstance().playMusic("content/sound/TitleMusic.wav");
        }
        
        UIManager.getInstance().displayUI("StartUI", 2, this);
    }

    @Override
    public void onExit()
    {

    }

    @Override
    public void update(GameTime game_time)
    {
        /*
        if (InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_ENTER))
        {
            ViewManager.getInstance().changeView("game");
        }
        */
    }

    @Override
    public void draw(Graphics graphics)
    {        
        int num_rows = (Game.SCREEN_HEIGHT / 80) + 1;
        int num_cols = (Game.SCREEN_WIDTH / 80) + 1;
        for (int row = 0; row < num_rows; row++)
        {
            for (int col = 0; col < num_cols; col++)
            {
                graphics.drawImage(ImageManager.getInstance().getSprite("images/start_menu_bg.png"), col * 80, row * 80, null);
            }
        }
        
        graphics.setFont(m_title_font);

        if (m_title_bounds == null)
        {
            m_title_bounds = graphics.getFontMetrics().getStringBounds(m_title, graphics);
        }
        
        int x = (Game.SCREEN_WIDTH / 2) - ((int)m_title_bounds.getMaxX() / 2);
        int y = (Game.SCREEN_HEIGHT - 400) - ((int)m_title_bounds.getMaxY() / 2);
        
        ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        graphics.setColor(Color.BLACK);
        graphics.drawString(m_title, x + 3, y + 3);
        
        GradientPaint gp = new GradientPaint(0, y - (int)m_title_bounds.getMaxY(), new Color(250, 255, 220, 255), 0, y + (int)m_title_bounds.getMaxY(), Color.GRAY);
        ((Graphics2D)graphics).setPaint(gp);
        graphics.drawString(m_title, x, y);
        
        graphics.setFont(null);
        ((Graphics2D)graphics).setPaint(null);
        ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        
        graphics.setColor(new Color(0, 0, 0, 64));
        graphics.fillRect(0, Game.SCREEN_HEIGHT - 352, Game.SCREEN_WIDTH, 174);
        graphics.fillRect(0, Game.SCREEN_HEIGHT - 350, Game.SCREEN_WIDTH, 170);
    }

}
