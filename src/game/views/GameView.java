/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.views;

import engine.GameTime;
import engine.input.InputHandler;
import engine.views.View;
import engine.views.ViewManager;
import game.Game;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

/**
 *
 * @author Michael
 */
public class GameView extends View
{
    private Game m_game;

    public GameView()
    {
        // Empty.
    }

    @Override
    public void initialize()
    {
        m_game = Game.getInstance();
        m_game.initialize();
        m_game.loadContent();
    }

    @Override
    public void destroy()
    {
        m_game.unloadContent();
        m_game = null;
    }

    @Override
    public void onEnter()
    {
        engine.AudioManager.getInstance().stopAudio();
        initialize();
    }

    @Override
    public void onExit()
    {
        destroy();
    }

    @Override
    public void update(GameTime game_time)
    {
        /*
        if (InputHandler.getInstance().isInputJustReleased(InputHandler.InputType.KEY, KeyEvent.VK_ESCAPE))
        {
            ViewManager.getInstance().changeView("start");
            return;
        }
        */

        m_game.update(game_time);
    }

    @Override
    public void draw(Graphics graphics)
    {
        m_game.draw(graphics);
    }
}
