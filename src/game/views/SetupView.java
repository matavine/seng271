/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.views;

import engine.GameTime;
import engine.ImageManager;
import engine.ui.UIManager;
import engine.views.View;
import game.Game;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author michael
 */
public class SetupView extends View
{
    private static final Font m_settings_font = new Font("Trebuchet MS", Font.BOLD, 48);
    
    private static final String m_map_settings = "Map Settings";
    private Rectangle2D m_map_settings_bounds;
    
    private static final String m_player_settings = "Player Settings";
    
    @Override
    public void initialize() 
    {
        
    }

    @Override
    public void destroy() 
    {
    }

    @Override
    public void onEnter() 
    {
        UIManager.getInstance().displayUI("SetupUI", 2, this);
    }

    @Override
    public void onExit() 
    {
        
    }

    @Override
    public void update(GameTime game_time) 
    {
        
    }

    @Override
    public void draw(Graphics graphics) 
    {
        int num_rows = (Game.SCREEN_HEIGHT / 80) + 1;
        int num_cols = (Game.SCREEN_WIDTH / 80) + 1;
        for (int row = 0; row < num_rows; row++)
        {
            for (int col = 0; col < num_cols; col++)
            {
                graphics.drawImage(ImageManager.getInstance().getSprite("images/start_menu_bg.png"), col * 80, row * 80, null);
            }
        }
        
        graphics.setFont(m_settings_font);
        if (m_map_settings_bounds == null)
        {
            m_map_settings_bounds = graphics.getFontMetrics().getStringBounds(m_map_settings, graphics);
        }
        
        graphics.setColor(new Color(0, 0, 0, 64));
        graphics.fillRect(0, (int)(Game.SCREEN_HEIGHT * 0.15f) - 2, Game.SCREEN_WIDTH, (int)(Game.SCREEN_HEIGHT * 0.75f) + 4);
        graphics.fillRect(0, (int)(Game.SCREEN_HEIGHT * 0.15f), Game.SCREEN_WIDTH, (int)(Game.SCREEN_HEIGHT * 0.75f));
        
        int map_settings_x = (int)(Game.SCREEN_WIDTH * 0.1f);
        int map_settings_y = (int)(Game.SCREEN_HEIGHT * 0.1f) - ((int)m_map_settings_bounds.getMaxY() / 2);
        
        ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        graphics.setColor(Color.BLACK);
        graphics.drawString(m_map_settings, map_settings_x + 3, map_settings_y + 3);
        
        int player_settings_x = map_settings_x + (Game.SCREEN_WIDTH / 5 * 2);
        int player_settings_y = map_settings_y;
        
        graphics.drawString(m_player_settings, player_settings_x + 3, player_settings_y + 3);
        
        GradientPaint gp = new GradientPaint(0, map_settings_y - (int)m_map_settings_bounds.getMaxY(), new Color(250, 255, 220, 255), 0, map_settings_y + (int)m_map_settings_bounds.getMaxY() + 10, Color.GRAY);
        ((Graphics2D)graphics).setPaint(gp);
        graphics.drawString(m_map_settings, map_settings_x, map_settings_y);
        graphics.drawString(m_player_settings, player_settings_x, player_settings_y);
        
        graphics.setFont(null);
        ((Graphics2D)graphics).setPaint(null);
        ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    }
    
}
