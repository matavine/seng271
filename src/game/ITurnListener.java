/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 * Used to react whenever any player's turn state changes.
 * @author michael
 */
public interface ITurnListener 
{
    /**
     * Called whenever a player's turn begins.
     */
    public void onBeginTurn();
    /**
     * Called whenever a player's turn ends.
     */
    public void onEndTurn();
}
