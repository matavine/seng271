/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import engine.GameTime;
import engine.IDrawable;
import engine.IUpdateable;
import engine.ImageManager;
import engine.ui.UIManager;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing cities in Aciv. Handles all city related processing.
 * @author Ben Cunningham
 */
public class City
    implements IStructure, IDrawable, IUpdateable, ISelectable
{    
    public static final int CITY_DOT_DIAMETER = 14;
    
    private Point m_position;
    private Tile m_tile;
    private Player m_player;
    private String m_name;
    
    private Rectangle2D m_name_size;
    private int m_name_plate_width; 
    
    private List<Player> m_seen_players;
    private byte m_buildings_flag;
    private Unit m_unit;
    private List m_tiles;
    private int m_visibility_range = 4;
    
    private int m_max_health = 60;
    private float m_health;
    
    private boolean m_is_selected;
    
    private Tile[] m_resource_tiles;
    
    /**
     * Used to create a new city object.
     * @param tile The tile to place the city on.
     * @param player The player to control the city.
     * @param name The name of the new city.
     */
    public City(Tile tile, Player player, String name)
    {
        m_tile = tile;
        m_player = player;
        m_name = name;
        m_health = m_max_health;
        m_seen_players = new ArrayList<Player>();
        m_seen_players.add(player);
        m_position = tile.getPosition();
        m_buildings_flag = 0;
        m_unit = null;
        
        Point tile_index = Game.getInstance().getMap().getTileIndex(m_position);
        m_resource_tiles = Game.getInstance().getMap().getNeighbors(tile_index.y, tile_index.x);
    }
    
    /**
     * Used to create a unit from the city.
     * @param unit_type The type of unit to create.
     * @return True if the unit is successfully created.
     */
    public boolean createUnit(String unit_type)
    {
        if(m_tile.getUnit() != null)
        {
            return false;
        }
        
        Unit unit = UnitFactory.getInstance().makeUnit(unit_type, m_player, m_tile);
        m_tile.setUnit(unit);
        unit.setSelected(true);
        m_player.addUnit(unit);
        return true;
    }
    
    /**
     * Used to get the city's current health.
     * @return the float value representing the city's health.
     */
    public float getHealth()
    {
        return m_health;
    }
    
    /**
     * Used to add health to the city.
     * @param new_health The amount of health to be added.
     */
    public void addHealth(int new_health)
    {
        m_health += new_health;
    }
    
    /**
     * Used to deal damage to the city.
     * @param damage The amount of damage to be dealt to the city.
     */
    public void takeDamage(float damage)
    {
        m_health -= damage;
    }
    
    /**
     * Used to change the player controlling the city.
     * @param new_player The player to take control of the city.
     */
    public void changePlayer(Player new_player)
    {
        m_player.removeCity(this);
        m_player.updatePlayerStillInGame();
        m_player = new_player;
        m_player.addCity(this);
    }
    
    /**
     * Used for getting the name of the city.
     * @return The name of the city as a string.
     */
    public String getName()
    {
        return m_name;
    }
    
    /**
     * Used for getting the tile that the city is placed on.
     * @return The Tile containing the city.
     */
    public Tile getTile()
    {
        return m_tile;
    }
    
    /**
     * Used for getting the player who controls the city.
     * @return The player controlling the city.
     */
    public Player getPlayer()
    {
        return m_player;
    }
    
    /**
     * Used to get the position of the city in world space.
     * @return The position of the city in world space.
     */
    public Point getPosition()
    {
        return m_position;
    }
    
    /**
     * Used to create a building within the city. (Not implemented)
     */
    public void createBuilding()
    {
        // TODO
    }
    
    /**
     * Used to check if a city has a specific type of building. (Not implemented)
     */
    public void hasBuilding()
    {
        // TODO
    }
    
    /**
     * Used to create a resource building around the city, increasing its income. (Not implemented)
     */
    public void createResourceBuilding()
    {
        // TODO
    }
    
    /**
     * Used to get the visibility range of the city.
     * @return 
     */
    public int getVisibilityRange()
    {
        return m_visibility_range;
    }
    
    /**
     * Used to inform a city that a player has seen it, allowing the city to show up on the minimap.
     */
    public void informSeen()
    {
        if (!m_seen_players.contains(Game.getInstance().getPlayer()))
        {
            m_seen_players.add(Game.getInstance().getPlayer());
        }
    }
    
    /**
     * Sets the selected value for the city.
     * @param value The selected value for the city. (True for selected).
     */
    @Override
    public void setSelected(boolean value) 
    {
        m_is_selected = value;
        
        if (m_is_selected)
        {
            displayUI();
            return;
        }
        
        hideUI();
    }
    
    /**
     * Used to check if the city is selected.
     * @return True if the city is selected.
     */
    @Override
    public boolean isSelected() 
    {
        return m_is_selected;
    }
    
    /**
     * Used to display the city's User Interface.
     */
    private void displayUI()
    {
        UIManager.getInstance().displayUI("CityUI", 1, this);
    }
    
    /**
     * Used to hide the city's User Interface.
     */
    private void hideUI()
    {
        UIManager.getInstance().hideUI("CityUI");
    }
    
    /**
     * Used to get the Income level of the city.
     * @return 
     */
    @Override
    public int getIncome()
    {
        int total_income = 0;
        for (int i = 0; i < m_resource_tiles.length; i++)
        {
            total_income += m_resource_tiles[i].getType().getIncome();
        }
        
        return total_income;
    }
    
    /**
     * Used to update the city each frame.
     * @param game_time The current time information for the game.
     */
    @Override
    public void update(GameTime game_time)
    {
    }
    
    /**
     * Used to draw the city.
     * @param graphics The graphics object to draw the city on.
     */
    @Override
    public void draw(Graphics graphics) {
        if (!Game.getInstance().getPlayer().hasDiscoveredTile(m_position))
        {
            return;
        }
        if (!m_seen_players.contains(Game.getInstance().getPlayer()))
        {
            return;
        }
        
        Image image;
        switch(m_player.getAge())
        {
            case STONE:
            case BRONZE:
            case IRON:
            case STEEL:
            case GUNPOWDER:
            default:
                image = ImageManager.getInstance().getSprite("images/StoneCity.png");
                break;
        }
        
        if (m_name_size == null)
        {
            m_name_size = graphics.getFontMetrics().getStringBounds(m_name, graphics);
            m_name_plate_width = 10 + CITY_DOT_DIAMETER + (int)m_name_size.getMaxX();
        }
        
        Point draw_position = Game.getInstance().getCamera().worldToScreenSpace(m_position);
        
        graphics.drawImage(image, draw_position.x, draw_position.y, null);
    }
    
    /**
     * Used to draw the UI elements of the city.
     * @param graphics The graphics object to draw on.
     */
    public void drawUI(Graphics graphics)
    {
        if (!Game.getInstance().getPlayer().hasDiscoveredTile(m_position))
        {
            return;
        }
        if (!m_seen_players.contains(Game.getInstance().getPlayer()))
        {
            return;
        }
        
        Point draw_position = Game.getInstance().getCamera().worldToScreenSpace(m_position);
        drawCityName(graphics, draw_position);
        
        if(m_health != m_max_health)
        {
            graphics.setColor(Color.DARK_GRAY);
            graphics.fillRect(draw_position.x + 10, draw_position.y + Tile.TILE_HEIGHT - 11, Tile.TILE_WIDTH - 20, 6);
            graphics.setColor(Color.BLACK);
            graphics.drawRect(draw_position.x + 10, draw_position.y + Tile.TILE_HEIGHT - 11, Tile.TILE_WIDTH - 20, 6);
            graphics.setColor(Color.YELLOW);
            graphics.fillRect(draw_position.x + 11, draw_position.y + Tile.TILE_HEIGHT - 10, (int)((Tile.TILE_WIDTH - 22) * ((float)m_health/(float)m_max_health)), 5);
        }
    }
    
    /**
     * Used to draw the city name tag.
     * @param graphics The graphics object to draw to.
     * @param draw_position The position to draw the name tag at.
     */
    private void drawCityName(Graphics graphics, Point draw_position)
    {
        if(m_tile != Game.getInstance().getPlayer().getHoveredTile()) // Player not hovering over tile.
        {
            graphics.setColor(m_player.getColor());
            graphics.fillOval(draw_position.x + 2, draw_position.y, CITY_DOT_DIAMETER, CITY_DOT_DIAMETER);
        
            graphics.setColor(Color.BLACK);
            graphics.drawOval(draw_position.x + 2, draw_position.y, CITY_DOT_DIAMETER, CITY_DOT_DIAMETER);
        }
        else
        {   
            int horizontal_offset = (m_name_plate_width - Tile.TILE_WIDTH)/2;
            
            Color color_background = new Color(m_player.getColor().getRed()/4*3, m_player.getColor().getGreen()/4*3, m_player.getColor().getBlue()/4*3);
            
            graphics.setColor(color_background);
            graphics.fillRoundRect(draw_position.x - horizontal_offset - 2, draw_position.y - 2, m_name_plate_width, 4 + CITY_DOT_DIAMETER,  CITY_DOT_DIAMETER, CITY_DOT_DIAMETER);

            graphics.setColor(Color.BLACK);
            graphics.drawRoundRect(draw_position.x - horizontal_offset - 2, draw_position.y - 2, m_name_plate_width, 4 + CITY_DOT_DIAMETER, CITY_DOT_DIAMETER, CITY_DOT_DIAMETER);
            graphics.drawString(m_name, draw_position.x - horizontal_offset + CITY_DOT_DIAMETER + 4, draw_position.y + 12);

            graphics.setColor(m_player.getColor());
            graphics.fillOval(draw_position.x - horizontal_offset, draw_position.y, CITY_DOT_DIAMETER, CITY_DOT_DIAMETER);

            graphics.setColor(Color.BLACK);
            graphics.drawOval(draw_position.x - horizontal_offset, draw_position.y, CITY_DOT_DIAMETER, CITY_DOT_DIAMETER);
        }
    }
}
