/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The class designed to hold all data required to create individual unit types.
 * @author Geoff
 */
public class UnitData
{
    public String image_path;
    public String unit_class;
    public String unit_type;
    public String unit_name;
    public String unit_type_weakness;
    public int move_range;
    public int attack_range;
    public int max_health;
    public int attack;
    public int defense;
    public int transport_capacity;
    public int visibility_range;
    public int cost;

    
    public HashMap<Tile.TileType, Integer> tile_move_cost;
 

    public UnitData()
    {
    }
    
    /**
     * Creates a list UnitData for all unit types using the data in the UnitData.xml file.
     * @return List<UnitData>
     */
    public static List<UnitData> getUnitData()
    {
        
        List<UnitData> unit_data_list = new LinkedList<UnitData>();
        
        try
        {
            File unit_data_file = new File("content/data/UnitData.xml");
            
            DocumentBuilderFactory doc_factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder doc_builder = doc_factory.newDocumentBuilder();
            Document doc = doc_builder.parse(unit_data_file);
                        
            NodeList node_list = doc.getElementsByTagName("unit");
            
            Node n;
            UnitData unit_data;
            
            for (int i = 0; i < node_list.getLength(); i++)
            {
                n = node_list.item(i);
                unit_data = new UnitData();
                
                Class data_class = unit_data.getClass();
                
                if (n.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element element = (Element) n;
                    NodeList child_nodes = element.getChildNodes();
                    
                    for (int j = 0; j < child_nodes.getLength(); j++)
                    {
                        if (!(child_nodes.item(j).getNodeName().equals("#text")))
                        {
                            if (child_nodes.item(j).getAttributes().item(0).getNodeValue().equals("java.util.HashMap"))
                            {
                                HashMap<Tile.TileType, Integer> move_costs = new HashMap();
                                NodeList hash_childs = child_nodes.item(j).getChildNodes();
                                
                                for (int k = 0; k < hash_childs.getLength(); k++)
                                {   
                                    if (!(hash_childs.item(k).getNodeName().equals("#text")))
                                    {
                                        move_costs.put(Tile.TileType.valueOf(hash_childs.item(k).getNodeName()), (Integer)Integer.parseInt(getTagValue(hash_childs.item(k).getNodeName(), element)));
                                    }
                                }
                                
                                data_class.getField(child_nodes.item(j).getNodeName()).set(unit_data, move_costs);
                                continue;
                            }                            
                            Object o = parseObjectFromString(getTagValue(child_nodes.item(j).getNodeName(), element), Class.forName(child_nodes.item(j).getAttributes().item(0).getNodeValue()));                         
                            data_class.getField(child_nodes.item(j).getNodeName()).set(unit_data, o);
                        }
                    }
                    unit_data_list.add(unit_data);
                }
            }        
        }
        catch (Exception e)
        {
            System.out.println("Error parsing UnitData XML file.");
            e.printStackTrace();
            return null;
        }
        return unit_data_list;
    }
    
    /**
     * Used to parse a class from a string.
     * @param <T> The class type to be parsed.
     * @param s The string to be parsed from.
     * @param clazz the class to be constructed from.
     * @return an object parsed from the string.
     * @throws Exception 
     */
    private static <T> T parseObjectFromString(String s, Class<T> clazz) throws Exception {
        return clazz.getConstructor(new Class[] {String.class }).newInstance(s);
    }
    
    /**
     * Used to get a tag value from an element.
     * @param tag The tag name to be read.
     * @param element The element to get a tag value from.
     * @return The tag value with the provided name in the provided element.
     */
    private static String getTagValue(String tag, Element element)
    {
        NodeList node_list = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node n = (Node) node_list.item(0);
        return n.getNodeValue();
    }

}
