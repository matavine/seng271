import sys
import os

path = os.path.split(os.path.abspath(__file__))[0]
code_files = [".java", ".py", ".c", ".h", ".cpp", ".cs"]
comment_characters = ["//", "#", "/*", "*/", "*"]

ignore_comments = True
ignore_brackets = True
ignore_whitespace = True

if (len(sys.argv) > 1):
	if (sys.argv[1] == "-h"):
		print("---------- LineCounter.py Help ----------\n")
		print("The first argument is the path to start the recursive count from.\n")
		print("  -c includes comment lines in the count.\n")
		print("  -b includes bracket only lines in the count.\n")
		print("  -w includes empty lines in the count.\n")
		print("  -h prints help text for the tool\n")
		sys.exit()
	elif (not sys.argv[1].startswith("-")):
		path = os.path.join(path, sys.argv[1])
	
if ("-c" in sys.argv):
	ignore_comments = False
if ("-b" in sys.argv):
	ignore_brackets = False
if ("-w" in sys.argv):
	ignore_whitespace = False

def countLines(path):
	line_count = 0
	
	for file_name in os.listdir(path):
		for file_type in code_files:
			if(file_name.endswith(file_type)):
				print("Reading: " + file_name)
				file_count = 0
				file = open(os.path.join(path, file_name))
				for line in file.readlines():
					is_expression = True
					for comment_char in comment_characters:
						if((line.strip().startswith(comment_char) and ignore_comments)):
							is_expression = False
					if(line.strip() == "" and ignore_whitespace):
						is_expression = False
					if( (line.strip() == "{" or line.strip() == "}") and ignore_brackets):
						is_expression = False
						
					if(is_expression):
						file_count += 1
				file.close()
				print("Lines: " + str(file_count))
				print("")
				line_count += file_count
				
				
		if(os.path.isdir(os.path.join(path, file_name))):
			line_count += countLines(os.path.join(path, file_name))
	
	return line_count
			
print("Couting number of lines of code in project: " + path)
count = countLines(path)

print("final count: " + str(count))

